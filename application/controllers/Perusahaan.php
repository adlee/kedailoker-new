<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('perusahaan_model');
	}

	public function index(){
		$data['perusahaan'] = $this->perusahaan_model->get_location();
		$this->load->view('perusahaan/index', $data);
	}

	public function login(){
		$email = $this->input->post('mail');
		$password = $this->input->post('sandi');
		$data = $this->perusahaan_model->get_login($email , $password);
	}

	public function home(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/home',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama','','required');
		$this->form_validation->set_rules('telepon','','required');
		$this->form_validation->set_rules('region','','required');
		$this->form_validation->set_rules('alamat','','required');
		$this->form_validation->set_rules('prop','','required');
		$this->form_validation->set_rules('kota','','required');
		$this->form_validation->set_rules('nama_hrd','','required');
		$this->form_validation->set_rules('teleponhrd','','required');
		$this->form_validation->set_rules('email','','required');
		$this->form_validation->set_rules('sandi','','required');
		$this->form_validation->set_rules('repeat','','required');

		if ($this->form_validation->run() === FALSE) {
			redirect('perusahaan');
		}else{
			$this->perusahaan_model->add_perusahaan();
			$tlp 	= $this->input->post('telepon');
			$this->perusahaan_model->get_session($tlp);
		}
	}

	public function persetujuan(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$this->load->view('perusahaan/persetujuan', $data);
	}

	public function setuju(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$this->perusahaan_model->set_on($no);
		redirect('perusahaan/home');
	}

	public function profile(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$data['ket']=$this->perusahaan_model->get_info($no);
		$this->load->view('perusahaan/header_profile',$data);
		$this->load->view('perusahaan/profile',$data);
		$this->load->view('perusahaan/footer_profile',$data);
	}
	public function ubah_profile(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$data['ket']=$this->perusahaan_model->get_info($no);
		$this->load->view('perusahaan/header_profile',$data);
		$this->load->view('perusahaan/ubah_profile',$data);
		$this->load->view('perusahaan/footer_profile',$data);
	}
	public function ubah_exc(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$this->perusahaan_model->update_profile($no);
		redirect('perusahaan/profile');
	}
	public function loker(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$data['ket']=$this->perusahaan_model->get_info($no);
		$data['loker']=$this->perusahaan_model->get_loker($no);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$this->load->view('perusahaan/header_profile',$data);
		$this->load->view('perusahaan/loker',$data);
		$this->load->view('perusahaan/footer_profile',$data);
	}

	public function detail_loker($id_post){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$data['ket']=$this->perusahaan_model->get_info($no);
		$data['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_post);
		$data['pelamar'] = $this->perusahaan_model->get_pelamar_idlow($id_post);
		$this->load->view('perusahaan/header_profile',$data);
		$this->load->view('perusahaan/detail_loker',$data);
		$this->load->view('perusahaan/footer_profile',$data);
	}
	public function delete_pelamar_p($id_loker, $id_pelamar){
		$this->perusahaan_model->delete_pelamar($id_loker, $id_pelamar);
		redirect('perusahaan/profile/loker/'.$id_loker);
	}
	public function delete_pelamar_h($id_loker, $id_pelamar){
		$this->perusahaan_model->delete_pelamar($id_loker, $id_pelamar);
		redirect('perusahaan/home/post/'.$id_loker);
	}
	public function delete_pelamar_pl($id_loker, $id_pelamar, $telp){
		$this->perusahaan_model->delete_pelamar($id_loker, $id_pelamar);
		redirect('perusahaan/home/pelamar/'.$id_pelamar.'/'.$telp);
	}
	public function konfirm_pelamar_p($id_loker, $id_pelamar){
		$this->perusahaan_model->konfirm_pelamar($id_loker, $id_pelamar);
		redirect('perusahaan/profile/loker/'.$id_loker);
	}
	public function konfirm_pelamar_h($id_loker, $id_pelamar){
		$this->perusahaan_model->konfirm_pelamar($id_loker, $id_pelamar);
		redirect('perusahaan/home/post/'.$id_loker);
	}
	public function konfirm_pelamar_pl($id_loker, $id_pelamar, $telp){
		$this->perusahaan_model->konfirm_pelamar($id_loker, $id_pelamar);
		redirect('perusahaan/home/pelamar/'.$id_pelamar.'/'.$telp);
	}
	public function delete_pelamar($id_loker, $id_pelamar){
		$this->perusahaan_model->delete_pelamar($id_loker, $id_pelamar);
		redirect('perusahaan/home');
	}

	public function uploadloker($id=FALSE){

		if ($id==FALSE) {

			$mail                  = $this->session->userdata('mail');
			$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
			$this->load->view('perusahaan/get_telepon',$datatlp);
			$no                    = $this->session->userdata('nom');	
			$data['status']        = $this->perusahaan_model->get_status($no);
			$data['ket']           = $this->perusahaan_model->get_info($no);

			$this->load->view('perusahaan/header_profile',$data);
			$this->load->view('perusahaan/uploadloker',$data);
			$this->load->view('perusahaan/footer_profile',$data);

		}else{

			$this->perusahaan_model->upload_loker($id);
			redirect('perusahaan/profile/loker');
		}	

	}

	public function delete_posting( $id_low){
		$this->perusahaan_model->delete_posting($id_low);
		redirect("perusahaan/home");
	}
	public function delete_posting_p( $id_low){
		$this->perusahaan_model->delete_posting($id_low);
		redirect("perusahaan/profile/loker");
	}
	public function postingan($id_post){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_post);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['datapelamar'] = $this->perusahaan_model->get_pelamar_idlow($id_post);
		$data['pelamarx']=$this->perusahaan_model->get_pelamar();
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/postingan',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	
	public function edit_postingan($id_post){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_post);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['pelamarx']=$this->perusahaan_model->get_pelamar();
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/edit_post',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function edit_postingan_exc($id_post){
		$this->perusahaan_model->update_loker($id_post);
		redirect('perusahaan/home');
	}
	public function edit_postingan_p($id_post){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_post);
		$data['pelamarx']=$this->perusahaan_model->get_pelamar();
		$this->load->view('perusahaan/header_profile',$data);
		$this->load->view('perusahaan/edit_post_p',$data);
		$this->load->view('perusahaan/footer_profile',$data);
	}
	public function edit_postingan_exc_p($id_post){
		$this->perusahaan_model->update_loker($id_post);
		redirect('perusahaan/profile/loker');
	}
	public function pelamar($id, $telp){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['informal'] = $this->perusahaan_model->get_pelamar_informal($telp);
		$data['pengalaman'] = $this->perusahaan_model->get_pelamar_pengalaman($telp);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['datapelamar'] = $this->perusahaan_model->get_pelamar_idlow($id);
		$data['datapelamar'] = $this->perusahaan_model->get_pelamar_id($id);
		$data['pelamarx']=$this->perusahaan_model->get_pelamar();
		$data['lamaran'] = $this->perusahaan_model->get_lamaran_ke($no, $id);
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/pelamar',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function detail_postingan($id_post, $id_pelamar, $telp){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_post);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['pelamary'] = $this->perusahaan_model->get_pelamar_id($id_pelamar);
		$data['informal'] = $this->perusahaan_model->get_pelamar_informal($telp);
		$data['pengalaman'] = $this->perusahaan_model->get_pelamar_pengalaman($telp);
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['pelamarx']=$this->perusahaan_model->get_pelamar();
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/detail_lamaran',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function spek_loker($id_post, $id_pelamar, $telp){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$data['ket']=$this->perusahaan_model->get_info($no);
		$data['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_post);
		$data['pelamar'] = $this->perusahaan_model->get_pelamar_id($id_pelamar);
		$data['informal'] = $this->perusahaan_model->get_pelamar_informal($telp);
		$data['pengalaman'] = $this->perusahaan_model->get_pelamar_pengalaman($telp);
		$this->load->view('perusahaan/header_profile',$data);
		$this->load->view('perusahaan/spek_loker',$data);
		$this->load->view('perusahaan/footer_profile',$data);
	}
	public function help1(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$data['ket']=$this->perusahaan_model->get_info($no);
		$this->load->view('perusahaan/header_profile',$data);
		$this->load->view('perusahaan/help/ubah_profile');
		$this->load->view('perusahaan/footer_profile',$data);		
	}

	public function message_pp($id_perusahaan, $id_lowongan, $id_pencari){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$data['ket']=$this->perusahaan_model->get_info($no);
		$data['pelamar'] = $this->perusahaan_model->get_pelamar_id($id_pencari);
		$data['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_lowongan);
		$this->load->view('perusahaan/header_profile',$data);
		$this->load->view('perusahaan/message_pp',$data);
		$this->load->view('perusahaan/footer_profile',$data);
	}
	public function send_message_pp($id_perusahaan, $id_lowongan, $id_pencari){
		$this->perusahaan_model->set_message($id_perusahaan, $id_lowongan, $id_pencari);
		redirect('perusahaan/profile/loker/'.$id_lowongan);
	}
	public function message_p($id_perusahaan, $id_lowongan, $id_pencari){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['datapelamar'] = $this->perusahaan_model->get_pelamar_id($id_pencari);
		$data['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_lowongan);
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/message_p',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function send_message_p($id_perusahaan, $id_lowongan, $id_pencari){
		$this->perusahaan_model->set_message($id_perusahaan, $id_lowongan, $id_pencari);
		redirect('perusahaan/home/post/'.$id_lowongan);
	}
	public function message_pl($id_perusahaan, $id_lowongan, $id_pencari){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['datapelamar'] = $this->perusahaan_model->get_pelamar_id($id_pencari);
		$data['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_lowongan);
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/message_pl',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function send_message_pl($id_perusahaan, $id_lowongan, $id_pencari, $telp){
		$this->perusahaan_model->set_message($id_perusahaan, $id_lowongan, $id_pencari);
		redirect('perusahaan/home/pelamar/'.$id_pencari."/".$telp);
	}
	public function send_message_ph($id_perusahaan){
		$this->perusahaan_model->set_message_ph($id_perusahaan);
		redirect('perusahaan/pesan/terkirim');
	}
	public function pesan_terkirim(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['pesan'] = $this->perusahaan_model->get_pesan();
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/data_pesan_terkirim',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function delete_pesan($id_pesan){
		$this->perusahaan_model->delete_message($id_pesan);
		redirect('perusahaan/pesan/terkirim');
	}
	public function lihat_pesan($id_pesan){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['pesan'] = $this->perusahaan_model->get_pesan($id_pesan);
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/detail_pesan_terkirim',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function edit_pesan($id_pesan){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['pesan'] = $this->perusahaan_model->get_pesan($id_pesan);
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/edit_pesan_terkirim',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function edit_pesan_exc($id_pesan){
		$this->perusahaan_model->edit_pesan($id_pesan);
		redirect('perusahaan/pesan/terkirim');
	}
	public function tutup_lowongan($id_lowongan){
		$this->perusahaan_model->tutup_lowongan($id_lowongan);
		redirect('perusahaan/home');
	}
	public function buka_lowongan($id_lowongan){
		$this->perusahaan_model->buka_lowongan($id_lowongan);
		redirect('perusahaan/home');
	}
	public function tutup_lowongan_p($id_lowongan){
		$this->perusahaan_model->tutup_lowongan($id_lowongan);
		redirect('perusahaan/profile/loker');
	}
	public function buka_lowongan_p($id_lowongan){
		$this->perusahaan_model->buka_lowongan($id_lowongan);
		redirect('perusahaan/profile/loker');
	}
	public function buka_pesan($idmjf){
		$this->perusahaan_model->buka_pesan($idmjf);
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['pesan'] = $this->perusahaan_model->get_pesan();
		$data['jobfair'] = $this->perusahaan_model->get_message_jf($no);
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$data['jobid'] = $this->perusahaan_model->get_jf_id($idmjf);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/data_pesan_jf',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function del_pesan_jf($id){
		$this->perusahaan_model->del_pesan_jf($id);
		redirect('perusahaan/pesan/terkirim');
	}
	public function masa_aktif(){
		$mail = $this->session->userdata('mail');
		$datatlp['no_telepon'] = $this->perusahaan_model->get_no_telepon($mail);
		$this->load->view('perusahaan/get_telepon',$datatlp);
		$no = $this->session->userdata('nom');	
		$data['status'] = $this->perusahaan_model->get_status($no);
		$this->load->view('perusahaan/cek_session',$data);
		$data['ket'] = $this->perusahaan_model->get_info($no);
		$data['sd'] = $this->perusahaan_model->get_sd($no);
		$data['smp'] = $this->perusahaan_model->get_smp($no);
		$data['smk'] = $this->perusahaan_model->get_smk($no);
		$data['sma'] = $this->perusahaan_model->get_sma($no);
		$data['d1'] = $this->perusahaan_model->get_d1($no);
		$data['d2'] = $this->perusahaan_model->get_d2($no);
		$data['d3'] = $this->perusahaan_model->get_d3($no);
		$data['d4'] = $this->perusahaan_model->get_d4($no);
		$data['s1'] = $this->perusahaan_model->get_s1($no);
		$data['s2'] = $this->perusahaan_model->get_s2($no);
		$data['post'] = $this->perusahaan_model->get_loker($no);
		$data['post2'] = $this->perusahaan_model->get_loker2($no);
		$data['pel'] = $this->perusahaan_model->get_namapelamar($no);
		$data['pelamar']=$this->perusahaan_model->get_pelamar();
		$data['pesan'] = $this->perusahaan_model->get_pesan();
		$data['new'] = $this->perusahaan_model->get_newmessage_jf($no);
		$this->load->view('perusahaan/header_home',$data);
		$this->load->view('perusahaan/masa_aktif',$data);
		$this->load->view('perusahaan/footer_home',$data);
	}
	public function update_masa_aktif(){
		$this->perusahaan_model->update_masa_aktif();
		redirect('perusahaan/masa_aktif');
	}
}
