<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller
{
    var $limit=10;
    var $offset=10;

    public function __construct() {
        parent::__construct();
        $this->load->model('upload_model'); //load model mupload yang berada di folder model
        $this->load->helper(array('url')); //load helper url 
        $this->load->helper('url_helper');
        $this->load->model('klien_model');
        $this->load->model('admin_model');
        $this->load->model('perusahaan_model');
    }
    public function update_perusahaan($id){
        $no = $this->session->userdata('no');
        $this->perusahaan_model->update_profile($no);

        $this->load->library('upload');
        $config['upload_path'] = './dist/img/perusahaan'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['overwrite'] = TRUE;
        $config['max_size'] = '2048'; //maksimum besar file 2M
        // $config['max_width']  = '1600'; //lebar maksimum 1288 px
        // $config['max_height']  = '1200'; //tinggi maksimu 768 px
        $dbfoto = $no;
        $config['file_name'] = $dbfoto;
      
        $this->upload->initialize($config);
        
        if($_FILES['fupload']['name'])
        {
            if ($this->upload->do_upload('fupload'))
            {
                $gbr = $this->upload->data();
                $data=explode("/", $gbr['file_type']);
                $type=$data[1];
                $data = array(
                  'foto' =>$dbfoto.".".$type
                );
                $this->upload_model->update_foto_perusahaan($data,$id);
                redirect('perusahaan/profile'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                redirect('perusahaan/profile/ubah'); //jika gagal maka akan ditampilkan form upload
            }
        }else{
            redirect('perusahaan/profile');
        }
    }
    public function index($page=NULL,$offset='',$key=NULL){
        $this->load->view('vupload'); //tampilan awal ketika controller upload di akses
    }
    public function add() {
        $this->load->view('fupload');
       
    }
    public function foto_insert(){
        $no = $this->session->userdata('no');
        $this->load->library('upload');
        $config['upload_path'] = './dist/img'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['overwrite'] = TRUE;
        $config['max_size'] = '2048'; //maksimum besar file 2M
       // $config['max_width']  = '1600'; //lebar maksimum 1288 px
      //  $config['max_height']  = '1200'; //tinggi maksimu 768 px
        $dbfoto = $no;
        $config['file_name'] = $dbfoto;
      

        $this->upload->initialize($config);
        
        if($_FILES['fupload']['name'])
        {
            if ($this->upload->do_upload('fupload'))
            {
                $gbr = $this->upload->data();
                $data=explode("/", $gbr['file_type']);
                $type=$data[1];
                $data = array(
                  'foto' =>$dbfoto.".".$type
                );
                $this->upload_model->get_insert($data,$no);
                redirect(''); //jika berhasil maka akan ditampilkan view vupload
            }else{
                redirect(''); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
    public function pas_foto_insert(){
        $no = $this->session->userdata('no');
        $this->load->library('upload');
        $config['upload_path'] = './dist/berkas'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diakses bisa anda sesuaikan
        $config['overwrite'] = TRUE;
        $config['max_size'] = '1024'; //maksimum besar file 2M
       // $config['max_width']  = '1600'; //lebar maksimum 1288 px
      //  $config['max_height']  = '1200'; //tinggi maksimu 768 px
        $dbfoto = $no;
        $config['file_name'] = "pas_".$dbfoto;
      

        $this->upload->initialize($config);
        
        if($_FILES['fupload']['name'])
        {
            if ($this->upload->do_upload('fupload'))
            {
                $gbr = $this->upload->data();
                $data=explode("/", $gbr['file_type']);
                $type=$data[1];
                $data = array(
                  'pas' =>"pas_".$dbfoto.".".$type
                );
                $this->upload_model->get_insert($data,$no);
                redirect('klien/profile'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                redirect(''); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
    public function cv_insert(){
        $no = $this->session->userdata('no');
        $this->load->library('upload');
        $config['upload_path'] = './dist/berkas'; //path folder
        $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['overwrite'] = TRUE;
        $config['max_size'] = '2048'; //maksimum besar file 2M
       // $config['max_width']  = '1600'; //lebar maksimum 1288 px
      //  $config['max_height']  = '1200'; //tinggi maksimu 768 px
        $dbfoto = $no;
        $config['file_name'] = $dbfoto;
      

        $this->upload->initialize($config);
        
        if($_FILES['fupload']['name'])
        {
            if ($this->upload->do_upload('fupload'))
            {
                $gbr = $this->upload->data();
                $data=explode("/", $gbr['file_type']);
                $type=$data[1];
                $data = array(
                  'cv' =>$dbfoto.".".$type
                );
                $this->upload_model->get_insert($data,$no);
                redirect('klien/profile'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                redirect(''); //jika gagal maka akan ditampilkan form upload
            }
        }
    }

     public function ktp_insert(){
        $no = $this->session->userdata('no');
        $this->load->library('upload');
        $config['upload_path'] = './dist/berkas'; //path folder
        $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['overwrite'] = TRUE;
        $config['max_size'] = '2048'; //maksimum besar file 2M
       // $config['max_width']  = '1600'; //lebar maksimum 1288 px
      //  $config['max_height']  = '1200'; //tinggi maksimu 768 px
        $dbfoto = $no;
        $config['file_name'] = "ktp_".$dbfoto;
      

        $this->upload->initialize($config);
        
        if($_FILES['fupload']['name'])
        {
            if ($this->upload->do_upload('fupload'))
            {
                $gbr = $this->upload->data();
                $data=explode("/", $gbr['file_type']);
                $type=$data[1];
                $data = array(
                  'ktp' =>"ktp_".$dbfoto.".".$type
                );
                $this->upload_model->get_insert($data,$no);
                redirect('klien/profile'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                redirect(''); //jika gagal maka akan ditampilkan form upload
            }
        }
    }

      public function ijazah_dp_insert(){
        $no = $this->session->userdata('no');
        $this->load->library('upload');
        $config['upload_path'] = './dist/berkas'; //path folder
        $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['overwrite'] = TRUE;
        $config['max_size'] = '2048'; //maksimum besar file 2M
       // $config['max_width']  = '1600'; //lebar maksimum 1288 px
      //  $config['max_height']  = '1200'; //tinggi maksimu 768 px
        $dbfoto = $no;
        $config['file_name'] = "ijazah_dp_".$dbfoto;
      

        $this->upload->initialize($config);
        
        if($_FILES['fupload']['name'])
        {
            if ($this->upload->do_upload('fupload'))
            {
                $gbr = $this->upload->data();
                $data=explode("/", $gbr['file_type']);
                $type=$data[1];
                $data = array(
                  'ijazah_depan' =>"ijazah_dp_".$dbfoto.".".$type
                );
                $this->upload_model->get_insert($data,$no);
                redirect('klien/profile'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                redirect(''); //jika gagal maka akan ditampilkan form upload
            }
        }
    }

     public function ijazah_blk_insert(){
        $no = $this->session->userdata('no');
        $this->load->library('upload');
        $config['upload_path'] = './dist/berkas'; //path folder
        $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['overwrite'] = TRUE;
        $config['max_size'] = '2048'; //maksimum besar file 2M
       // $config['max_width']  = '1600'; //lebar maksimum 1288 px
      //  $config['max_height']  = '1200'; //tinggi maksimu 768 px
        $dbfoto = $no;
        $config['file_name'] = "ijazah_blk_".$dbfoto;
      

        $this->upload->initialize($config);
        
        if($_FILES['fupload']['name'])
        {
            if ($this->upload->do_upload('fupload'))
            {
                $gbr = $this->upload->data();
                $data=explode("/", $gbr['file_type']);
                $type=$data[1];
                $data = array(
                  'ijazah_depan' =>"ijazah_blk_".$dbfoto.".".$type
                );
                $this->upload_model->get_insert($data,$no);
                redirect('klien/profile'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                redirect(''); //jika gagal maka akan ditampilkan form upload
            }
        }
    }


     public function berkas1_insert(){
        $no = $this->session->userdata('no');
        $this->load->library('upload');
        $config['upload_path'] = './dist/berkas'; //path folder
        $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['overwrite'] = TRUE;
        $config['max_size'] = '2048'; //maksimum besar file 2M
       // $config['max_width']  = '1600'; //lebar maksimum 1288 px
      //  $config['max_height']  = '1200'; //tinggi maksimu 768 px
        $dbfoto = $no;
        $config['file_name'] = "berkas1_".$dbfoto;
      

        $this->upload->initialize($config);
        
        if($_FILES['fupload']['name'])
        {
            if ($this->upload->do_upload('fupload'))
            {
                $gbr = $this->upload->data();
                $data=explode("/", $gbr['file_type']);
                $type=$data[1];
                $data = array(
                  'berkas1' =>"berkas1_".$dbfoto.".".$type
                );
                $this->upload_model->get_insert($data,$no);
                redirect('klien/profile'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                redirect(''); //jika gagal maka akan ditampilkan form upload
            }
        }
    }

    public function berkas_pi_insert(){
        $no = $this->session->userdata('no');
        $this->load->library('upload');
        $config['upload_path'] = './dist/berkas'; //path folder
        $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['overwrite'] = TRUE;
        $config['max_size'] = '2048'; //maksimum besar file 2M
       // $config['max_width']  = '1600'; //lebar maksimum 1288 px
      //  $config['max_height']  = '1200'; //tinggi maksimu 768 px
        $dbfoto = $no;
        $config['file_name'] = $dbfoto;
      

        $this->upload->initialize($config);
        
        if($_FILES['fupload']['name'])
        {
            if ($this->upload->do_upload('fupload'))
            {
                $gbr = $this->upload->data();
                $data=explode("/", $gbr['file_type']);
                $type=$data[1];
                $data = array(
                  'cv' =>$dbfoto.".".$type
                );
                $this->upload_model->get_insert($data,$no);
                redirect(''); //jika berhasil maka akan ditampilkan view vupload
            }else{
                redirect(''); //jika gagal maka akan ditampilkan form upload
            }
        }
    } 
}