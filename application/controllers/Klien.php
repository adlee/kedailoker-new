<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klien extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('klien_model');
		$this->load->model('admin_model');
	}

	public function index(){
		$no = $this->session->userdata('no');
		$datax['pend']= $this->klien_model->get_pendidikan($no);
		$this->load->view('klien/ambilpend', $datax);
		$pendi = $this->session->userdata('pendi');
		//echo $pendi;
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['lamar']=$this->klien_model->show_lamaran($no);
		$data['lowongseven']=$this->klien_model->show_lowonganseven($no,$pendi);
		$data['lowongsevenluar']=$this->klien_model->show_lowongansevenluar($no,$pendi);
		$data['lowong']=$this->klien_model->show_lowongan($no,$pendi);
		$data['pesan']=$this->klien_model->show_pesan($no);
		$data['jobfair']=$this->klien_model->show_jobfair();
		$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/index', $data);
		$this->load->view('klien/modal',$data);
	}


	public function cari(){
		$no = $this->session->userdata('no');
		$datax['pend']= $this->klien_model->get_pendidikan($no);
		$this->load->view('klien/ambilpend', $datax);
		$pendi = $this->session->userdata('pendi');
		//echo $pendi;
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['lamar']=$this->klien_model->show_lamaran($no);
		$data['lowongseven']=$this->klien_model->show_lowonganseven($no,$pendi);
		$data['lowongsevenluar']=$this->klien_model->show_lowongansevenluar($no,$pendi);
		$data['lowong']=$this->klien_model->show_lowongan($no,$pendi);
		$data['cari']=$this->klien_model->show_cari($no,$pendi);
		$data['pesan']=$this->klien_model->show_pesan($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$data['jobfair']=$this->klien_model->show_jobfair();
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/cari', $data);
		$this->load->view('klien/modal',$data);
	}

	public function settingakun(){
		$no = $this->session->userdata('no');
		$datax['pend']= $this->klien_model->get_pendidikan($no);
		$this->load->view('klien/ambilpend', $datax);
		$pendi = $this->session->userdata('pendi');
		//echo $pendi;
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/setting', $data);
		$this->load->view('klien/modal',$data);
	}

	public function help(){
		$no = $this->session->userdata('no');
		$datax['pend']= $this->klien_model->get_pendidikan($no);
		$this->load->view('klien/ambilpend', $datax);
		$pendi = $this->session->userdata('pendi');
		//echo $pendi;
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/bantuan', $data);
		$this->load->view('klien/modal',$data);
	}

	public function show_pesan(){
		$no = $this->session->userdata('no');
		$datax['pend']= $this->klien_model->get_pendidikan($no);
		$this->load->view('klien/ambilpend', $datax);
		$id=$this->session->set_userdata('id');
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
		$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/pesan',$data);
		$this->load->view('klien/modal',$data);
	}

	public function status_update(){
		$no = $this->session->userdata('no');
		$datax['pend']= $this->klien_model->get_pendidikan($no);
		$this->load->view('klien/ambilpend', $datax);
		$id=$this->session->set_userdata('id');
		$data['status'] = $this->klien_model->get_status($no);
		$data['bank'] = $this->admin_model->get_bank();
		$data['tlp'] = $this->admin_model->get_tlp();
		$data['user']=$this->klien_model->show_klien($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
		$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/statuspembayaran',$data);
		$this->load->view('klien/modal',$data);
	}


	public function bacapesan($id_pesan){
		$no = $this->session->userdata('no');
		$datax['pend']= $this->klien_model->get_pendidikan($no);
		$this->load->view('klien/ambilpend', $datax);
		$id=$this->session->set_userdata('id');
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->klien_model->baca($id_pesan);
		
		//$this->load->view('klien/headerklien', $data);
		//$this->load->view('klien/pesan',$data);
		//$this->load->view('klien/modal',$data);
	}
	
public function detaillowongseven($id){
		$no = $this->session->userdata('no');
		$datax['pend']= $this->klien_model->get_pendidikan($no);

		$this->load->view('klien/ambilpend', $datax);

		$pendi = $this->session->userdata('pendi');
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['lamar']=$this->klien_model->show_lamaran($no);
		$data['lowongseven']=$this->klien_model->show_lowonganseven($no,$pendi);
		$data['lowongsevenluar']=$this->klien_model->show_lowongansevenluar($no,$pendi);
		$data['det_loker']=$this->klien_model->get_loker_id($id);
		$data['idp']=$this->klien_model->get_id_pencari($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
		$data['jobfair']=$this->klien_model->show_jobfair();
		$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$data['lowong']=$this->klien_model->show_lowongan($no,$pendi);
		$data['pendi'] = $pendi;
		
		// print_r($data['idp']);
		// print_r($data['det_loker']);
		// die();
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/detlowongseven', $data,$pendi);
		$this->load->view('klien/modal',$data);
}

public function detailjobfair($id){
	$no = $this->session->userdata('no');
		$datax['pend']= $this->klien_model->get_pendidikan($no);
		$this->load->view('klien/ambilpend', $datax);
		$pendi = $this->session->userdata('pendi');
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['lamar']=$this->klien_model->show_lamaran($no);
		$data['lowongseven']=$this->klien_model->show_lowonganseven($no,$pendi);
		$data['lowongsevenluar']=$this->klien_model->show_lowongansevenluar($no,$pendi);
		$data['det_loker']=$this->klien_model->get_loker_id($id);
		$data['job']=$this->klien_model->get_jobfair_id($id);
		$data['idp']=$this->klien_model->get_id_pencari($no);
		$data['jobfair']=$this->klien_model->show_jobfair();
		$data['pesan']=$this->klien_model->show_pesan($no);
		$data['lowong']=$this->klien_model->show_lowongan($no,$pendi);
		$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);

		$data['lisjob']=$this->klien_model->lisjob($id);

		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/detjobfair', $data);
		$this->load->view('klien/modal',$data);
		
}
	

	public function login(){
		$hp = url_title( $this->input->post('hp'), 'dash', TRUE);
		$password = url_title( $this->input->post('sandi'), 'dash', TRUE);
		$data = $this->klien_model->get_login($hp , $password);
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama','nama','required');
		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('nomor','hp','required');
		$this->form_validation->set_rules('repeatnomor','rehp','required');
		$this->form_validation->set_rules('pass','pass','required');
		$this->form_validation->set_rules('konfirmSandi','kof','required');
		$this->form_validation->set_rules('tanggal','tgl','required');
		$this->form_validation->set_rules('bulan','bln','required');
		$this->form_validation->set_rules('tahun','thn','required');
		$this->form_validation->set_rules('jkelamin','jk','required');
		$this->form_validation->set_rules('pend','pend','required');

		$this->form_validation->set_rules('alamat','alamat','required');
		$this->form_validation->set_rules('provinsi','provinsi','required');
		$this->form_validation->set_rules('kabupaten','kabupaten','required');
		$this->form_validation->set_rules('kecamatan','kecamatan','required');



		if ($this->form_validation->run() == FALSE) {
			redirect('');
		}else{
			$tgl 	= $this->input->post('tanggal');
			$bln 	= $this->input->post('bulan');
			$thn 	= $this->input->post('tahun');

			$data = array(	'username' => $this->input->post('nama'),
							'password' =>  $this->input->post('konfirmSandi'),
							'email' => $this->input->post('email'),
							'telp' => $this->input->post('repeatnomor'),
							'ttl' =>  "$thn-$bln-$tgl",
							'pend_terakhir' => $this->input->post('pend'),
							'jk' => $this->input->post('jkelamin'),
							'alamat' => $this->input->post('alamat'),
							'idprov' =>  $this->input->post('provinsi'),
							'idkab' => $this->input->post('kabupaten'),
							'idkec' => $this->input->post('kecamatan')
			);
			
			$this->klien_model->add_klien($data);
			$telepon = url_title( $this->input->post('nomor'), 'dash', TRUE);
			$get_session = $this->klien_model->get_session_tandingan($telepon);
			if($get_session == 0) {
				redirect('adminkl1011');
			}
			else {
				// redirect('klien/pembayaran/keanggotaan');
				$this->pembayaran_keanggotaan();
			}
		}
	}

	public function input_pendidikan(){
		$no = $this->session->userdata('no');
		$this->load->helper('form');
		$this->load->library('form_validation');

		
			$this->klien_model->add_pendidikan($no);

			//$telepon = url_title( $this->input->post('nomor'), 'dash', TRUE);
			//$this->klien_model->get_session($telepon);
		
	}
	public function input_pengalaman(){
		$no = $this->session->userdata('no');
		$this->load->helper('form');
		$this->load->library('form_validation');

		
			$this->klien_model->add_pengalaman($no);

			//$telepon = url_title( $this->input->post('nomor'), 'dash', TRUE);
			//$this->klien_model->get_session($telepon);
		
	}

	public function pembayaran_keanggotaan(){
		// $data['bank'] = $this->admin_model->get_bank();
		// $data['tlp'] = $this->admin_model->get_tlp();
		$no = $this->session->userdata('no');
		$titlehead['titlehead'] = "";
		$this->load->view('klien/header', $titlehead);
		$this->load->view('klien/aktivasi', $no);
		$this->load->view('klien/footer');
	}

	public function pembayaran_keanggotaan_premium(){
		$data['bank'] = $this->admin_model->get_bank();
		$data['tlp'] = $this->admin_model->get_tlp();
		$titlehead['titlehead'] = "";
		$this->load->view('klien/header', $titlehead);
		$this->load->view('klien/pembayaran_keanggotaan', $data);
	}

	public function pembayaran_keanggotaan_biasa(){
		$no = $this->session->userdata('no');
		$data['data'] = $this->klien_model->get_id_pencari($no);
		$titlehead['titlehead'] = "";
		$this->load->view('klien/header', $titlehead);
		$this->load->view('klien/aktivasi_biasa',$data);
		$this->load->view('klien/footer');
	}

	public function up_stat_pencaker($status=0){
		$data_status['nohp'] = $this->session->userdata('no');
		$data_status['status'] = $this->input->get('status');
		$this->klien_model->up_pencaker_free_pending($data_status);
		redirect('klien/login');
	}

	public function pembersihan_session() {
		$this->session->sess_destroy();
		redirect('home','refresh');
	}

	public function tambahkuota(){
		$data['bank'] = $this->admin_model->get_bank();
		$data['tlp'] = $this->admin_model->get_tlp();
		$this->load->view('klien/tambahkuota', $data);
	}

	public function bayar(){
		$no = $this->session->userdata('no');
		$data['info'] = $this->klien_model->get_payment($no);
		$data['bank'] = $this->admin_model->get_bank();
		$data['tlp'] = $this->admin_model->get_tlp();
		$titlehead['titlehead'] = "";
		$this->load->view('klien/header', $titlehead);
		$this->load->view('klien/pending_klien', $data);
	}

	public function pending(){
		$no = $this->session->userdata('no');
		$data['info'] = $this->klien_model->cek_payment($no);
		$data['bank'] = $this->admin_model->get_bank();
		$data['tlp'] = $this->admin_model->get_tlp();
		$titlehead['titlehead'] = "(menunggu konfirmasi admin)";
		$this->load->view('klien/header', $titlehead);
		$this->load->view('klien/pending_klien', $data);
	}

	public function form_lamaran(){
		$this->load->view('klien/form_lamaran');
	}
	public function profile(){
		$no = $this->session->userdata('no');
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		
		$data['pend']=$this->klien_model->show_pendidikan($no);
		$data['peng']=$this->klien_model->show_pengalaman($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/profile',$data);
		$this->load->view('klien/modal',$data);
	}
	public function tambahpk(){
		$no = $this->session->userdata('no');
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
		
		$data['pend']=$this->klien_model->show_pendidikan($no);
		$data['peng']=$this->klien_model->show_pengalaman($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/inputpengalaman',$data);
		$this->load->view('klien/modal',$data);
	}

	public function tambahpi(){
		$no = $this->session->userdata('no');
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
		
		$data['pend']=$this->klien_model->show_pendidikan($no);
		$data['peng']=$this->klien_model->show_pengalaman($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/inputpendidikan',$data);
		$this->load->view('klien/modal',$data);
	}

	public function profilepksukses(){
		$no = $this->session->userdata('no');
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pend']=$this->klien_model->show_pendidikan($no);
		$data['peng']=$this->klien_model->show_pengalaman($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/editpksukses',$data);
		$this->load->view('klien/modal',$data);
	}

	public function profilepisukses(){
		$no = $this->session->userdata('no');
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pend']=$this->klien_model->show_pendidikan($no);
		$data['peng']=$this->klien_model->show_pengalaman($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/editpisukses',$data);
		$this->load->view('klien/modal',$data);
	}
	public function edit_profile(){
		$no = $this->session->userdata('no');
		//$data['idp'] = $this->klien_model->get_id($no);
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pend']=$this->klien_model->show_pendidikan($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/editprofile',$data);
		$this->load->view('klien/modal',$data);
	}

	public function edit_profile_execute(){
		$no = $this->session->userdata('no');
		$this->load->helper('form');
		$this->load->library('form_validation');

		
			$this->klien_model->edit_profile($no);
	}

	public function edit_akun_execute(){
		$no = $this->session->userdata('no');
		$this->load->helper('form');
		$this->load->library('form_validation');

		
			$this->klien_model->edit_akun($no);
	}

	public function edit_pk($id_pencari){
		$no = $this->session->userdata('no');
		//$data['idp'] = $this->klien_model->get_id($no);
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['pend']=$this->klien_model->show_pendidikan($no);
		$data['peng']=$this->klien_model->show_each_pengalaman($no, $id_pencari);
		$data['pesan']=$this->klien_model->show_pesan($no);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/profileeditpk',$data);
		$this->load->view('klien/modal',$data);
	}

	public function edit_pk_execute($id){
		$no = $this->session->userdata('no');
		$this->load->helper('form');
		$this->load->library('form_validation');

		
			$this->klien_model->edit_pengalaman($id);
	}

	public function edit_pend($id_pencari){
		$no = $this->session->userdata('no');
		//$data['idp'] = $this->klien_model->get_id($no);
		$data['status'] = $this->klien_model->get_status($no);
		$data['user']=$this->klien_model->show_klien($no);
		$data['peng']=$this->klien_model->show_pengalaman($no);
		$data['pend']=$this->klien_model->show_each_pendidikan($no, $id_pencari);
			$data['pesanbaru']=$this->klien_model->show_pesan_baru($no);
		$data['pesan']=$this->klien_model->show_pesan($no);
		$this->load->view('klien/headerklien', $data);
		$this->load->view('klien/profileditpend',$data);
		$this->load->view('klien/modal',$data);
	}

	public function edit_pend_execute($id){
		$no = $this->session->userdata('no');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->klien_model->edit_pendidikan($id);
	}

	public function deletependidikan($id){
		$this->klien_model->deletependidikan($id);
		redirect('klien/profile-edit-pi-sukses');
	}
	public function deletepengalaman($id){
		$this->klien_model->deletepengalaman($id);
		redirect('klien/profile-edit-pk-sukses');
	}
	
	public function insertlowong($id_pencari,$id_post){
		$sql = $this->db->query("select * from lamaran where id_pencari = '$id_pencari' and id_low = '$id_post' and status='menunggu'")->num_rows();
		if ($sql >=1) {
			redirect('klien/home');
		}else{
			$this->klien_model->insertlowong($id_pencari,$id_post);
			redirect('klien/home');
		}

	}

	public function daftar_jf(){
		$no = $this->session->userdata('no');
		$data['user']=$this->klien_model->show_klien($no);
		$data['jobfair']=$this->klien_model->get_jobfair();
		$data['informal']=$this->klien_model->get_informal($no);
		$data['pengalaman']=$this->klien_model->get_pengalaman($no);
		$this->load->view('klien/form_jobfair', $data);
	}
	public function daftar_jf_exc(){
		$no = $this->session->userdata('no');
		$this->klien_model->join_jf($no);
		$data['join']=$this->klien_model->get_join($no);
		$this->load->view('klien/jf_joined', $data);
	}

	public function daftar_jf_umum(){
		//$no = $this->session->userdata('no');
		
		$data['jobfair']=$this->klien_model->get_jobfair();	
		
		$this->load->view('klien/form_jobfair_umum', $data);
	}

	public function tiket(){
		$no = $this->session->userdata('no');
		$this->klien_model->join_jf($no);
		$data['join']=$this->klien_model->get_join($no);
		$this->load->view('pdf/tiket', $data);
	}
	public function tiketumum(){
			
	$this->klien_model->join_jf();
		$this->load->view('pdf/tiketumum');
	}

	public function tambahkuota_exc(){
		$no = $this->session->userdata('no');
		$this->klien_model->tambah_kuota($no);

	}
	
}
