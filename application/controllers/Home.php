<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('klien_model');
	}

	public function index(){
		$this->load->view('header_awal');
		$this->load->view('index');
	}

	public function register(){
		$sql['sql'] = $this->klien_model->get_provinsi();
		$this->load->view('header_awal',$sql);
		$this->load->view('register',$sql);
	}

	public function daftar_jobfair(){
		$this->load->view('jobfair/form_daftar');
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('');
	}

	public function ambil_data(){
		$modul=$this->input->post('modul');
		$id=$this->input->post('id');
		$idkab=$this->input->post('idkab');
		if($modul=="kabupaten"){
			echo $this->klien_model->get_kabupaten($id);
		}else if($modul=="kecamatan"){
			echo $this->klien_model->get_kecamatan($id,$idkab);
		}
	}

	public function syarat_ketentuan_pencaker() {
		$this->load->view('header_awal');
		$this->load->view('sk_pencaker');
	}

	public function syarat_ketentuan_perusahaan() {
		$this->load->view('header_awal');
		$this->load->view('sk_perusahaan');
	}

	public function faq_pencaker() {
		$this->load->view('header_awal');
		$this->load->view('faq_perusahaan');
	}

	public function faq_perusahaan() {
		$this->load->view('header_awal');
		$this->load->view('faq_perusahaan');
	}

	public function kebijakan_privasi() {
		$this->load->view('header_awal');
		$this->load->view('kebijakan_privasi');	
	}
}
