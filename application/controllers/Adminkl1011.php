<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkl1011 extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url_helper');
		$this->load->helper('url');
		$this->load->model('admin_model');
		$this->load->model('klien_model');
		$this->load->model('perusahaan_model');
		$this->load->library('upload');
	}

	public function index(){	
		$this->load->view('admin/index');
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
	}
	
	public function login(){
		$username = url_title( $this->input->post('username'), 'dash', TRUE);
		$password = url_title( $this->input->post('password'), 'dash', TRUE);
		$data = $this->admin_model->get_admin($username , $password);
	}

	public function home(){
		$cek['tanggal']                       = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);

		$data1['count_jobfair']               = $this->perusahaan_model->get_jobfair();
		$data1['count_klien']                 = $this->klien_model->get_count();
		$data1['count_new_klien']             = $this->klien_model->get_count_new();
		$data1['count_perusahaan']            = $this->perusahaan_model->get_count();
		$data1['count_new_perusahaan']        = $this->perusahaan_model->get_count_new();
		$data1['per']                         = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending']               = $this->perusahaan_model->get_loker_pending();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();

		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/home');
		$this->load->view('admin/support/admin_footer');
	}

	public function semua_klien(){
		$cek['tanggal']                       = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair']               = $this->perusahaan_model->get_jobfair();
		$data1['count_klien']                 = $this->klien_model->get_count();
		$data1['count_new_klien']             = $this->klien_model->get_count_new();
		$data1['count_perusahaan']            = $this->perusahaan_model->get_count();
		$data1['per']                         = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending']               = $this->perusahaan_model->get_loker_pending();
		$data1['count_new_perusahaan']        = $this->perusahaan_model->get_count_new();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['klien']                       = $this->admin_model->get_klien();

		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/klien/data_semua_klien', $data2);
		$this->load->view('admin/support/admin_footer');
	}

	public function klien_detail($id_klien){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['klien'] = $this->admin_model->get_klien();
		$data2['klien_detail'] = $this->klien_model->get_klien_detail($id_klien);

		// print_r($this->klien_model->get_klien_detail($id_klien));
		// die();
		$this->load->view('admin/aktif_lowongan', $cek);
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/klien/klien_detail', $data2);
		$this->load->view('admin/support/admin_footer');
	}
	
	public function klien_baru(){
		$cek['tanggal']                       = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);

		$data1['count_jobfair']               = $this->perusahaan_model->get_jobfair();
		$data1['count_klien']                 = $this->klien_model->get_count();
		$data1['count_new_klien']             = $this->klien_model->get_count_new();
		$data1['count_perusahaan']            = $this->perusahaan_model->get_count();
		$data1['per']                         = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending']               = $this->perusahaan_model->get_loker_pending();
		$data1['count_new_perusahaan']        = $this->perusahaan_model->get_count_new();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['klien']                       = $this->klien_model->get_new_klien();

		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/klien/data_klien_baru', $data2);
		$this->load->view('admin/support/admin_footer');
	}

	public function klien_on($id, $sms){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$this->klien_model->set_on($id, $sms);
		redirect('adminkl1012/klien/baru');
	}

	public function klien_delete($id){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$this->klien_model->delete($id);
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['klien'] = $this->klien_model->get_klien();
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/klien/data_semua_klien', $data2);
		$this->load->view('admin/support/admin_footer');
	}

	public function klien_new_detail($id){
		$cek['tanggal']                       = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		
		$data1['count_jobfair']               = $this->perusahaan_model->get_jobfair();
		$data1['count_klien']                 = $this->klien_model->get_count();
		$data1['count_new_klien']             = $this->klien_model->get_count_new();
		$data1['count_perusahaan']            = $this->perusahaan_model->get_count();
		$data1['per']                         = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending']               = $this->perusahaan_model->get_loker_pending();
		$data1['count_new_perusahaan']        = $this->perusahaan_model->get_count_new();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		 
		$data2['klien'] = $this->klien_model->detail($id);
		
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/klien/detail_klien', $data2);
		$this->load->view('admin/support/admin_footer');
	}

	public function klien_set_kuota($sts = FALSE, $sms = FALSE){
		if (($sts == FALSE) and ($sms == FALSE)) {
			$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
			$this->load->view('admin/aktif_lowongan', $cek);
			$data['off'] = $this->admin_model->count_off();
			$data['warning'] = $this->admin_model->count_warning();
			$data['on'] = $this->admin_model->count_on();
			$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
			$data1['count_klien'] = $this->klien_model->get_count();
			$data1['count_new_klien'] = $this->klien_model->get_count_new();
			$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
			$data1['per'] = $this->perusahaan_model->get_perusahaan();
			$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
			$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
			$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
			$this->load->view('admin/support/admin_header', $data1);
			$this->load->view('admin/klien/set_kuota', $data);
			$this->load->view('admin/support/admin_footer');	
		}else{
			$this->admin_model->update_sms($sts, $sms);
			redirect('adminkl1012/klien/setkuota');
		}
	}

	public function klien_set_kuota_all(){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/klien/form_set_kuota');
		$this->load->view('admin/support/admin_footer');
	}

	public function klien_set_kuota_process(){
		$this->admin_model->update_sms_all();
		redirect('adminkl1012/klien/setkuota');
	}

	public function klien_set_kuota_filter_pendidikan($sts){
		$x=$this->input->post('pendidikan');
		if ($sts=='on') {
			if ($x=='all') {
				$data = $this->db->query("SELECT * from pencari_lowongan where kuota_sms > 5 ")->result();	
			}else{
				$data = $this->db->query("SELECT * from pencari_lowongan where kuota_sms > 5 and pend_terakhir = '$x' ")->result();
			}
			$hasil="";
			if($data) {
				$hasil.='<form method="post" action="<?php echo base_url();?>adminkl1012/klien/kuota/on/csv/<?php echo $jml_on ?>" target="blank">'.
		                  '<input type="hidden" name="nama" value="0">'.
		                  '<input type="hidden" name="telepon" value="0">'.
		                  '<input type="hidden" name="pendidikan" value="0">'.
		                  '<input type="hidden" name="kuota" value="0">'.
		                  '<tr style="color:black;text-align:left;font-weight:bold;background:#ABFFAB">'.
		                    '<td class="column-check-on" style="text-align:center;border-right:4px solid #eee;border-bottom:4px solid #eee;padding:5px 10px;border-top:none;margin-right:10px"><input class="check-all-on" type="checkbox" checked></td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Nama &nbsp <input type="checkbox" class="column-check-on" value="cek" name="nama" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Telepon &nbsp <input type="checkbox" class="column-check-on" value="cek" name="telepon" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Pendidikan &nbsp <input type="checkbox" class="column-check-on" value="cek" name="pendidikan" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Kuota &nbsp <input type="checkbox" class="column-check-on" value="cek" name="kuota" checked>'.
		                    '</td>';
		        $no=1; foreach ($data as $klien_on) {
				$hasil.=	'<tr>'.
							'<td align="center" style="padding:5px 10px;border-top:none;border-right:4px solid #eee">'.
		                      '<input type="checkbox" class="column-check-on" value="'.$klien_on->id.'" name="cek'.$no++.'" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->username .'</td>  '.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->telp .'</td>  '.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->pend_terakhir .'</td>  '.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->kuota_sms .'</td></tr> ';
		    	}
				echo $hasil;
			}else{
				$hasil.='<tr>'.
							'<td>Tidak ditemukan data pendidikan '.$x.'</td>'.
						'</tr>';
				echo $hasil;
			}
		}else if ($sts=='warning') {
			if ($x=='all') {
				$data = $this->db->query("SELECT * from pencari_lowongan where kuota_sms > 0 and kuota_sms < 6 ")->result();	
			}else{
				$data = $this->db->query("SELECT * from pencari_lowongan where kuota_sms > 0 and kuota_sms < 6 and pend_terakhir = '$x' ")->result();
			}
			$hasil="";
			if($data) {
				$hasil.='<form method="post" action="<?php echo base_url();?>adminkl1012/klien/kuota/warning/csv/<?php echo $jml_warning ?>" target="blank">'.
		                  '<input type="hidden" name="nama" value="0">'.
		                  '<input type="hidden" name="telepon" value="0">'.
		                  '<input type="hidden" name="pendidikan" value="0">'.
		                  '<input type="hidden" name="kuota" value="0">'.
		                  '<tr style="color:black;text-align:left;font-weight:bold;background:#E6E680">'.
		                    '<td class="column-check-warning" style="text-align:center;border-right:4px solid #eee;border-bottom:4px solid #eee;padding:5px 10px;border-top:none;margin-right:10px"><input class="check-all-warning" type="checkbox" checked></td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Nama &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="nama" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Telepon &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="telepon" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Pendidikan &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="pendidikan" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Kuota &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="kuota" checked>'.
		                    '</td>';
		        $no=1; foreach ($data as $klien_on) {
				$hasil.=	'<tr>'.
							'<td align="center" style="padding:5px 10px;border-top:none;border-right:4px solid #eee">'.
		                      '<input type="checkbox" class="column-check-warning" value="'.$klien_on->id.'" name="cek'.$no++.'" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->username .'</td>  '.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->telp .'</td>  '.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->pend_terakhir .'</td>  '.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->kuota_sms .'</td></tr> ';
		    	}
				echo $hasil;
			}else{
				$hasil.='<tr>'.
							'<td>Tidak ditemukan data pendidikan '.$x.'</td>'.
						'</tr>';
				echo $hasil;
			}
		}
		else if ($sts=='off') {
			if ($x=='all') {
				$data = $this->db->query("SELECT * from pencari_lowongan where kuota_sms = 0 ")->result();	
			}else{
				$data = $this->db->query("SELECT * from pencari_lowongan where kuota_sms = 0  and pend_terakhir = '$x' ")->result();
			}
			$hasil="";
			if($data) {
				$hasil.='<form method="post" action="<?php echo base_url();?>adminkl1012/klien/kuota/off/csv/<?php echo $jml_off ?>" target="blank">'.
		                  '<input type="hidden" name="nama" value="0">'.
		                  '<input type="hidden" name="telepon" value="0">'.
		                  '<input type="hidden" name="pendidikan" value="0">'.
		                  '<input type="hidden" name="kuota" value="0">'.
		                  '<tr style="color:black;text-align:left;font-weight:bold;background:#FF8C8C">'.
		                    '<td class="column-check-off" style="text-align:center;border-right:4px solid #eee;border-bottom:4px solid #eee;padding:5px 10px;border-top:none;margin-right:10px"><input class="check-all-off" type="checkbox" checked></td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Nama &nbsp <input type="checkbox" class="column-check-off" value="cek" name="nama" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Telepon &nbsp <input type="checkbox" class="column-check-off" value="cek" name="telepon" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Pendidikan &nbsp <input type="checkbox" class="column-check-off" value="cek" name="pendidikan" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">'.
		                      'Kuota &nbsp <input type="checkbox" class="column-check-off" value="cek" name="kuota" checked>'.
		                    '</td>';
		        $no=1; foreach ($data as $klien_on) {
				$hasil.=	'<tr>'.
							'<td align="center" style="padding:5px 10px;border-top:none;border-right:4px solid #eee">'.
		                      '<input type="checkbox" class="column-check-off" value="'.$klien_on->id.'" name="cek'.$no++.'" checked>'.
		                    '</td>'.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->username .'</td>  '.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->telp .'</td>  '.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->pend_terakhir .'</td>  '.
		                    '<td style="padding:5px 10px;border-top:none">'.$klien_on->kuota_sms .'</td></tr> ';
		    	}
				echo $hasil;
			}else{
				$hasil.='<tr>'.
							'<td>Tidak ditemukan data pendidikan '.$x.'</td>'.
						'</tr>';
				echo $hasil;
			}
		}
	}

	public function klien_update_kuota($id=FALSE, $sekarang=FALSE, $sms=FALSE){
		if (($id==FALSE) and ($sekarang==FALSE) and ($sms==FALSE)) {
			$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
			$this->load->view('admin/aktif_lowongan', $cek);
			$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
			$data1['count_klien'] = $this->klien_model->get_count();
			$data1['count_new_klien'] = $this->klien_model->get_count_new();
			$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
			$data1['per'] = $this->perusahaan_model->get_perusahaan();
			$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
			$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
			$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
			$data2['update'] = $this->klien_model->get_update();
			$this->load->view('admin/support/admin_header', $data1);
			$this->load->view('admin/klien/data_update_kuota', $data2);
			$this->load->view('admin/support/admin_footer');
	}else if (($sekarang==FALSE) and ($sms==FALSE)) {
		$this->klien_model->delete_permintaan_update($id);
			redirect('adminkl1012/klien/updatekuota');		
		}else {
			$this->klien_model->proses_permintaan_update($id, $sekarang, $sms);
			redirect('adminkl1012/klien/updatekuota');		
		}	
	}

	public function data_perusahaan(){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['perusahaan'] = $this->perusahaan_model->get_perusahaan();
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/perusahaan/data_semua_perusahaan', $data2);
		$this->load->view('admin/support/admin_footer');
	}
	public function set_off_perusahaan($id){
		$this->perusahaan_model->set_off_perusahaan($id);
		redirect('adminkl1012/perusahaan/data');
	}
	public function set_on_perusahaan($id){
		$this->perusahaan_model->set_on_perusahaan($id);
		redirect('adminkl1012/perusahaan/data');
	}
	public function delete_perusahaan($id){
		$this->perusahaan_model->delete_perusahaan($id);
		redirect('adminkl1012/perusahaan/data');
	}
	public function delete_posting($id_perus, $id_low){
		$this->perusahaan_model->delete_posting($id_low);
		redirect("adminkl1012/perusahaan/".$id_perus);
	}

	public function perusahaan($id){

		$cek['tanggal']                       = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);

		$data1['count_jobfair']               = $this->perusahaan_model->get_jobfair();
		$data1['count_klien']                 = $this->klien_model->get_count();
		$data1['count_new_klien']             = $this->klien_model->get_count_new();
		$data1['count_perusahaan']            = $this->perusahaan_model->get_count();
		$data1['per']                         = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending']               = $this->perusahaan_model->get_loker_pending();
		$data2['perusahaan']                  = $this->perusahaan_model->get_perusahaan($id);
		$data2['loker']                       = $this->perusahaan_model->get_loker_id($id);
		$data1['count_new_perusahaan']        = $this->perusahaan_model->get_count_new();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['pelamar']                     =$this->perusahaan_model->get_pelamar();
		$data2['pendidikan']                  =$this->perusahaan_model->get_pend_pelamar();

		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/perusahaan/perusahaan_terpilih', $data2);
		$this->load->view('admin/support/admin_footer');

	}

	public function update_data_perusahaan() {
		
		$update = $this->admin_model->update_data_perusahaan();

		if($update == 1)
			redirect('adminkl1012/perusahaan/'.$this->input->post('id_p'));
		else 
			echo 'err';
	}

	public function loker_perusahaan($idp, $id_post){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['perusahaan'] = $this->perusahaan_model->get_perusahaan($idp);
		$data2['loker'] = $this->perusahaan_model->get_loker_selected($id_post);
		$data2['pilih'] = $this->perusahaan_model->get_lowongan_idpost($id_post);
		$data2['pelamar'] = $this->perusahaan_model->get_pelamar_idlow($id_post);
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/perusahaan/perusahaan_loker_terpilih', $data2);
		$this->load->view('admin/support/admin_footer');
	}
	public function share_loker_perusahaan($idp, $id_post, $pend, $bayar){
		$this->perusahaan_model->share_posting($id_post, $bayar);
		//exc decrease on generate csv
		//$this->perusahaan_model->decrease_sms($pend);
		redirect("adminkl1012/perusahaan/".$idp);
	}

	public function data_jobfair(){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['jf'] = $this->admin_model->get_jf();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/jobfair/data_semua_jobfair');
		$this->load->view('admin/support/admin_footer');
	}

	public function create_jobfair(){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/jobfair/create_jobfair');
		$this->load->view('admin/support/admin_footer');
	}

	public function create_loker()
	{
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair'] 				= $this->perusahaan_model->get_jobfair();
		$data1['job'] 						= $this->klien_model->tampil_jobfair();

		if ($data1['job'] != '0') {
			$data1['jobfair'] = $this->klien_model->tampil_jobfair();
		}

		$data1['count_klien'] 					= $this->klien_model->get_count();
		$data1['count_new_klien'] 				= $this->klien_model->get_count_new();
		$data1['count_perusahaan'] 				= $this->perusahaan_model->get_count();
		$data1['count_new_perusahaan'] 			= $this->perusahaan_model->get_count_new();
		$data1['per'] 							= $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending'] 				= $this->perusahaan_model->get_loker_pending();
		$data1['count_permintaan_perusahaan'] 	= $this->perusahaan_model->get_permintaan_new();
		$this->load->view('admin/support/admin_header',$data1);
		$this->load->view('admin/loker/loker');
		$this->load->view('admin/support/admin_footer');
	}

	public function create_jobfair_exc($jml){
		$ret = $this->admin_model->set_jobfair($jml); 
		if($ret == 1)
			redirect('adminkl1012/jobfair/data');
	}

	public function tambah_loker(){
		$this->admin_model->set_loker(); 
		redirect('adminkl1012/loker/create','refresh');
	}

	public function delete_jobfair($id){
		$this->admin_model->delete_jobfair($id); 
		redirect('adminkl1012/jobfair/data');
	}
	public function edit_jobfair($id){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['jf'] = $this->admin_model->get_jf($id);
		$data2['jobfair_perusahaan'] = $this->perusahaan_model->get_jobfair_perusahaan($id);
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/jobfair/edit_jobfair',$data2);
		$this->load->view('admin/support/admin_footer');
	}
	public function edit_jobfair_exc($id, $jml){
		$this->admin_model->update_jobfair($id, $jml); 
		redirect('adminkl1012/jobfair/data');
	}
	public function perusahaan_baru(){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['perusahaan'] = $this->perusahaan_model->get_perusahaan();
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/perusahaan/data_semua_perusahaan', $data2);
		$this->load->view('admin/support/admin_footer');
	}
	public function csv_perusahaan_on($jml){
		$perusahaan   = $_POST['perusahaan'] ;
		$hrd   = $_POST['hrd'] ;
        $telepon  = $_POST['telepon'] ;
        $lowongan  = $_POST['lowongan'] ;
		$this->admin_model->perusahaan_on($jml, $perusahaan, $hrd, $telepon, $lowongan);
   		$this->load->view('admin/perusahaan/excel_status');
	}
	public function csv_perusahaan_warning($jml){
		$perusahaan   = $_POST['perusahaan'] ;
		$hrd   = $_POST['hrd'] ;
        $telepon  = $_POST['telepon'] ;
        $lowongan  = $_POST['lowongan'] ;
		$this->admin_model->perusahaan_warning($jml, $perusahaan, $hrd, $telepon, $lowongan);
    	$this->load->view('admin/perusahaan/excel_status');
	}
	public function csv_perusahaan_off($jml){
		$perusahaan   = $_POST['perusahaan'] ;
		$hrd   = $_POST['hrd'] ;
        $telepon  = $_POST['telepon'] ;
        $lowongan  = $_POST['lowongan'] ;
		$this->admin_model->perusahaan_off($jml, $perusahaan, $hrd, $telepon, $lowongan);
    	$this->load->view('admin/perusahaan/excel_status');
	}

	public function csv_on($jml){
		$nama   = $_POST['nama'] ;
        $telepon   = $_POST['telepon'] ;
        $pendidikan  = $_POST['pendidikan'] ;
        $kuota  = $_POST['kuota'] ;
        $this->admin_model->pencari_kuota_on($jml, $nama, $telepon, $pendidikan, $kuota);
		$this->load->view('admin/klien/excel_kuota');
	}
	public function csv_warning($jml){
		$nama   = $_POST['nama'] ;
        $telepon   = $_POST['telepon'] ;
        $pendidikan  = $_POST['pendidikan'] ;
        $kuota  = $_POST['kuota'] ;
		$this->admin_model->pencari_kuota_warning($jml, $nama, $telepon, $pendidikan, $kuota);
        $this->load->view('admin/klien/excel_kuota');
	}
	public function csv_off($jml){
		$nama   = $_POST['nama'] ;
        $telepon   = $_POST['telepon'] ;
        $pendidikan  = $_POST['pendidikan'] ;
        $kuota  = $_POST['kuota'] ;
		$this->admin_model->pencari_kuota_off($jml, $nama, $telepon, $pendidikan, $kuota);
        $this->load->view('admin/klien/excel_kuota');
	}
	public function csv_klien(){
		$this->admin_model->pencari_csv();
        $this->load->view('admin/klien/excel_kuota');
	}
	public function data_status_perusahaan(){
		$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
		$this->load->view('admin/aktif_lowongan', $cek);
		$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
		$data1['count_klien'] = $this->klien_model->get_count();
		$data1['count_new_klien'] = $this->klien_model->get_count_new();
		$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
		$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
		$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
		$data1['per'] = $this->perusahaan_model->get_perusahaan();
		$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
		$data2['perusahaan'] = $this->perusahaan_model->get_perusahaan_n_loker();
		$this->load->view('admin/support/admin_header', $data1);
		$this->load->view('admin/perusahaan/data_status_perusahaan', $data2);
		$this->load->view('admin/support/admin_footer');
	}
	public function permintaan_perusahaan(){
		
			$cek['tanggal'] = $this->admin_model->cek_masa_aktif_lowongan();
			$this->load->view('admin/aktif_lowongan', $cek);
			$data1['count_jobfair'] = $this->perusahaan_model->get_jobfair();
			$data1['count_klien'] = $this->klien_model->get_count();
			$data1['count_new_klien'] = $this->klien_model->get_count_new();
			$data1['count_perusahaan'] = $this->perusahaan_model->get_count();
			$data1['per'] = $this->perusahaan_model->get_perusahaan();
			$data1['loker_pending'] = $this->perusahaan_model->get_loker_pending();
			$data1['count_new_perusahaan'] = $this->perusahaan_model->get_count_new();
			$data1['count_permintaan_perusahaan'] = $this->perusahaan_model->get_permintaan_new();
			$data2['update'] = $this->admin_model->get_permintaan_perusahaan();
			$data2['per'] = $this->perusahaan_model->get_perusahaan();
			$this->load->view('admin/support/admin_header', $data1);
			$this->load->view('admin/perusahaan/permintaan_perusahaan', $data2);
			$this->load->view('admin/support/admin_footer');
	}
	public function proses_permintaan_perusahaan($id, $sekarang, $tambah){
		$this->admin_model->perpanjang_lowongan($id, $sekarang, $tambah);
		redirect('adminkl1012/perusahaan/permintaan/masa/aktif');		
	}
	public function hapus_permintaan_perusahaan($id){
		$this->admin_model->delete_permintaan_perpanjangan($id);
		redirect('adminkl1012/perusahaan/permintaan/masa/aktif');		
	}
}
