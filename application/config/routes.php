<?php

// Perusahaan

// alamat->text

// foto->foto

defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'home';
$route['logout']             = 'home/logout';
$route['jobfair/daftar']     = 'home/daftar_jobfair';
$route['faq_pencaker']       = 'home/faq_pencaker';
$route['faq_perusahaan']     = 'home/faq_perusahaan';
$route['sk_pencaker']        = 'home/syarat_ketentuan_pencaker';
$route['sk_perusahaan']      = 'home/syarat_ketentuan_perusahaan';
$route['kebijakan_privasi']  = 'home/kebijakan_privasi';



$route['adminkl1011']											= 'adminkl1011';
$route['adminkl1012/login'] 									= 'adminkl1011/login';
$route['adminkl1012/home'] 		 								= 'adminkl1011/home';
$route['adminkl1012/klien/semua'] 								= 'adminkl1011/semua_klien';
$route['adminkl1012/klien_detail/(:any)'] 						= 'adminkl1011/klien_detail/$1';
$route['adminkl1012/klien/baru'] 								= 'adminkl1011/klien_baru';
$route['adminkl1012/klien/on/(:any)/(:any)']					= 'adminkl1011/klien_on/$1/$2';
$route['adminkl1012/klien/delete/(:any)']						= 'adminkl1011/klien_delete/$1';
$route['adminkl1012/klien/detail/(:any)']						= 'adminkl1011/klien_new_detail/$1'; 
$route['adminkl1012/klien/setkuota']							= 'adminkl1011/klien_set_kuota';
$route['adminkl1012/klien/setkuota/all']						= 'adminkl1011/klien_set_kuota_all';
$route['adminkl1012/klien/setkuota/(:any)/(:any)']				= 'adminkl1011/klien_set_kuota/$1/$2';
$route['adminkl1012/klien/setkuota/search/filter/(:any)']		= 'adminkl1011/klien_set_kuota_filter_pendidikan/$1';
$route['adminkl1012/klien/setkuota/process']					= 'adminkl1011/klien_set_kuota_process';
$route['adminkl1012/klien/updatekuota']		 		 			= 'adminkl1011/klien_update_kuota';
$route['adminkl1012/klien/updatekuota/(:any)'] 		 			= 'adminkl1011/klien_update_kuota/$1';
$route['adminkl1012/klien/updatekuota/(:any)/(:any)/(:any)']	= 'adminkl1011/klien_update_kuota/$1/$2/$3';

$route['adminkl1012/perusahaan/data'] 										= 'adminkl1011/data_perusahaan';
$route['adminkl1012/perusahaan/data_status'] 								= 'adminkl1011/data_status_perusahaan';
$route['adminkl1012/perusahaan/off/(:any)'] 								= 'adminkl1011/set_off_perusahaan/$1';
$route['adminkl1012/perusahaan/on/(:any)'] 									= 'adminkl1011/set_on_perusahaan/$1';
$route['adminkl1012/perusahaan/delete/(:any)'] 								= 'adminkl1011/delete_perusahaan/$1';
$route['adminkl1012/perusahaan/(:any)'] 									= 'adminkl1011/perusahaan/$1';
$route['adminkl1012/perusahaan/(:any)/(:any)'] 								= 'adminkl1011/loker_perusahaan/$1/$2';
$route['adminkl1012/posting/delete/(:any)/(:any)'] 							= 'adminkl1011/delete_posting/$1/$2';
$route['adminkl1012/perusahaan/share/(:any)/(:any)/(:any)/(:any)'] 			= 'adminkl1011/share_loker_perusahaan/$1/$2/$3/$4';
$route['adminkl1012/perusahaan/aktif/on/csv/(:any)'] 						= 'adminkl1011/csv_perusahaan_on/$1';
$route['adminkl1012/perusahaan/aktif/warning/csv/(:any)'] 					= 'adminkl1011/csv_perusahaan_warning/$1';
$route['adminkl1012/perusahaan/aktif/off/csv/(:any)'] 						= 'adminkl1011/csv_perusahaan_off/$1';
$route['adminkl1012/perusahaan/permintaan/masa/aktif']		 				= 'adminkl1011/permintaan_perusahaan';
$route['adminkl1012/perusahaan/permintaan/masa/aktif/(:any)']				= 'adminkl1011/hapus_permintaan_perusahaan/$1';
$route['adminkl1012/perusahaan/permintaan/masa/aktif/(:any)/(:any)/(:any)']	= 'adminkl1011/proses_permintaan_perusahaan/$1/$2/$3';
$route['adminkl1012/update_data_perusahaan'] 									= 'adminkl1011/update_data_perusahaan';

$route['adminkl1012/jobfair/data'] 						= 'adminkl1011/data_jobfair';
$route['adminkl1012/jobfair/create'] 					= 'adminkl1011/create_jobfair';
$route['adminkl1012/loker/create'] 						= 'adminkl1011/create_loker';
$route['adminkl1012/jobfair/create_exc/(:any)'] 		= 'adminkl1011/create_jobfair_exc/$1';

$route['adminkl1012/loker/create/addloker'] = 'adminkl1011/tambah_loker';

$route['adminkl1012/jobfair/delete/(:any)'] 			= 'adminkl1011/delete_jobfair/$1';
$route['adminkl1012/jobfair/edit/(:any)'] 				= 'adminkl1011/edit_jobfair/$1';
$route['adminkl1012/jobfair/edit_exc/(:any)/(:any)']	= 'adminkl1011/edit_jobfair_exc/$1/$2';
$route['adminkl1012/baru/perusahaan_baru'] 				= 'adminkl1011/perusahaan_baru';
$route['adminkl1012/klien/kuota/on/csv/(:any)'] 		= 'adminkl1011/csv_on/$1';
$route['adminkl1012/klien/kuota/warning/csv/(:any)'] 	= 'adminkl1011/csv_warning/$1';
$route['adminkl1012/klien/kuota/off/csv/(:any)'] 		= 'adminkl1011/csv_off/$1';
$route['adminkl1012/klien/csv'] 						= 'adminkl1011/csv_klien';
	
   

$route['klien/login'] 							= 'klien/login';
$route['klien/home']		 	 				= 'klien';
$route['register']		 	 					= 'home/register';

$route['klien/up_stat_pencaker']		 	 	= 'klien/up_stat_pencaker';

$route['klien/daftar']			 				= 'klien/create';
$route['klien/bayar']			 				= 'klien/bayar';
$route['klien/pembayaran/keanggotaan']			= 'klien/pembayaran_keanggotaan';
$route['klien/pending']			 				= 'klien/pending';
$route['klien/lamaran/buat/(:any)/(:any)']	 	= 'klien/insertlowong/$1/$2';
$route['klien/profile']							='klien/profile';
$route['klien/edit-profile']					='klien/edit_profile';
$route['klien/edit-akun']						='klien/edit_akun_execute';
$route['klien/profile-edit-pk-sukses']			='klien/profilepksukses';
$route['klien/profile-edit-pi-sukses']			='klien/profilepisukses';
$route['klien/formtambahpendidikan']			='klien/tambahpi';
$route['klien/formtambahpengalaman']			='klien/tambahpk';
$route['klien/tambahpendidikan']				='klien/input_pendidikan';
$route['klien/tambahpengalaman']				='klien/input_pengalaman';
$route['klien/edit-pengalaman-kerja/(:any)']	='klien/edit_pk/$1';
$route['klien/edit-pk-proses/(:any)']			='klien/edit_pk_execute/$1';
$route['klien/delete-pk/(:any)']				='klien/edit_pk_execute/$1';
$route['klien/deletependidikan/(:any)']				='klien/deletependidikan/$1';
$route['klien/deletepengalaman/(:any)']				='klien/deletepengalaman/$1';
$route['klien/edit-pendidikan/(:any)']			='klien/edit_pend/$1';
$route['klien/edit-pend-proses/(:any)']			='klien/edit_pend_execute/$1';
$route['klien/deletependidikan']				='klien/deletepi';
$route['klien/setting']							='klien/settingakun';
$route['klien/lamaran-minggu-ini/(:any)']		='klien/detaillowongseven/$1';
$route['klien/detjobfair/(:any)']				='klien/detailjobfair/$1';
$route['klien/help']							='klien/help';
$route['klien/cari']							='klien/cari';
$route['klien/pesan']							='klien/show_pesan';
$route['klien/statuspembayaran']							='klien/status_update';
$route['klien/bacapesan/(:any)']						='klien/bacapesan/$1';

$route['klien/jobfair']							='klien/daftar_jf';
$route['klien/jobfair/create']					='klien/tiket';
$route['klien/jobfair/createumum']					='klien/tiketumum';
$route['klien/tambahkuota']						='klien/tambahkuota';
$route['klien/tambahkuotaexc']						='klien/tambahkuota_exc';
$route['klien/jobfairumum']							='klien/daftar_jf_umum';



$route['perusahaan']		 								= 'perusahaan';
$route['perusahaan/login']		 							= 'perusahaan/login';
$route['perusahaan/home']		 	 						= 'perusahaan/home';
$route['perusahaan/home/post/(:any)']						= 'perusahaan/postingan/$1';
$route['perusahaan/home/pelamar/(:any)']					= 'perusahaan/pelamar/$1';
$route['perusahaan/daftar']									= 'perusahaan/create';
$route['perusahaan/persetujuan'] 	 						= 'perusahaan/persetujuan';
$route['perusahaan/setuju'] 	 							= 'perusahaan/setuju';
$route['perusahaan/profile'] 	 							= 'perusahaan/profile';
$route['perusahaan/profile/ubah'] 	 						= 'perusahaan/ubah_profile';
$route['perusahaan/profile/update/(:any)'] 					= 'upload/update_perusahaan/$1';
$route['perusahaan/profile/loker'] 	 						= 'perusahaan/loker';
$route['perusahaan/profile/loker/(:any)'] 	 				= 'perusahaan/detail_loker/$1';
$route['perusahaan/profile/uploadloker']					= 'perusahaan/uploadloker';
$route['perusahaan/profile/uploadloker/(:any)']				= 'perusahaan/uploadloker/$1';
$route['perusahaan/home/delete/(:any)/(:any)'] 				= 'perusahaan/delete_pelamar/$1/$2';
$route['perusahaan/profile/pelamar/delete/(:any)/(:any)']	= 'perusahaan/delete_pelamar_p/$1/$2';
$route['perusahaan/post/pelamar/delete/(:any)/(:any)']		= 'perusahaan/delete_pelamar_h/$1/$2';
$route['perusahaan/profile/pelamar/konfirm/(:any)/(:any)']	= 'perusahaan/konfirm_pelamar_p/$1/$2';
$route['perusahaan/post/pelamar/konfirm/(:any)/(:any)']		= 'perusahaan/konfirm_pelamar_h/$1/$2';
$route['perusahaan/home/delete/(:any)'] 					= 'perusahaan/delete_posting/$1';
$route['perusahaan/profile/loker/delete/(:any)'] 			= 'perusahaan/delete_posting_p/$1';
$route['perusahaan/post/edit/(:any)']						= 'perusahaan/edit_postingan/$1';
$route['perusahaan/post/update/(:any)']						= 'perusahaan/edit_postingan_exc/$1';
$route['perusahaan/profile/post/edit/(:any)']				= 'perusahaan/edit_postingan_p/$1';
$route['perusahaan/profile/post/update/(:any)']				= 'perusahaan/edit_postingan_exc_p/$1';
$route['perusahaan/pelamar/lamaran/konfirm/(:any)/(:any)/(:any)']	= 'perusahaan/konfirm_pelamar_pl/$1/$2/$3';
$route['perusahaan/pelamar/lamaran/delete/(:any)/(:any)/(:any)']	= 'perusahaan/delete_pelamar_pl/$1/$2/$3';
$route['perusahaan/home/pelamar/(:any)/(:any)']						= 'perusahaan/pelamar/$1/$2';
$route['perusahaan/profile/loker/(:any)/(:any)/(:any)'] 	 		= 'perusahaan/spek_loker/$1/$2/$3';
$route['perusahaan/home/post/(:any)/(:any)/(:any)']					= 'perusahaan/detail_postingan/$1/$2/$3';
$route['perusahaan/profile/help/1'] 	 							= 'perusahaan/help1';
$route['perusahaan/profile/mp/(:any)/(:any)/(:any)']				= 'perusahaan/message_pp/$1/$2/$3';
$route['perusahaan/home/mp/(:any)/(:any)/(:any)']					= 'perusahaan/message_p/$1/$2/$3';
$route['perusahaan/home/mpl/(:any)/(:any)/(:any)']					= 'perusahaan/message_pl/$1/$2/$3';
$route['perusahaan/send/message_pp/(:any)/(:any)/(:any)']			= 'perusahaan/send_message_pp/$1/$2/$3';
$route['perusahaan/send/message_p/(:any)/(:any)/(:any)']			= 'perusahaan/send_message_p/$1/$2/$3';
$route['perusahaan/send/message_pl/(:any)/(:any)/(:any)/(:any)']	= 'perusahaan/send_message_pl/$1/$2/$3/$4';
$route['perusahaan/send/message_ph/(:any)']							= 'perusahaan/send_message_ph/$1';
$route['perusahaan/pesan/terkirim']									= 'perusahaan/pesan_terkirim';
$route['perusahaan/pesan/delete/(:any)']							= 'perusahaan/delete_pesan/$1';
$route['perusahaan/pesan/lihat/(:any)']								= 'perusahaan/lihat_pesan/$1';
$route['perusahaan/pesan/edit/(:any)']								= 'perusahaan/edit_pesan/$1';
$route['perusahaan/edit/message/(:any)']							= 'perusahaan/edit_pesan_exc/$1';
$route['perusahaan/home/off/(:any)']								= 'perusahaan/tutup_lowongan/$1';
$route['perusahaan/home/on/(:any)']									= 'perusahaan/buka_lowongan/$1';
$route['perusahaan/home/off_p/(:any)']								= 'perusahaan/tutup_lowongan_p/$1';
$route['perusahaan/home/on_p/(:any)']								= 'perusahaan/buka_lowongan_p/$1';
$route['perusahaan/pesanjf/lihat/(:any)']							= 'perusahaan/buka_pesan/$1';
$route['perusahaan/pesanjf/delete/(:any)']							= 'perusahaan/del_pesan_jf/$1';
$route['perusahaan/masa_aktif']										= 'perusahaan/masa_aktif';
$route['perusahaan/update/masa_aktif']								= 'perusahaan/update_masa_aktif';



$route['upload/pas-foto']		 			= 'upload/pas_foto_insert';
$route['upload/foto']		 				= 'upload/foto_insert';
$route['upload/cv']		 					= 'upload/cv_insert';
$route['upload/ktp']		 				= 'upload/ktp_insert';
$route['upload/ijazah-dp']		 			= 'upload/ijazah_dp_insert';
$route['upload/ijazah-blk']		 			= 'upload/ijazah_blk_insert';
$route['upload/berkas1']		 			= 'upload/berkas1_insert';
$route['upload/berkas2']		 			= 'upload/berkas2_insert';
$route['upload/berkas3']		 			= 'upload/berkas3_insert';

$route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;
