<?php defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

$setting = parse_ini_file(FCPATH. 'setting.ini.php');

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => $setting['hostname'],
	'username' => $setting['username'],
	'password' => $setting['password'],
	'database' => $setting['database'],
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== $setting['environment']),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);