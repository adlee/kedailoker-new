<!DOCTYPE html>
<html>
<head>
	<title>KEDAI LOKER</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/style.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/script.js"></script>	
</head>
<body style="background-color:#FFE0E0">
	<form>
		<div id="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#C20000;">
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
				<div class="kedai" style="padding:5px">kedailoker</div>
			</div>
			<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style="background:#c20000;background:red;background:#FFC600;color:#000;font-weight:bold;font-size:117%;padding:17px 5px 23px 20px">
				Pendaftaran JOBFAIR GRATIS & INFORMASI JADWAL JOBFAIR GRATIS Se. JATENG
			</div>
	    </div>
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12" style="margin-top:30px">
			<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Nama lengkap sesuai KTP / Kartu Identitas lainya" required name="nama">
			<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" maxlength="12" placeholder="Nomor seluler aktif" required name="nomor" onkeydown="return numbers(event);">
			<input type="text" class="col-lg-6 col-md-6 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Alamat lengkap" required name="alamat">
			<input type="text" class="col-lg-5 col-md-5 col-sm-11 col-xs-12 input" style="margin-bottom:10px;text-align:center;" placeholder="Kota/ Kabupaten" required name="alamat">
			<p style="clear:both">Tempat, Tanggal Lahir</p>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin:0 15px 0 -15px">
				<input class="input" style="padding-left:10px;width:100%" type="text" placeholder="tempat" required onkeydown="return numbersonly(event);">	
			</div>
			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-11" style="margin:0 0 0 -15px">
				<select class="col-lg-4 col-md-4 col-sm-4 col-xs-4 input" style="height:35px" name="" required>
					<option>tgl</option>
					<?php for ($i=1; $i<=31 ; $i++) { 
						?><option value="<?php echo $i; ?> "><?php echo $i; ?> </option><?php
					} ?>
				</select>
				<select class="col-lg-4 col-md-4 col-sm-4 col-xs-4 input" style="height:35px;" name="" required>
					<option>bln</option>
					<?php for ($i=1; $i<=12 ; $i++) { 
						?><option value="<?php echo $i; ?> "><?php echo $i; ?> </option><?php
					} ?>
				</select>
				<select class="col-lg-4 col-md-4 col-sm-4 col-xs-4 input" style="height:35px" name="" required>
					<option>thn</option>
					<?php for ($i=1900; $i<=date('Y') ; $i++) { 
						?><option value="<?php echo $i; ?> "><?php echo $i; ?> </option><?php
					} ?>
				</select>
			</div>
			<p style="clear:both">Jenis Kelamin</p>
			<div id="jkelamin" style="clear:both">
				<label class="control control--radio">
			  		<input type="radio" required name="jkelamin"/>Perempuan
			  		<div class="control__indicator"></div>
				</label>
				<label class="control control--radio">
				  	<input type="radio" required name="jkelamin"/>Laki-laki
				  	<div class="control__indicator"></div>
				</label>
			</div>
			<p style="margin-top:10px">Pendidikan Terakhir</p>
			<select class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="height:35px;text-align:center;margin-right:90px" name=""  required>
				<option>SD</option><option>SMP</option><option>SMA</option>
				<option>SMK</option><option>D1</option><option>D2</option>
				<option>D3</option><option>D4</option><option>S1</option>
			</select>
			<p style="clear:both">Pendidikan informal/ kursus/ pelatihan</p>
			<textarea class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="height: 155px;" placeholder="Kursus ..........................................................."></textarea>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top:25px;margi-green">
			<div>
				<p style="padding:8px 0 8px 0">Pengalaman Kerja</p>
				<textarea class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="height: 155px;" placeholder="........................................................................"></textarea>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12" style="margin-top:25px;">
			<p style="padding:8px 0 8px 0;text-align:center">PILIH DAFTAR JOBFAIR GRATIS</p>
			<div style="background:none;height:388px;overflow:scroll;margin-left:-10px" class="pilihdaftar">
			  	<div style="background:#D22E2E" >
				<ul style="background:none">
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
					<li><input type="checkbox" style="margin:0 10px 0 -20px">PT. xxxxxxxxxx</li>
				</ul>
				</div>
			</div>
			<input type="submit" style="width:100%;padding:10px 0;margin:0 0 5px 0" class="tombol" name="daftar" value="DAFTAR">
			<input type="button" class="kembali" style="width:100%;padding:10px 0;margin:0 0 10px 0" onclick="history.back(-1)" value="kembali">
		</div>
	</form>
</body>
</html>