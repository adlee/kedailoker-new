		<div class="column col-lg-9 col-md-9 col-sm-9 col-xs-12" id="main" style="text-align:left;background: #FFE0E0;font-size: 100%;box-shadow: -1px -1px 1px 1px;">
			<div>
				<div class="tab-content" style="color:#000">
			  		<div class="tab-pane fade in active">
					    <h3 style="padding-left:20px">Data Perusahaan</h3>
					    <div class="row">
						    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						    	<?php foreach ($ket as $eachuser) {
							    	$id=$eachuser['id'];
							    } ?>
							    <form action="<?php echo base_url();?>perusahaan/profile/update/<?php echo $id ?>" method="post" enctype="multipart/form-data" style="margin-top:10px"><?php 
							    	foreach ($ket as $eachuser) { 
							    	$region=$eachuser['region']; ?>
								    <div class="form-group">
							            <div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;;width:20%;text-align:left">Nama Perusahaan</div>
							                <input type="text" class="form-control" name="namaper" value="<?php echo $eachuser['nama_per']?>"  onkeypress="return isNumberKey(event)" required>
							                </div><!-- /.input group -->
							            </div>
							            <div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;;width:20%;text-align:left">Region</div>
							                <select class="form-control select2" name="region" required style="width:100%;">
										       	<option selected value="<?php echo $region;?>"><?php echo $region;?> negeri</option>
										       	<?php if ($region=="dalam") {?>
										       		<option value="luar">luar negeri</option><?php
										       	}else {?>
										       		<option value="dalam">dalam negeri</option><?php 
										       	} ?>
										    </select>
										    </div>
							            </div>
							            <div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;;width:20%;text-align:left">Alamat</div>
							                <input type="text" class="form-control" name="alamat" value="<?php echo $eachuser['alamat']?>"required>
							                </div><!-- /.input group -->
							            </div>
							            <div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;;width:20%;text-align:left">Email</div>
							                <input type="email" class="form-control" name="email" value="<?php echo $eachuser['email_hrd']?>" required>
							                </div><!-- /.input group -->
							            </div>
							            <div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;;width:20%;text-align:left">Telepon</div>
							                <input type="text" class="form-control" name="notelp" value="<?php echo $eachuser['no_telp']?>" maxlength="12" onkeydown="return numbers(event)" required readonly>
							                </div><!-- /.input group -->
							            </div>
							            <div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;;width:20%;text-align:left">HRD</div>
							                <input type="text" class="form-control" name="namahrd" value="<?php echo $eachuser['nama_hrd']?>" required>
							                </div><!-- /.input group -->
							            </div>	
							            <div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;;width:20%;text-align:left">Telepon HRD</div>
							                <input type="text" class="form-control" name="nohrd" value="<?php echo $eachuser['no_hrd']?>" maxlength="12" onkeydown="return numbers(event)" required>
							                </div><!-- /.input group -->
							            </div>	
							            <div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;;width:20%;text-align:left">Password lama</div>
							                <input type="text" class="form-control" value="<?php echo $eachuser['password']?>"readonly>
							                </div><!-- /.input group -->
							            </div>
							            <div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;;width:20%;text-align:left">Password baru</div>
							                <input type="text" class="form-control" name="password" value="<?php echo $eachuser['password']?>" required>
							                </div><!-- /.input group -->
							            </div>
										<div class="form-group">
							              <div class="input-group" style="width:100%">
							                <div class="input-group-addon" style="color:#000;background:transparent;border:none;width:20%;text-align:left">Foto/logo</div>
							                <input class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="line-height: 30px;border-radius:3px;margin-left:0;background:#fff" type="file" id="fupload" name="fupload" accept="image/*">
											<div><i id="i"></i></div>
										  </div><!-- /.input group -->
							            </div>
							            <div class="box-footer" style="margin:5px 0 0 15px;float:right">
								          <input type="button" class="btn btn-default"  value="Kembali" onclick="history.back(-1)" > &nbsp 
								          <button type="submit" name="submit" class="btn btn-info"> Simpan</button>
								        </div>
        							</div><?php 
        							} ?>
						    	</form>
			    			</div>
			    		</div>
			    	</div>
		  		</div>
			</div>
		</div>