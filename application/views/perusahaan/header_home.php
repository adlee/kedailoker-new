<?php
  if ($this->session->userdata('nom')==null) {
    redirect('perusahaan');
  }else{
  	foreach ($status as $status) {
  		if ($status['status']=="off") {
  			redirect('perusahaan/persetujuan');	
  		}else{
?>
<!DOCTYPE html>
<html>
<head>
	<title>KEDAI LOKER (PERUSAHAAN)</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/style.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/script.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/bootstrap.min.js"></script>
	<!-- sweet alert -->
    <script src="<?php echo base_url();?>dist/js/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/sweetalert.css">
	<!-- editor -->
    <script type="text/javascript" src="<?php echo base_url();?>dist/js/tinymce/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/tinymce/tinymce.min.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<style>
	  #nav-upload-icon{
        top: 10px;
        left: 10px;
        z-index: 1000;
	    }
			#gambar:hover + #nav-upload-icon,  
		#nav-upload-icon:hover {
		    visibility: visible;
		}
		#gambar:hover{
			opacity: 0.8;
		}

		#nav-upload-icon { 
		    visibility : hidden; 
		    position:absolute;
		    z-index:50000
		    margin:0 auto; left:49%;top:130px;
		    color:white;
		}
	</style>
	<style type="text/css">
	.begron{
		background-color: #002060;
	}
</style>
	<script>
	  tinymce.init({
	    selector: '#mytextarea',
	    menubar:false,
	    statusbar: false
	  });
  	</script>
</head>
<body class="begron">
	<div id="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#002060">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<a href="<?php echo base_url();?>perusahaan/home"><div class="kedai">kedailoker</div></a>
			<div class="imgakun" >
				<button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown" style="margin:-5px 0 0px -8px;padding:10px 10px 0 15px;border:none;width:220px"><p style="font-size:100%;width:155px;overflow:hidden;font-weight:bold;color:#000;float:left;margin-right:5px">
				Hello, <?php 
		    		foreach ($ket as $eachuser) {
		    			$id_perusahaan=$eachuser['id'];
		    			$nm_perusahaan=$eachuser['nama_per'];
		    			$jml=0;
		    			$nama= explode(" ", $eachuser['nama_per']);
		    			$namadepan=$nama[0];
		    			echo $eachuser['nama_per']."&nbsp</p><span class='caret'></span>";
		    		} ?>
				</button>
				<ul class="dropdown-menu" style="margin:-1px 0 0 21px">
				   <li><a href="<?php echo base_url();?>logout">LOGOUT</a></li>
				</ul>
				<?php $jml=0; foreach ($new as $pesanjf) {$jml++;}
				if ($jml>0) { ?>
					<a href="<?php echo base_url();?>perusahaan/pesan/terkirim" class="btn btn-danger" style="padding:8px 6px 8px 7px;margin-left:-4px;border-radius:0;border:none">
	                  <i class="fa fa-envelope-o" style="color:#fff"></i>
	                  <span class="label label-info" style="padding:1px 4px;font-size:70%"><?php echo $jml ?></span>
	                </a><?php
				} ?>
			</div>
		</div>
			<div class="col-lg-1 col-md-2 col-sm-2 col-xs-12 begron"border="1" style="margin:6px 10px 0 0;color:#000;text-align:center;padding:22px 0;color:#fff">
				Total Member
			</div><?php 
			$sdn=0; foreach ($sd as $sdx) { $sdn++;}
			$smpn=0; foreach ($smp as $smpx) { $smpn++;}
			$sman=0; foreach ($sma as $smax) { $sman++;}
			$smkn=0; foreach ($smk as $smkx) { $smkn++;}
			$d1n=0; foreach ($d1 as $d1x) { $d1n++;}
			$d2n=0; foreach ($d2 as $d2x) { $d2n++;}
			$d3n=0; foreach ($d3 as $d3x) { $d3n++;}
			$d4n=0; foreach ($d4 as $d4x) { $d4n++;}
			$s1n=0; foreach ($s1 as $s1x) { $s1n++;}
			$s2n=0; foreach ($s2 as $s2x) { $s2n++;}
			?>
			<table class="col-lg-1 col-md-1 col-sm-1 col-xs-4" style="margin:6px 0 0 0;color:#000;color:#fff;text-align:center">
				<tr><td style="width:30px;padding-left:5px">SD</td>
					<td style="width:40px;background:#fff;padding:6px 0;border-bottom:1px solid red"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $sdn ?></span></td>
				</tr>
				<tr><td style="width:30px;padding-left:5px">SMP</td>
					<td style="width:40px;background:#fff;padding:6px 0"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $smpn ?></span></td>
				</tr>
			</table>
			<table class="col-lg-1 col-md-1 col-sm-1 col-xs-4" style="color:#fff;margin:6px 0 0 0;text-align:center">
				<tr><td style="width:30px;padding-left:5px">SMK</td>
					<td style="width:40px;background:#fff;padding:6px 0;border-bottom:1px solid red"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $smkn ?></span></td>
				</tr>
				<tr>
					<td style="width:30px;padding-left:5px">SMA</td>
					<td style="width:40px;background:#fff;padding:6px 0"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $sman ?></span></td>
				</tr>
			</table>
			<table class="col-lg-1 col-md-1 col-sm-1 col-xs-4" style="color:#fff;margin:6px 0 0 0;text-align:center">
				<tr><td style="width:30px;padding-left:5px">D1</td>
					<td style="width:40px;background:#fff;padding:6px 0;border-bottom:1px solid red"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $d1n ?></span></td>
				</tr>
				<tr>
					<td style="width:30px;padding-left:5px">D2</td>
					<td style="width:40px;background:#fff;padding:6px 0"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $d2n ?></span></td>
				</tr>
			</table>
			<table class="col-lg-1 col-md-1 col-sm-1 col-xs-4" style="color:#fff;margin:6px 0 0 0;text-align:center">
				<tr><td style="width:30px;padding-left:5px">D3</td>
					<td style="width:40px;background:#fff;padding:6px 0;border-bottom:1px solid red"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $d3n ?></span></td>
				</tr>
				<tr>
					<td style="width:30px;padding-left:5px">D4</td>
					<td style="width:40px;background:#fff;padding:6px 0"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $d4n ?></span></td>
				</tr>
			</table>
			<table class="col-lg-1 col-md-1 col-sm-1 col-xs-4" style="color:#fff;margin:6px 0 0 0;text-align:center">
				<tr><td style="width:30px;padding-left:5px">S1</td>
					<td style="width:40px;background:#fff;padding:6px 0;border-bottom:1px solid red"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $s1n ?></span></td>
				</tr>
				<tr>
					<td style="width:30px;padding-left:5px">S2</td>
					<td style="width:40px;background:#fff;padding:6px 0"><span style="color:black;width:100%;padding:0 5px;height:30px;margin:1px"><?php echo $s2n ?></span></td>
				</tr>
			</table>
			<div style="float:right;margin:7px 4px 0 0"><?php 
				foreach ($ket as $eachuser) {
			  		$img=$eachuser['foto'];
			  		$data=explode(".", $eachuser['foto']);
	                $foto=$data[0];
	                $type=$data[1];
	                if ($type=="jpeg") {?>
	                	<a href="#aboutModal" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $foto?>.jpg" title="foto" width=64px height=64px  id="pp"></a>
	                <?php
	                }else{?>
	                	<a href="#aboutModal" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $img?>" title="foto" width=64px height=64px  id="pp"></a>
	                <?php
	                }
			   	} ?>
		</div>
	</div>
	<div class="row" >
        <div class="column col-lg-3 col-md-3 col-sm-3 col-xs-12" id="main" style="text-align:left;background: #002060;font-size: 100%;box-shadow: -2px -1px 1px 1px;">
        	<a href="<?php echo base_url();?>perusahaan/profile"><div class="btn btn-primary form-control">Profil Perusahaan</div></a>
			<a href="<?php echo base_url();?>perusahaan/pesan/terkirim"><div class="btn btn-primary form-control" style="margin-top:-4px">Respon Perusahaan</div></a>
			<a href="<?php echo base_url();?>perusahaan/masa_aktif"><div class="btn btn-primary form-control" style="margin-top:-4px">Masa Aktif Lowongan</div></a>
			<p style="text-align: center;color: #fff;padding: 7px 0  7px 0;border-bottom: 1px solid #eee;margin-bottom: 0px;">
				Posting<br/>Lowongan Kerja
			</p>
			<div class="scroll" style="background:#002060;height:440px;border-bottom:none">
				<ul><?php $no=1;
				foreach ($post as $loker) { 
					$id_post=$loker['id_post'];
					$jml=0;
                	foreach ($pelamar as $datapelamar) {
                		$id_lokerpelamar=$datapelamar['id_low'];
                		if ($id_post==$id_lokerpelamar) {
                			$jml++;
                		}
                	}?>
					<a href="<?php echo base_url();?>perusahaan/home/post/<?php echo $id_post;?>"><li><p style="color:#FFF;width:25px;float:left"><?php echo $no ?></p> <span style="color:#FFF"><?php echo $loker['judul'] ?></span> 
						<span style="float:right;color:#FFF;"><?php 
							if($jml>0){ echo "(".$jml.")";} ?> &nbsp 
						</span></li>
					</a><?php
				$no++; 	
				} ?>
				</ul>
			</div>
		</div>
	<?php }}} ?>