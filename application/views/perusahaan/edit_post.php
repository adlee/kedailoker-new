		<div class="column col-lg-6 col-md-6 col-sm-6 col-xs-12" id="main" style="text-align:left;background: #FFE0E0;font-size: 100%;box-shadow: -1px -1px 1px 1px;">
        	<div class="scrollkonten" style="height:100%">
        		<h3 style="padding-left:27px;color:#7C0000;margin-bottom:20px">Edit Lowongan</h3><?php 
    			foreach ($pilih as $postingan) {
				 $id_loker=$postingan['id_post'];
				 $g1=$postingan['gaji_min'];
                 $g2=$postingan['gaji_max'];
                 ?>
                 <form method="post" action="<?php echo base_url();?>perusahaan/post/update/<?php echo $id_loker;?>">
	        		<div class="col-lg-12" style="margin-bottom:5px">
				    	<div class="col-lg-12 col-md-12 col-sm-12" style="margin:10px 0 -10px 0">Jenis Lowongan</div>
						<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:10px">
				    		<input type="text" class="form-control" name="judul" value="<?php echo $postingan['judul'] ?>" name="judul" required>
				    	</div>
				    </div>
				    <div class="col-lg-12" style="margin-bottom:5px">
				    	<div class="col-lg-12 col-md-12 col-sm-12" style="margin:10px 0 -10px 0">Kuota</div>
						<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:10px">
				    		<input type="text" class="form-control" name="kuota" value="<?php echo $postingan['kuota'] ?>"    onkeypress="return numbers(event)" required>
				    	</div>
				    </div>
					<div class="col-lg-12" style="margin-bottom:5px">
				    	<div class="col-lg-12 col-md-12 col-sm-12" style="margin:10px 0 -10px 0">Syarat ketentuan</div>
						<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:10px">
							<textarea class="form-control" id="mytextarea" name="isi" id="isi" cols="30" rows="5"><?php echo $postingan['isi'] ?></textarea>
						</div>
				    </div>
				    <div class="col-lg-12" style="margin-bottom:5px">
				    	<div class="col-lg-12 col-md-12 col-sm-12" style="margin:10px 0 -10px 0">Minimal pendidikan</div>
						<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:10px">
							<select class="form-control" name="pendidikan" required>
								<option value="<?php echo $postingan['pendidikan'] ?>" selected><?php echo $postingan['pendidikan'] ?></option>
								<option value="SD">SD</option>
								<option value="SMP">SMP</option>
								<option value="SMA">SMA</option>
								<option value="SMK">SMK</option>
								<option value="D1">D1</option>
								<option value="D2">D2</option>
								<option value="D3">D3</option>
								<option value="D4">D4</option>
								<option value="S1">S1</option>
								<option value="S2">S2</option>
							</select>
				    	</div>
				    </div>
				    <div class="col-lg-12" style="margin-bottom:5px">
				    	<div class="col-lg-12 col-md-12 col-sm-12" style="margin:10px 0 0px 0">Range gaji</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
						<p style="position:absolute;top:8px;left:22px;color:gray">Rp</p>
						<input type="text" style="padding-left:30px;width:50%;float:left" class="form-control" name="gaji1" value="<?php echo $g1;?>"  onkeypress="return numbers(event)" required>
						<input type="text" style="width:50%" class="form-control" name="gaji2" value="<?php echo $g2 ?>"  onkeypress="return numbers(event)" required>
				    	</div>
				    </div><?php 
	        		} ?>
	        		<div style="margin:20px 30px 5px 0;float:right">
	        			<button class="btn btn-default" onclick="history.back(-1)" style="float:left">kembali</button> &nbsp 
				    	<input type="submit" class="btn btn-danger" style="padding:6px 6px" value="Simpan perubahan">
				    </div>
				 </form>
	    	</div>
		</div>