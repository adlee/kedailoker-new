		<div class="column col-lg-6 col-md-6 col-sm-6 col-xs-12" id="main" style="text-align:left;background: #FFE0E0;font-size: 100%;box-shadow: -1px -1px 1px 1px;">
        	<div class="scrollkonten" style="height:100%">
	        	<div class="table-responsive" style="margin-top:-1px">
			    	<table class="table table-hover"><?php
				    	foreach ($ket as $eachuser) {
			    			$id_perusahaan=$eachuser['id'];
			    			$nm_perusahaan=$eachuser['nama_per'];
			    		}
		        		foreach ($datapelamar as $dtpelamar) {
		    				$id_pelamar=$dtpelamar['id'];
		    				$nama=$dtpelamar['username'];
		    				$telp=$dtpelamar['telp'];
		    				$nama=$dtpelamar['username'];
		    				$telepon=$dtpelamar['telp'];?>
		    				<tr style="background:#F1CCCC;"><td colspan="5"><h3 style="margin-top:5px;color:#7C0000">Data pelamar (<?php echo $dtpelamar['username'] ?>)</h3></td></tr>
		    				<tr><td>Jenis Kelamin</td><td>:</td><td> <?php 
		    						if ($dtpelamar['jk']=='L') { echo "Laki-Laki";
		    						}else{ echo "Perempuan";} ?>
		    					</td>
		    				</tr>
		    				<tr><td>Telepon</td><td>:</td><td> <?php echo $dtpelamar['telp']?></td></tr>
		    				<tr><td>Email</td><td>:</td><td> <?php echo $dtpelamar['email'] ?></td></tr>
		    				<tr><td>Foto</td><td>:</td><td> <?php 
							  		$img=$dtpelamar['foto'];
							  		$data=explode(".", $dtpelamar['foto']);
					                $foto=$data[0];
					                $type=$data[1];
					                if ($type=="jpeg") {?>
					                	<img src="<?php echo base_url(); ?>dist/img/<?php echo $foto?>.jpg" title="foto" width=150px id="pp"></a>
					                <?php
					                }else{?>
					                	<img src="<?php echo base_url(); ?>dist/img/<?php echo $img?>" title="foto" width=150px id="pp"></a>
					                <?php
					                }?>
								</td>
							</tr>
							<tr><td>Pend. Informal</td><td>:</td>
								<td><?php $no=1; foreach ($informal as $pendinformal) { 	
										echo $no.". ".$pendinformal['pendidikan']."<br/>"; 
										$no++;
									} ?>
								</td>
							</tr><tr><td>Peng. Kerja</td><td>:</td>
								<td><?php $no=1; 
								foreach ($datapelamar as $dtpelamar) {
									$tlp=$dtpelamar['telp']	;
									foreach ($pengalaman as $pengkerja) { 	
										if ($tlp==$pengkerja['id_pencari']) {
											echo $no.". ".$pengkerja['jabatan']."<br/>"."&nbsp &nbsp "."Di ".$pengkerja['institusi']."<br/>"."&nbsp &nbsp "."Tahun ".$pengkerja['masuk'];
											if ($pengkerja['masuk']!=$pengkerja['keluar']) {
												echo " - ".$pengkerja['keluar']."<br/>";
											}
											$no++;			
										}
									}
								}?>
								</td>
							</tr><?php
		    			} ?>
		    		</table>
	    		</div>
	    		<div class="table-responsive">
				    <div class="row">
						<div style="width:100%">
			                <div class="box-body">
			                  <table id="example1" class="table table-striped">
			                    <thead  style="background:#C20000">
			                      <tr style="background:#F1CCCC;"><td colspan="5"><h3 style="margin-top:5px;color:#7C0000">Data Lamaran</h3></td></tr>
			                      <tr align="center" style="color:#fff;text-align:center;font-weight:bold">
			                        <td style="border-right:1px solid #eee">No</td>
			                        <td style="border-right:1px solid #eee">Tanggal</td>
			                        <td style="border-right:1px solid #eee">Lowongan</td>
			                        <td style="border-right:1px solid #eee">Status</td>
			                        <td>Aksi</td>
			                      </tr>
			                    </thead>
			                    <tbody style="background:#E3D9D9">
			                    <?php $no=1;
			                    foreach ($lamaran as $dtlamaran) {
				    			  $id_loker=$dtlamaran['id_post'];
				    			  $sts=$dtlamaran['status'];?>
			                      <tr>
			                        <td align="center" style="width:30px;border-right:1px solid #eee"><?php echo $no;?></td>
			                        <td style="border-right:2px solid #eee"><?php echo $dtlamaran['tgl'];?></td>
			                        <td style="border-right:1px solid #eee"><?php echo $dtlamaran['judul'];?></td>
			                        <td align="center" style="border-right:1px solid #eee"><?php echo $sts;?></td>
			                        <td align="right"><?php if ($sts=="menunggu") { ?>
											<button title="terima <?php echo $nama ?>" class="btn btn-info" style="padding:2px 4px" onclick='swal({title: "Konfirmasi?",text: "Anda akan menerima dan menempatkan <?php echo $nama;?> sebagai karyawan baru", type: "warning",
				                              showCancelButton: true, confirmButtonColor: "#59BFCE", confirmButtonText: "Konfirm", closeOnConfirm: false },
				                              function(){ swal("Konfirm", "<?php echo $nama;?> menjadi anggota di perusahaan ini.", "success"); window.location.href="<?php echo base_url();?>perusahaan/pelamar/lamaran/konfirm/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>/<?php echo $dtpelamar['telp'] ?>"; });'><i class="fa fa-check">&nbsp</i>
				                           </button>
											<button title="hapus lamaran" class="btn btn-danger" style="padding:2px 4px" onclick='swal({title: "Hapus <?php echo $nama;?> ?",text: "Data ini akan dihapus secara permanen", type: "warning",
				                              showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Hapus", closeOnConfirm: false },
				                              function(){ swal("Hapus", "<?php echo $nama;?> berhasil di hapus.", "success"); window.location.href="<?php echo base_url();?>perusahaan/pelamar/lamaran/delete/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>/<?php echo $telp ?>"; });'><i class="fa fa-trash-o">&nbsp</i>
				                           </button>
				                           <a title="kirim pesan ke <?php echo $nama;?>" href="<?php echo base_url();?>perusahaan/home/mpl/<?php echo $id_perusahaan;?>/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>" class="btn btn-success" style="margin:0;padding:2px 4px"><i class="fa fa-send-o">&nbsp</i></a><?php
			    						}else{?>
			    							<button title="hapus lamaran" class="btn btn-danger" style="padding:2px 4px" onclick='swal({title: "Hapus <?php echo $nama;?> ?",text: "Data ini akan dihapus secara permanen", type: "warning",
				                              showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Hapus", closeOnConfirm: false },
				                              function(){ swal("Hapus", "<?php echo $nama;?> berhasil di hapus.", "success"); window.location.href="<?php echo base_url();?>perusahaan/pelamar/lamaran/delete/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>/<?php echo $telp ?>"; });'><i class="fa fa-trash-o">&nbsp</i>
				                           </button>
				                           <a title="kirim pesan ke <?php echo $nama;?>" href="<?php echo base_url();?>perusahaan/home/mpl/<?php echo $id_perusahaan;?>/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>" class="btn btn-success" style="margin:0;padding:2px 4px"><i class="fa fa-send-o">&nbsp</i></a><?php
			    						} ?>
				                    </td>
			                      </tr><?php
			                      $no++;
			                    }
			                    ?>
			                    </tbody>
			                  </table>
			                </div><!-- /.box-body -->
			                <button class="btn btn-default" onclick="history.back(-1)" style="margin:20px 0 20px 10px" >Kembali</button>
			            </div>
		    		</div>
	    		</div>
	    	</div>
		</div>