		<div class="column col-lg-9 col-md-9 col-sm-9 col-xs-12" id="main" style="text-align:left;background: #FFE0E0;font-size: 100%;box-shadow: -1px -1px 1px 1px;">
			<div class="scrollkonten" style="height:100%">
				<div class="tab-content" style="color:#000">
			  		<div class="tab-pane fade in active">
					    <h3 style="padding-left:20px;color:#7C0000">Data Lowongan Pekerjaan</h3>
					    <div class="row" style="margin:20px 0 0 -5px">
							<div class="col-xs-12">
				                <div class="box-body">
				                  <table id="example1" class="table table-striped">
				                    <thead  style="background:#C20000">
				                      <tr align="center" style="color:#fff;text-align:center;font-weight:bold">
				                        <td style="border-right:1px solid #eee">No</td>
				                        <td style="border-right:1px solid #eee">Lowongan</td>
				                        <td style="border-right:1px solid #eee">Jumlah Pelamar</td>
				                        <td style="border-right:1px solid #eee">Posting</td>
				                        <td style="border-right:1px solid #eee">Kuota</td>
			                        	<td>Aksi</td>
				                      </tr>
				                    </thead>
				                    <tbody style="background:#E3D9D9">
				                    <?php $no=1;
				                    foreach ($loker as $dataloker) { 
				                    	$jml=0;
				                    	$id_loker=$dataloker['id_post'];
				                    	foreach ($pelamar as $datapelamar) {
				                    		$id_lokerpelamar=$datapelamar['id_low'];
				                    		if ($id_loker==$id_lokerpelamar) {
				                    			$jml++;
				                    		}
				                    	}
				                    	$kuota=$dataloker['kuota'];
				                    	$sisa=$kuota-$jml; 
				                    	$stskt=$dataloker['status_kuota'];
				                    	if ($stskt=="buka") { $stskt="on";
				                    	}else{ $stskt="off";} ?>
				                      <tr>
				                        <td align="center" style="width:30px;border-right:1px solid #eee"><?php echo $no;?></td>
				                        <td style="border-right:1px solid #eee"><?php echo $dataloker['judul'];?></td>
				                        <td align="center" style="border-right:1px solid #eee"><?php echo $jml;?></td>
				                        <td align="center" style="border-right:1px solid #eee"><?php echo $dataloker['lstatus'];?></td>
				                        <td align="center" style="border-right:1px solid #eee"><?php echo "($sisa/$kuota) $stskt";?></td>
			                        	<td align="center">
				                        	<a title="lihat detail" href="<?php echo base_url();?>perusahaan/profile/loker/<?php echo $id_loker;?>" class="btn btn-default" style="margin:-5px 0 -5px 0;padding:2px 4px"><i class="fa fa-eye">&nbsp</i></a>
				                        	<a title="edit lowongan" href="<?php echo base_url();?>perusahaan/profile/post/edit/<?php echo $id_loker;?>" class="btn btn-warning" style="margin:0;padding:2px 4px"><i class="fa fa-edit">&nbsp</i></a>
					                        <button title="hapus lowongan <?php echo $dataloker['judul'] ?>" class="btn btn-danger" style="padding:2px 4px" onclick='swal({title: "Hapus <?php echo $dataloker['judul'];?> ?",text: "Data ini akan dihapus secara permanen", type: "warning",
				                              showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Hapus", closeOnConfirm: false },
				                              function(){ swal("Hapus", "<?php echo $dataloker['judul'];?> berhasil di hapus.", "success"); window.location.href="<?php echo base_url();?>perusahaan/profile/loker/delete/<?php echo $id_loker;?>"; });'><i class="fa fa-trash">&nbsp</i>
				                            </button><?php 
					                        if ($stskt=="on") {?>
					                        	<button title="Tutup Lowongan (<?php echo $dataloker['judul']?>)" class="btn btn-success" style="padding:2px 4px" onclick='swal({title: "Tutup lowongan <?php echo $dataloker['judul'];?> ?",text: "Lowongan ini akan ditutup sehingga user (pencari keja) tidak dapat  melihat dan mengirim lamaran untuk lowongan ini", type: "warning",
					                              showCancelButton: true, confirmButtonColor: "#62C162", confirmButtonText: "Tutup", closeOnConfirm: false },
					                              function(){ swal("Tutup", "<?php echo $dataloker['judul'];?> berhasil di tutup.", "success"); window.location.href="<?php echo base_url();?>perusahaan/home/off_p/<?php echo $id_loker;?>"; });'><i class="fa fa-lock">&nbsp</i>
					                            </button><?php
					                        }else{?>
					                        	<button title="Buka Lowongan (<?php echo $dataloker['judul']?>)" class="btn btn-info" style="padding:2px 4px" onclick='swal({title: "Buka lowongan <?php echo $dataloker['judul'];?> ?",text: "Lowongan ini akan dibuka sehingga user (pencari keja) dapat  melihat dan mengirim lamaran untuk lowongan ini", type: "warning",
					                              showCancelButton: true, confirmButtonColor: "#57A9B5", confirmButtonText: "Buka", closeOnConfirm: false },
					                              function(){ swal("Buka", "<?php echo $dataloker['judul'];?> berhasil di buka.", "success"); window.location.href="<?php echo base_url();?>perusahaan/home/on_p/<?php echo $id_loker;?>"; });'><i class="fa fa-unlock">&nbsp</i>
					                            </button><?php 
					                        } ?>
				                        </td>
				                      </tr><?php
				                      $no++;
				                    }
				                    ?>
				                    </tbody>
				                  </table>
				                  <button type="button" class="btn btn-default" style="margin:5px 0 0 0px;padding:5px 10px" data-toggle="collapse" data-target="#demo">
					        		Keterangan</button>
									<div id="demo" class="collapse" style="background:#FDEFEF;padding:2px 0 5px 5px;margin:0 0 25px 0px">
										<p>
										<table style="line-height:30px">
											<tr><td style="font-weight:bold">POSTING</td></tr>
											<tr><td valign="top" >shared</td><td valign="top">:&nbsp</td><td valign="top">Lowongan pekerjaan telah dibagikan kepada pelamar(pencari kerja)</td></tr>
											<tr><td valign="top" >pending</td><td valign="top">:&nbsp</td><td valign="top">Lowongan pekerjaan belum dibagikan kepada pelamar(pencari kerja), menunggu konfirmasi administrator</td></tr>
											<tr><td style="font-weight:bold">KUOTA</td></tr>
											<tr><td valign="top" >(x/y)</td><td valign="top">:&nbsp</td><td valign="top">Sisa kuota x, dari kuota awal y</td></tr>
											<tr><td valign="top" >on</td><td valign="top">:&nbsp</td><td valign="top">Lowongan masih terbuka, pelamar(pencari kerja) dapat mengakses</td></tr>
											<tr><td valign="top" >off</td><td valign="top">:&nbsp</td>
											<td valign="top">Lowongan sudah ditutup, pelamar(pencari kerja) tidak dapat mengakses</td></tr> 
										</table>
										<p>
												<p>- Perusahaan mempunyai kesempatan 1x24 jam untuk mengurangi/menambahi/mengedit info lowongan kerja yang akan diunggah.</p>
												<p>- 1x24 jam terhitung mulai dari perusahaan melakukan pembayaran.</p>
												<p>- Admin akan mengecek lowongan kerja sebelum diunggah ke KedaiLoker.com</p>
												<p>- Bila ada yang ingin ditanyakan silakan menghubungi Admin (0822.4365.7541) atau telepon 024 76583450</p>
											</p>
										</p>
									</div>
				                </div><!-- /.box-body -->
				            </div>
			    		</div>
			    	</div>
		  		</div>
			</div>
		</div>