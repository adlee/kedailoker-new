<!DOCTYPE html>
<html>
<head>
	<title>KEDAI LOKER</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/style.css">
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/script.js"></script>	
</head>
<body style="background-color:#FFE0E0">
	<form action="<?php echo base_url();?>perusahaan/setuju" method="post">
		<div id="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#C20000;">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="kedai" style="padding:5px 0 10px 0;text-align:center">Kondisi dan Ketentuan</div>
			</div>
	    </div>
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px;color:#000">
	    	<p style="font-weight:bold;font-size:150%;text-decoration:underline">Definisi</p>
		    <table style="line-height:22px;margin-top:-10px;text-align:justify">
		    	<tr><td colspan="3">Yang dimaksud dengan :</td>
		    	</tr>
		    	<tr><td style="width:30px"><b>1.</b></td>
		    		<td colspan="2">Perusahaan adalah kedailoker.com dan/atau PT. Anindita Madia Group</td>
		    	</tr>
		    	<tr><td><b>2.</b></td>
		    		<td colspan="2"><b>Pihak Ketiga</b> yang disebut <b>Pelanggan</b> adalah organisasi atau individu yang menggunakan kedailoker.com untuk keperluan :</td>
		    	</tr>
		    	<tr><td> </td>
		    		<td style="width:20px">a)</td>
		    		<td>Rekruitmen, memasang iklan atau kepentingan lainya</td>
		    	</tr>
		    	<tr><td> </td>
		    		<td style="width:20px">b)</td>
		    		<td>Memasang iklan lowongan atau peluang kerja atau untuk mendapat informasi secara umum</td>
		    	</tr>
		    	<tr><td><b>3.</b></td>
		    		<td colspan="2"><b>Premium service </b>adalah layanan perusahaan bagi pelanggan yang membayar sejumlah biaya untuk keperluan :</td>
		    	</tr>
		    	<tr><td> </td>
		    		<td style="width:20px">a)</td>
		    		<td>Rekruitmen, memasang iklan atau kepentingan lainya</td>
		    	</tr>
		    	<tr><td> </td>
		    		<td style="width:20px">b)</td>
		    		<td>Memasang iklan lowongan atau peluang kerja atau untuk mendapat informasi secara umum</td>
		    	</tr>
		    </table>
		    <p style="font-weight:bold;font-size:150%;text-decoration:underline;margin-top:20px">Kondisi dan Ketentuan</p>
		    <table style="line-height:22px;margin-top:-10px;text-align:justify">
		    	<tr><td style="width:30px" valign="top"><b>1.</b></td>
		    		<td colspan="2">Perusahaan berhak menolak memberikan pelayanan kepada pelanggan yang dianggap melanggar kebijaksanaan Perusahaan, dimana 
		    		interpretasinya menjadi hak Perusahaan sepenuhnya. Keputusan Perusahaan untuk menolak melayani atau menghentikan layanan bersifat mutlak dan 
		    		tidak dapat diganggu gugat. Bila pelanggan telah melakukan pembayaran terhadap Perusahaan tetapi ditolak atau diberhentikan keanggotaanya, 
		    		maka pe;anggan berhak emminta kembali pembayaran, dengan dikenakan pemotongan biaya sebagaimana ditentukan oleh perusahaan</td>
		    	</tr>
		    	<tr><td style="width:20px" valign="top"><b>2.</b></td>
		    		<td colspan="2">Pelanggal setuju untuk tidak memasang iklan yang tidak benar, menyesatkan , melecehkan, membangkitkan kebencian, 
		    		menuntut biaya, memfitnah, bersifat deskriminatif, terhadap suku agama dan ras tertentu ataupun menyinggung prinsip keagamaan</td>
		    	</tr>
		    	<tr><td style="width:20px" valign="top"><b>3.</b></td>
		    		<td colspan="2">Pelanggan merupakan satu-satunya pihak yang bertanggung jawab penuh atas informasi yang dipasang di kedailoker.com</td>
		    	</tr>
		    	<tr><td style="width:20px" valign="top"><b>4.</b></td>
		    		<td colspan="2">Pelanggan seuju untuk membebaskan perusahaan dan atau seluruh karyawanya dari tuntutan yang timbul akibat kerugian, 
		    		hilangnya uang dan sebagainya, yang terjadi akibat penggunaan layanan yang disediakan oleh perusahaan atau pengguna situs-situs 
		    		dan link lainya yang terkait</td>
		    	</tr>
		    	<tr><td style="width:20px" valign="top"><b>5.</b></td>
		    		<td colspan="2">Pelanggan tidak diperkenankan menggunakan data yang diperoleh dari kedailoker.com untuk tujuan lain diluar tujuan 
		    		untuk mengisi lowongan pekerjaan atau peluang pekerjaan yang dimiliki pelanggan. Pelanggaran terhadap ketentuan ini dapat diperkarakan 
		    		ke pengadilan oleh perusahaan dan atau pihak-pihak yang dirugikan</td>
		    	</tr>
		    	<tr><td style="width:20px" valign="top"><b>6.</b></td>
		    		<td colspan="2">Pelanggan sepakat untuk menyetujui syarat dan ketentuan lain yang mungkin akan ditambahkan oleh perusahaan dari waktu 
		    		ke waktu tanpa pemberitahuan sebelumnya kepada pelanggan</td>
		    	</tr>
		    </table>
	    </div>
	    <div style="margin:0px 20px 5px 0;float:right;padding:10px 30px">
	    	<a href="<?php echo base_url();?>logout" style="padding:10px 15px" class="kembali">Tidak Setuju</a>
	    	<input type="submit" class="tombol" style="padding:9px 15px" value="Setuju">
	    </div>
	 </form>
</body>
</html>