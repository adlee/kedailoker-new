		<div class="column col-lg-9 col-md-9 col-sm-9 col-xs-12" id="main" style="text-align:left;background: #FFE0E0;font-size: 100%;box-shadow: -1px -1px 1px 1px;">
			<div>
				<div class="tab-content" style="color:#000">
			  		<div class="tab-pane fade in active" style="margin-left:-10px">
			  		<?php 
			  		foreach ($ket as $ketr) {
			  			$id=$ketr['id'];
			  		} ?>
			  		<form action="<?php echo base_url()?>perusahaan/profile/uploadloker/<?php echo $id ?>" method="post">
					    <h3 style="padding-left:30px;margin-bottom:25px;color:#7C0000">Upload Lowongan Pekerjaan</h3>
					    <div class="col-lg-12" style="margin-bottom:5px">
					    	<div class="col-lg-3 col-md-3 col-sm-4" style="margin-top:5px">Lowongan</div>
							<div class="col-lg-9 col-md-9 col-sm-8">
					    		<input type="text" class="form-control" name="judul" value=""  placeholder="Jenis pekerjaan" required>
					    	</div>
					    </div>
					    <div class="col-lg-12" style="margin-bottom:5px">
					    	<div class="col-lg-3 col-md-3 col-sm-4" style="margin-top:5px">Kuota</div>
							<div class="col-lg-9 col-md-9 col-sm-8">
					    		<input type="text" class="form-control" name="kuota" value=""  placeholder="Banyak orang yang dibutuhkan"  onkeypress="return numbers(event)" required>
					    	</div>
					    </div>
					    <div class="col-lg-12" style="margin-bottom:15px;margin-top:10px">
					    	<div class="col-lg-3 col-md-3 col-sm-4" style="margin-top:5px">Minimal pendidikan</div>
							<div class="col-lg-9 col-md-9 col-sm-8">
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek1" value="SD" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp SD</div></div>
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek2" value="SMP" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp SMP</div></div>
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek3" value="SMA" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp SMA</div></div>
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek4" value="SMK" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp SMK</div></div>
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek5" value="D1" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp D1</div></div>
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek6" value="D2" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp D2</div></div>
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek7" value="D3" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp D3</div></div>
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek8" value="D4" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp D4</div></div>
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek9" value="S1" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp S1</div></div>
								<div style="margin:0;padding:0" class="col-lg-2 col-md-2 col-sm-2"><input type="checkbox" name="cek10" value="S2" style="margin-top:5px;float:left"><div style="margin-top:1px">&nbsp S2</div></div>
					    	</div>
					    </div>
					    <div class="col-lg-12" style="margin-bottom:5px">
					    	<div class="col-lg-3 col-md-3 col-sm-4" style="margin-top:5px">Syarat dan ketentuan</div>
							<div class="col-lg-9 col-md-9 col-sm-8">
					    		<textarea id="mytextarea" class="form-control" name="isi" id="isi" cols="30" rows="5" placeholder="menguasai x, menguasi y, dll"></textarea>
					    	</div>
					    </div>
						<div class="col-lg-12" style="margin-bottom:5px">
					    	<div class="col-lg-3 col-md-3 col-sm-4" style="margin-top:5px">Range gaji</div>
							<div class="col-lg-9 col-md-9 col-sm-8">
								<div class="form-group col-lg-6  col-md-6 col-sm-12 col-xs-12">
					              <div class="input-group" style="margin-left:-15px">
					                <div class="input-group-addon">
					                  RP
					                </div>
					                <input type="text" class="form-control" name="gaji1" value=""  onkeypress="return numbers(event)" required>
					              </div><!-- /.input group -->
					            </div>
								<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
					              <div class="input-group" style="margin-left:-15px">
					                <div class="input-group-addon">
					                  RP
					                </div>
					                <input type="text" class="form-control" name="gaji2" value=""  onkeypress="return numbers(event)" required>
					              </div><!-- /.input group -->
					            </div>
					        </div>
					    </div>

					    <div class="col-lg-12" style="margin-bottom:5px">
					    	<div class="col-lg-3 col-md-3 col-sm-4" style="margin-top:5px">Masa berlaku</div>
							<div class="col-lg-9 col-md-9 col-sm-8">
								<div class="form-group col-lg-12  col-md-12 col-sm-12 col-xs-12">
					              <div class="input-group" style="margin-left:-15px">
					                <div class="input-group-addon">
					                  Bulanan
					                </div> 
					                <select name="masa_aktif" class="form-control">
					                	<?php 	$a1 = (rand()%9);	$a11 = (rand()%9);	$a12 = (rand()%9);
												$a2 = (rand()%9);	$a21 = (rand()%9);	$a22 = (rand()%9);
												$a3 = (rand()%9);	$a31 = (rand()%9);	$a32 = (rand()%9);
										 ?>
										<!-- <option value="<?php echo "50$a1$a11$a12";?>">1 bulan = Rp <?php echo "50.$a1$a11$a12";?></option> -->
										<option value="<?php echo "100$a2$a21$a22";?>">1 bulan = Rp <?php echo "100.$a2$a21$a22";?></option><!-- 
										<option value="<?php echo "150$a3$a31$a32";?>">3 bulan = Rp <?php echo "150.$a3$a31$a32";?></option> -->
					                </select>
					                <!-- <input type="text" class="form-control" name="gaji1" value=""  onkeypress="return numbers(event)" required> -->
					              </div><!-- /.input group -->
					            </div><br/>
					            <p>Ada penawaran menarik bila ingin mendapatkan paket terusan. Silakan kirim pesan di WA 0822.4365.7541 atau telepon 024 76583450.</p>

					            <input type="submit" class="tombol" style="padding:10px 20px" value="Simpan">
					            <button class="tombolon" style=";background:gray;margin-bottom:15px;margin-left:10px;padding:10px 10px" onclick='swal({title: "Ketentuan share",   
							        text: "Perusahaan dikenakan biaya untuk share lowongan guna masa berlaku lowongan tersebut dengan biaya yang telah ditentukan, konfirmasikan pembayaran anda ke administrator KEDAILOKER untuk segera diproses lebih lanjut untuk share lowongan kepada pencari lowongan",  showCancelButton: false,   closeOnConfirm: false,   animation: "slide-from-top",   
							        });'>Ketentuan share lowongan
							    </button>

					        </div>
					    </div>
					</form>
			    	</div>
		  		</div>
			</div>
		</div>
