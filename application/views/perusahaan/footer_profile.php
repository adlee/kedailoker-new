<!-- Modal -->
	    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	                    <h4 class="modal-title" id="myModalLabel" style="text-align:center">Profile</h4>
	                </div>
	                <form>
		                <div class="modal-body">
		                    <center> <?php 
		             	       	foreach ($ket as $eachuser) {
							  		$img=$eachuser['foto'];
							  		$data=explode(".", $eachuser['foto']);
					                $foto=$data[0];
					                $type=$data[1];
					                if ($type=="jpeg") {?>
					                	<img style="margin:15px 0;border-radius:20px" id="gambar" src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $foto?>.jpg" width="145" border="0"><span id="nav-upload-icon" class="glyphicon glyphicon-camera" aria-hidden="true"></span>
					                <?php
					                }else{?>
					                	<img style="margin:15px 0;border-radius:20px" id="gambar" src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $img?>" width="145" border="0"><span id="nav-upload-icon" class="glyphicon glyphicon-camera" aria-hidden="true"></span>
					                <?php
					                }
							   	}  ?>
			                    <h3 class="media-heading"><?php 
					    			foreach ($ket as $eachuser) {
					    				$alamat=$eachuser['alamat'];?>
					    				<p><?php echo $eachuser['nama_per'] ?></p>
					    				<span class="label label-info">region <?php echo $eachuser['region']." negeri" ?></span><?php
					    			}?> 
		                    </center><hr>
		                    <center>
			                    <p class="text-left" style="padding-left:15px"><strong>Alamat </strong><br>
			                        <?php echo $alamat ?>
			                    </p>
			                    <br>
		                    </center>
		                </div>
	                </form>
	            </div>
        	</div>
    	</div>
    </div>
    <script>
    	$('#gambar').on('click', function() {
		    $('#profile-image-upload').click();
		});
    </script>
</body>
</html>