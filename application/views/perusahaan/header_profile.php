<?php
  if ($this->session->userdata('nom')==null) {
    redirect('perusahaan');
  }else{
  	foreach ($status as $status) {
  		if ($status['status']=="off") {
  			redirect('perusahaan/persetujuan');	
  		}else{
?>
<!DOCTYPE html>
<html>
<head>
	<title>KEDAI LOKER (PERUSAHAAN)</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/style.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/script.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/image_preview.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/script.js"></script>
	<!-- sweet alert -->
    <script src="<?php echo base_url();?>dist/js/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/sweetalert.css">
    <!-- editor -->
    <script type="text/javascript" src="<?php echo base_url();?>dist/js/tinymce/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/tinymce/tinymce.min.js"></script>
	<!--dropdown-->
	<link rel="stylesheet" href="<?php echo base_url();?>dist/css/AdminLTE.min.css">
    <script src="<?php echo base_url();?>plugins/jQuery/jQuery_1.2.4.min.js"></script>
    <script src="<?php echo base_url();?>dist/js/app.min.js"></script>
	<!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<style>
	  #nav-upload-icon{
        top: 10px;
        left: 10px;
        z-index: 1000;
	    }
			#gambar:hover + #nav-upload-icon,  
		#nav-upload-icon:hover {
		    visibility: visible;
		}
		#gambar:hover{
			opacity: 0.8;
		}

		#nav-upload-icon { 
		    visibility : hidden; 
		    position:absolute;
		    z-index:50000
		    margin:0 auto; left:49%;top:130px;
		    color:white;
		}
		#li a:hover{
			background: #9e9999;
		}
		.aktif{
			background: #eee;
		}
		.begron{
		background-color: #002060;
		}
	</style>
	<script>
	  tinymce.init({
	    selector: '#mytextarea',
	    menubar:false,
	    statusbar: false
	  });
  	</script>
</head>
<body style="background-color:#002060">
	<div id="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#002060">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<a href="<?php echo base_url();?>perusahaan/home"><div class="kedai">kedailoker</div></a>
			<div class="imgakun" >
				<button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown" style="margin:-5px 0 0px -8px;padding:10px 10px 0 15px;border:none;width:190px"><p style="font-size:100%;width:155px;overflow:hidden;font-weight:bold;color:#000;float:left;margin-right:5px">
				Hello, <?php 
		    		foreach ($ket as $eachuser) {
		    			$id_perusahaan=$eachuser['id'];
		    			$nm_perusahaan=$eachuser['nama_per'];
		    			$jml=0;
		    			$nama= explode(" ", $eachuser['nama_per']);
		    			$namadepan=$nama[0];
		    			echo $eachuser['nama_per']."&nbsp</p><span class='caret'></span>";
		    		} ?>
				</button>
				<ul class="dropdown-menu" style="margin:-1px 0 0 21px">
				   <li><a href="<?php echo base_url();?>logout">LOGOUT</a></li>
				</ul>
			</div>	
		</div>
		<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 kedai" style="text-align:center;margin:10px 0">PROFIL PERUSAHAAN</div>
		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12" style="text-align:center;margin:4px 0 0 0"><?php 
			foreach ($ket as $eachuser) {
		  		$img=$eachuser['foto'];
		  		$data=explode(".", $eachuser['foto']);
                $foto=$data[0];
                $type=$data[1];
                if ($type=="jpeg") {?>
                	<a href="#aboutModal" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $foto?>.jpg" title="foto" width=65px height=65px  id="pp"></a>
                <?php
                }else{?>
                	<a href="#aboutModal" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $img?>" title="foto" width=65px height=65px  id="pp"></a>
                <?php
                }
		   	}  ?>
		</div>
	</div>
	<div class="row" >
        <div class="column col-lg-3 col-md-3 col-sm-3 col-xs-12" id="main" style="text-align:left;background: #002060;font-size: 100%;box-shadow: -2px -1px 1px 1px;">
        	<ul class="nav nav-tabs nav-stacked" style="border:none;font-weight:bold">
			  	<li style="padding:0"><a href="<?php echo base_url()?>perusahaan/profile" style="padding:7px 5px 7px 20px;color:white;font-size:16px" >
			  	Data Perusahaan</a></li>
			 	<li style="border-top:1px solid gray"><a href="<?php echo base_url()?>perusahaan/profile/loker" style="padding:7px 5px 7px 20px;color:white;font-size:16px">
			 	Data Lowongan Pekejaan</a></li>
			  	<li style="border-top:1px solid gray"><a href="<?php echo base_url()?>perusahaan/profile/uploadloker" style="padding:7px 5px 7px 20px;color:white;font-size:16px">
			  	Upload Lowongan Pekerjaan</a></li>
			  	<li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/petunjuk_penggunaan.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:16px">
		        Download Manual Penggunaan</a></li>
			  	<section class="sidebar" style="border-top:1px solid gray">
		          <ul class="sidebar-menu">
		            <li class="treeview" id="li">
		              <a href="" style="padding:10px 5px 10px 20px;color:white;font-size:16px">
		                &nbspHELP / TUTORIAL<i class="fa fa-angle-left pull-right"></i>
		              </a>
		              <ul class="treeview-menu" >
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/informasi_umum_perusahaan.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Informasi umum penggunaan</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/ubah_profil_perusahaan.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Ubah profil perusahaan</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/upload_loker.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Upload lowongan pekerjaan</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/lihat_loker.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Lihat lowongan pekerjaan</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/edit_loker.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Edit lowongan pekerjaan</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/buka_tutup_loker.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Buka/ tutup lowongan</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/hapus_loker.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Hapus lowongan</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/lihat_pelamar.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Lihat pelamar kerja</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/terima_pelamar.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Terima pelamar kerja</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/hapus_pelamar.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Hapus pelamar kerja</a>
		                </li>
		                <li style="border-top:1px solid gray"><a target="blank" href="<?php echo base_url()?>dist/tutorial/kirim_pesan.pdf" style="padding:7px 5px 7px 20px;color:white;font-size:13px"><i class="fa fa-angle-right"></i>
		                	Kirim pesan ke pelamar</a>
		                </li>
		              </ul>
		            </li>
		          </ul>
		        </section>
			</ul>
		</div>
<?php } } } ?>