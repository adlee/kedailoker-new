		<div class="column col-lg-6 col-md-6 col-sm-6 col-xs-12" id="main" style="text-align:left;background: #FFE0E0;font-size: 100%;box-shadow: -1px -1px 1px 1px;">
        	<div class="scrollkonten" style="height:100%">
        		<div class="table-responsive" style="margin:0 0 0 0px">
			    	<table class="table table-hover"><?php 
			    		foreach ($ket as $eachuser) {
			    			$id_perusahaan=$eachuser['id'];
			    			$nm_perusahaan=$eachuser['nama_per'];
			    		}
		    			foreach ($pilih as $postingan) {
		    				$id_loker=$postingan['id_post'];
			                $g1=$postingan['gaji_min'];
			                $g2=$postingan['gaji_max'];
			                ?>
		    				<tr style="background:#F1CCCC;"><td colspan="3"><h3 style="margin-top:5px;color:#7C0000">Data Lowongan</h3></td></tr>
		    				<tr><td>Lowongan</td><td>:</td><td> <?php echo $postingan['judul'] ?></td></tr>
		    				<tr><td>Tanggal post</td><td>:</td><td> <?php echo $postingan['tgl']?></td></tr>
		    				<tr><td>Syarat dan ketentuan</td><td>:</td><td> <?php echo $postingan['isi'] ?></td></tr>
		    				<tr><td>Minimal Pendidikan</td><td>:</td><td> <?php echo $postingan['pendidikan'] ?></td></tr>
		    				<tr><td>Gaji</td><td>:</td><td> 
		    					<?php
		    					$harga1=number_format($g1,0,",",".");
		    					$harga2=number_format($g2,0,",","."); 
		    					echo "Rp. $harga1 -  Rp $harga2";?></td></tr>
		    				<tr><td>Status</td><td>:</td><td> <?php 
		    					if ($postingan['status']=="pending") {
		    						echo "Pending (menunggu konfirmasi admin)";
		    					}else{
		    						echo "Shared (lowongan telah di publikasikan)";
		    					} ?>
		    					</td>
		    				</tr><?php
		    			}?>
		    		</table>
	    		</div>
	    		<div class="table-responsive">
				    <div class="row">
						<div style="width:100%">
			                <div class="box-body">
			                  <table id="example1" class="table table-striped">
			                    <thead  style="background:#C20000">
			                      <tr style="background:#F1CCCC;"><td colspan="5"><h3 style="margin-top:5px;color:#7C0000">Data Pelamar</h3></td></tr>
			                      <tr align="center" style="color:#fff;text-align:center;font-weight:bold">
			                        <td style="border-right:1px solid #eee">No</td>
			                        <td style="border-right:1px solid #eee">Tanggal</td>
			                        <td style="border-right:1px solid #eee">Nama</td>
			                        <td style="border-right:1px solid #eee">Status</td>
			                        <td>Aksi</td>
			                      </tr>
			                    </thead>
			                    <tbody style="background:#E3D9D9">
			                    <?php $no=1;
			                    foreach ($datapelamar as $dtpelamar) {
				    			  $id_pelamar=$dtpelamar['idpel'];
				    			  $sts=$dtpelamar['status'];?>
			                      <tr>
			                        <td align="center" style="width:30px;border-right:1px solid #eee"><?php echo $no;?></td>
			                        <td style="border-right:2px solid #eee"><?php echo $dtpelamar['tgl'];?></td>
			                        <td align="center" style="border-right:1px solid #eee"><?php echo $dtpelamar['username'];?></td>
			                        <td align="center" style="border-right:1px solid #eee"><?php echo $sts;?></td>
			                        <td align="right"><?php 
			                        	if ($sts=="menunggu") { ?>
			                        	<a title="lihat rincian <?php echo $dtpelamar['username'];?>" href="<?php echo base_url();?>perusahaan/home/post/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>/<?php echo $dtpelamar['telp'];?>" class="btn btn-default" style="margin:0;padding:2px 4px"><i class="fa fa-eye">&nbsp</i></a>
										<button title="terima <?php echo $dtpelamar['username'];?>" class="btn btn-info" style="padding:2px 4px" onclick='swal({title: "Konfirmasi?",text: "Anda akan menerima dan menempatkan <?php echo $dtpelamar['username'];?> sebagai karyawan baru", type: "warning",
			                              showCancelButton: true, confirmButtonColor: "#59BFCE", confirmButtonText: "Konfirm", closeOnConfirm: false },
			                              function(){ swal("Konfirm", "<?php echo $dtpelamar['username'];?> menjadi anggota di perusahaan ini.", "success"); window.location.href="<?php echo base_url();?>perusahaan/post/pelamar/konfirm/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>"; });'><i class="fa fa-check">&nbsp</i>
			                            </button>
										<button title="hapus <?php echo $dtpelamar['username'] ?>" class="btn btn-danger" style="padding:2px 4px" onclick='swal({title: "Hapus <?php echo $dtpelamar['username'];?> ?",text: "Data ini akan dihapus secara permanen", type: "warning",
			                              showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Hapus", closeOnConfirm: false },
			                              function(){ swal("Hapus", "<?php echo $dtpelamar['username'];?> berhasil di hapus.", "success"); window.location.href="<?php echo base_url();?>perusahaan/post/pelamar/delete/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>"; });'><i class="fa fa-trash-o">&nbsp</i>
			                            </button>
			                            <a title="kirim pesan ke <?php echo $dtpelamar['username'];?>" href="<?php echo base_url();?>perusahaan/home/mp/<?php echo $id_perusahaan;?>/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>" class="btn btn-success" style="margin:0;padding:2px 4px"><i class="fa fa-send-o">&nbsp</i></a><?php
		    						}else{?>
		    							<a title="lihat rincian <?php echo $dtpelamar['username'];?>" href="<?php echo base_url();?>perusahaan/home/post/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>/<?php echo $dtpelamar['telp'];?>" class="btn btn-default" style="margin:0;padding:2px 4px"><i class="fa fa-eye">&nbsp</i></a>
		    							<button title="hapus <?php echo $dtpelamar['username'] ?>" class="btn btn-danger" style="padding:2px 4px" onclick='swal({title: "Hapus <?php echo $dtpelamar['username'];?> ?",text: "Data ini akan dihapus secara permanen", type: "warning",
			                              showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Hapus", closeOnConfirm: false },
			                              function(){ swal("Hapus", "<?php echo $dtpelamar['username'];?> berhasil di hapus.", "success"); window.location.href="<?php echo base_url();?>perusahaan/post/pelamar/delete/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>"; });'><i class="fa fa-trash-o">&nbsp</i>
			                           </button>
			                           <a title="kirim pesan ke <?php echo $dtpelamar['username'];?>" href="<?php echo base_url();?>perusahaan/home/mp/<?php echo $id_perusahaan;?>/<?php echo $id_loker;?>/<?php echo $id_pelamar;?>" class="btn btn-success" style="margin:0;padding:2px 4px"><i class="fa fa-send-o">&nbsp</i></a><?php
			                        } ?>
				                    </td>
			                      </tr><?php
			                      $no++;
			                    }
			                    ?>
			                    </tbody>
			                  </table>
			                </div><!-- /.box-body -->
			            </div>
		    		</div>
	    		</div>
	    	</div>
		</div>