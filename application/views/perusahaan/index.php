<!DOCTYPE html>
<html>
<head>
	<title>KEDAI LOKER</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/style.css" />
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/script.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/ajax_kota.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.validate.js"></script>

</head>
<body class="begron">
	<div id="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#002060;">
		<div class="col-lg-5 col-md-4 col-sm-3 col-xs-12" style="background:none;margin:10px 0;font-size:25pt;font-weight:bold;color:#FFC600;"><a href="<?=site_url()?>"><img src="<?= base_url('dist/img/KL3.png')?>" style="width: 200px;"></a></div>
		<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12" style="background:#002060;margin-bottom:-20px">
			<div style="padding:8px 5px;margin:10px 0 5px 0;"></div>
		</div>
		<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12" style="background:#002060;margin-top:15px">
			<form action="<?php echo base_url();?>perusahaan/login" method="post">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin:0 0 2px 1px;background:none;padding:0 2px 0 2px">
					<label class="col-lg-12 col-md-12 col-sm-12 col-xs-0" style="margin:7px 0 0 0;padding:0;color:#fff;font-weight:normal;font-size:90%;">
					Email</label>
					<input  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" type="email" name="mail" required> 				
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin:0 0 -20px 1px;background:none;padding:0 2px 0 2px">
					<label class="col-lg-12 col-md-12 col-sm-12 col-xs-0" style="margin:7px 0 0 0;padding:0;color:#fff;font-weight:normal;font-size:90%;">
					Sandi</label>
					<input  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" type="password" name="sandi" required> 				
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin:0 0 5px 1px;background:none;padding:0 2px 0 2px;margin-top:24px">
					<input  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tombol" style="padding:3.5px 0;" type="submit" name="" value="Masuk" > 				
				</div>
			</form>
		</div>
    </div>

	<div style="color:black">
		<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12" style="font-family: arial;text-align: left;padding: 10px 17px 0 25px;">
			Kedailoker
			<ul>
			  <li style="text-align:justify">
				Membantu pencarian kerja mendapatkan informasi lowongan kerja langsung dari perusahaan yang membuka 
				lowongan kerja melalui SMS langsung ke seluler, dan pencari kerja dapat langsung mengirim lamaran ke 
				Perusahaan
			  </li>
			  <li style="text-align:justify">
			  	Membantu HRD perusahaan mendapatkan tenaga kerja sesuai klasifikasi yang dibutuhkan secara cepat dan 
			  	tepat
			  </li>
			</ul>
			<img src="<?php echo base_url();?>dist/img/promo.png" width="99%">
		</div>
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
			<h1 style="margin:10px 0 0 0;">Daftar</h1>
			<p style="padding:2px">Sesuai dengan legalitas perusahaan</p>
			<form action="<?php echo base_url();?>perusahaan/daftar" method="post" class="formdaftar" id="formdaftar" style="padding:2px">
				<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Nama perusahaan" required name="nama" id="nama">
				<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" maxlength="12" placeholder="Nomor telepon perusahaan" required name="telepon" id="telepon" onkeydown="return numbers(event);">
				<select class="input col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="height:35px" name="region" required>
					<option value="dalam">Dalam Negeri</option>
					<option value="luar">Luar Negeri</option>
				</select><input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Alamat lengkap perusahaan" required name="alamat" id="alamat">
				<select class="input col-lg-5 col-md-5 col-sm-5 col-xs-5 input" style="height:35px;margin-right:5px" name="prop" id="prop" onchange="ajaxkota(this.value)" required>
					<option value="">Provinsi</option><?php 
					foreach ($perusahaan as $lokasi) {
						echo '<option value="'.$lokasi['lokasi_propinsi'].'">'.$lokasi['lokasi_nama'].'</option>';
					}?>
				</select>

				<select class="input col-lg-5 col-md-5 col-sm-5 col-xs-5 input" style="height:35px" name="kota" id="kota" required>
					<option>Kabupaten/ Kota</option>
				</select>
				<p style="clear:both;color:red;font-size:80%">* wajib diisi untuk region dalam negeri</p>
				<p style="clear:both">HRD/ Recrutmen</p>
				<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Nama lengkap" required name="nama_hrd" id="nama_hrd">
				<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Nomor telepon seluler" required name="teleponhrd" id="teleponhrd" onkeydown="return numbers(event);">
				<input type="email" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Alamat email" required name="email" id="email">
				<input type="password" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Kata sandi" required name="sandi" id="sandi">
				<input type="password" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Masukan ulang kata sandi" required name="repeat" id="repeat">
				 &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
				<p>Dengan mengklik daftar, anda kami nyatakan setuju dengan <a href="<?= site_url('sk_perusahaan')?>" target="_blank" style="color: blue;">Syarat dan Ketentuan Perusahaan</a> </p>
				<input type="submit" class="tombol btn btn-block" style="padding:5px 30px;margin:7px 0 14px 0" name="daftar" value="Daftar">
				<a href="<?php echo base_url();?>" class="kembali btn btn-block" style="padding:5px 30px;margin:7px 0 14px 0;clear:both">kembali</a>
			</form>
		</div>
	</div>
	<script>
	$().ready(function() {
		// validate the comment form when it is submitted
		$("#formdaftar").validate({
			rules: {
				nama: {
                	required: true
                },

				telepon: {
					required: true,
					minlength: 3,
					remote: {
                        url: "<?php echo base_url();?>dist/support/cek_nomor_perusahaan.php",
                        type: "post"
                     }
				},

				alamat: {
                	required: true
                },
                prop: {
                	required: true
                },

				kota: {
                	required: true
                },

				nama_hrd: {
                	required: true
                },

				teleponhrd: {
					required: true,
					minlength: 3,
					remote: {
                        url: "<?php echo base_url();?>dist/support/cek_nomor_hrd.php",
                        type: "post"
                     }
				},

				email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "<?php echo base_url();?>dist/support/cek_email_perusahaan.php",
                        type: "post"
                     }
                },

				sandi: {
					required: true,
					minlength: 5
				},

				repeat: {
					required: true,
					minlength: 5,
					equalTo: "#sandi"
				}
			},
			messages: {
				nama: {
                    required: "Nama tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp &nbsp",
                    minlength: "Nama harus 3 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
                },
                telepon: {
					required: "Nomor telepon tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp ",
					minlength: "Nomor harus 3 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					remote: "Nomor sudah dipakai &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
				},
				alamat: {
                    required: "Alamat tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
                },
                prop: {
                    required: " &nbsp",
                },
                nama_hrd: {
                    required: "Nama tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp",
                    minlength: "Nama harus 3 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
                },
				teleponhrd: {
					required: "Nomor telepon tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					minlength: "Nomor harus 3 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp ",
					remote: "Nomor sudah dipakai &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
				},
				email: {
                    required: "Email tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
                    email: "Email ini tidak valid &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
                    remote: "Email sudah dipakai &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp "
                },
				sandi: {
					required: "Lengkapi sandi unik anda &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					minlength: "Password harus 5 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
				},
				repeat: {
					required: "Kolom ini tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					minlength: "Password harus 5 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					equalTo: "Konfirmasi password harus sama &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
				}
			}
		});
	});
	</script>
</body>
</html>