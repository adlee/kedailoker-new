		<div class="column col-lg-6 col-md-6 col-sm-6 col-xs-12" id="main" style="text-align:left;background: #FFE0E0;font-size: 100%;box-shadow: -1px -1px 1px 1px;">
        	<div class="scrollkonten" style="height:100%">
	        	<div class="table-responsive" style="margin:0 5px 0 0px">
			    	<table class="table table-hover"><?php 
		    			foreach ($pilih as $postingan) {
		    				$id_loker=$postingan['id_post'];
			                $g1=$postingan['gaji_min'];
			                $g2=$postingan['gaji_max'];
			                ?>
			                <tr style="background:#F1CCCC;"><td colspan="3"><h3 style="margin-top:5px;color:#7C0000">Lowongan (<?php echo $postingan['judul'];?>)</h3></td></tr>
		    				<tr><td>Syarat dan ketentuan</td><td>:</td><td> <?php echo $postingan['isi'] ?></td></tr>
		    				<tr><td>Minimal Pendidikan</td><td>:</td><td> <?php echo $postingan['pendidikan'] ?></td></tr>
		    				<tr><td>Gaji</td><td>:</td><td> 
		    					<?php
		    					$harga1=number_format($g1,0,",",".");
		    					$harga2=number_format($g2,0,",","."); 
		    					echo "Rp. $harga1 -  Rp $harga2";?></td></tr>
		    				<?php
		    			}?>
		    		</table>
	    		</div>
	    		<div class="table-responsive" style="margin:0px 5px 0 0px">
			    	<table class="table table-hover"><?php
		    			foreach ($pelamary as $dtpelamar) {
		    				$sts=$dtpelamar['status'];?>
		    				<tr style="background:#F1CCCC;"><td colspan="3"><h3 style="margin-top:5px;color:#7C0000">Pelamar (<?php echo $dtpelamar['username'] ?>)</h3></td></tr>
		    				<tr><td>Jenis Kelamin</td><td>:</td><td> <?php 
		    						if ($dtpelamar['jk']=='L') { echo "Laki-Laki";
		    						}else{ echo "Perempuan";} ?>
		    					</td>
		    				</tr>
		    				<tr><td>Telepon</td><td>:</td><td> <?php echo $dtpelamar['telp']?></td></tr>
		    				<tr><td>Email</td><td>:</td><td> <?php echo $dtpelamar['email'] ?></td></tr>
		    				<tr><td>Foto</td><td>:</td><td> <?php 
							  		$img=$dtpelamar['foto'];
							  		$data=explode(".", $dtpelamar['foto']);
					                $foto=$data[0];
					                $type=$data[1];
					                if ($type=="jpeg") {?>
					                	<img src="<?php echo base_url(); ?>dist/img/<?php echo $foto?>.jpg" title="foto" width=150px id="pp"></a>
					                <?php
					                }else{?>
					                	<img src="<?php echo base_url(); ?>dist/img/<?php echo $img?>" title="foto" width=150px id="pp"></a>
					                <?php
					                }?>
								</td>
							</tr>
							<tr><td>Pend. Informal</td><td>:</td>
								<td><?php $no=1; foreach ($informal as $pendinformal) { 	
										echo $no.". ".$pendinformal['pendidikan']."<br/>"; 
										$no++;
									} ?>
								</td>
							</tr><tr><td>Peng. Kerja</td><td>:</td>
								<td><?php $no=1; foreach ($pengalaman as $pengkerja) { 	
										echo $no.". ".$pengkerja['pengalaman']."<br/>";
										$no++;
									} ?>
								</td>
							</tr><?php
		    			} ?>
		    		</table>
	    		<button class="btn btn-default" onclick="history.back(-1)" style="margin:0 0 30px 10px" >Kembali</button>
	    		</div>
	    	</div>
		</div>