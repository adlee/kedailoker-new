<div style="color:black">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-family: arial;text-align: left;padding: 10px 17px 0 25px;">
		<h3>Frequently Asked Questions (FAQ) Pencaker</h3>
		<h4>Memiliki masalah untuk login?</h4>
<p>
Jangan khawatir, ini terjadi pada semua orang. Jika Anda lupa password Anda, klik &#39; Lupa? &#39; di

atas kotak sign in, kemudian masukkan alamat email yang Anda gunakan untuk akun

kedailoker.com Anda. Klik &#39; Kirim &#39; dan kami akan mengirimkan password Anda ke email Anda.

Pastikan untuk memeriksa spam mail / junk Anda jika Anda tidak dapat menemukan email di

kotak masuk Anda.</p>

<h4>Bagaimana caranya saya untuk mengubah password?</h4>

<p>Pertama, masuk ke akun kedailoker.com Anda. Pergi ke ikon profil Anda di bagian kiri atas

halaman. Klik &quot;Profil&quot;. Isi kolom dan simpang data Anda.</p>

<h4>Bagaimana caranya untuk mendaftar di kedailoker.com?</h4>

<p>Pergi ke halaman utama Kedailoker.com dan mengisi kolom yang diperlukan dalam kotak daftar.

Verifikasi nomor HP Anda sebagai nama pengguna. Setelah Anda telah mengirimkan verifikasi,

kami akan mengirimi SMS balasan dalam waktu 1x24 jam bahwa akun Anda telah aktif.

Pastikan untuk menggunakan no HP Anda yang aktif, karena itu akan berguna sebagai nama

akun pengguna dan sebagai saran informasi info lowongan kerja dan informasi lain yang dikirim

oleh kedailoker.com.</p>

<p>Setelah Anda telah mengaktifkan akun Anda , ketika Anda pertama kali masuk , silahkan

mengisi data diri dan riwayat diri Anda. Menulis data diri dan riwayat diri Anda dengan lengkap

sangat penting untuk meningkatkan kesempatan Anda untuk berkarir. Pastikan lengkap dan

selalu diperbarui.</p>

<h4>Siapakah yang melihat resume saya?</h4>

<p>Ketika Anda membuat resume Anda, semua Perusahaan akan dapat melihat resume Anda secara

default. Namun, hanya perusahaan yang berprospektif sajalah yang bisa melihat kontak

informasi Anda.</p>

<h4>Bagaimana caranya untuk mendaftar di kedailoker.com?</h4>

<p>Untuk melamar peluang kerja melalui kedailoker.com, Anda akan membutuhkan dua hal:</p>

<ol>
	<li>Akun kedailoker.com</li>
	<li>Data diri dan riwayat diri lengkap yang disimpan ke akun kedailoker.com Anda</li>
</ol>

<p>Lowongan pekerjaan yang terbuka akan langsung diberitahukan ke dashboard Anda apabila

sesuai dengan kualifikasi Anda. Klik tombol ‘Lamar’ untuk melamar. Setelah Anda

menyelesaikan langkah-langkah, data diri dan riwayat diri Anda akan dikirim ke Perusahaan.</p>

<h4>Mengapa saya tidak mendapat respon setelah melamar secara online?</h4>

<p>Setiap Perusahaan memiliki metode sendiri untuk mengevaluasi data diri dan riwayat diri.

Beberapa Perusahaan dapat mengirimkan balasan melalui kotak masuk Respon Perusahaan atau

menghubungi Anda untuk merespon lamaran Anda. Namun, ada Perusahaan yang tidak akan

menghubungi Anda kecuali mereka ingin memulai proses wawancara. Namun kami himbau

Anda untuk sering cek kotak masuk Respon Perusahaan untuk melihat balasan dari perusahaan

yang Anda lamar.</p>

<h4>Bagaimana caranya agar peluang saya untuk direkrut menjadi lebih besar?</h4>

<p>Membuat resume yang benar-benar menonjol. Anda ingin menyoroti pengalaman spesifik dan

peran Anda sehingga perusahaan tahu Anda akan cocok dengan kebutuhan mereka.

Menambahkan lebih banyak pengalaman, pendidikan, sertifikasi dan keterampilan akan sangat

membantu.</p>
	</div>
	
</div>
</body>
</html>