<div style="color:black">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-family: arial;text-align: left;padding: 10px 17px 0 25px;">
		<p>Kedailoker.com tidak hanya berusaha untuk memberikan pelayanan berkualitas yang sesuai

		harapan Pencari Kerja dan Perusahaan, tapi kami juga memberikan pengalaman yang aman dan

		terjamin.</p>

		<p>Informasi yang dikirim tetap bersifat pribadi dan hanya digunakan oleh kedailoker.com sebagai

		bahan evaluasi dan digunakan oleh Pencari Kerja untuk melamar secara online.</p>
		<h4>Kebijakan Privasi</h4>

		<ol>
			<li>Informasi yang dimasukkan untuk iklan lowongan akan dicek terlebih dahulu oleh

kedailoker.com. Bila ada hal yang kurang sesuai dengan Syarat dan Ketentuan, maka

kedailoker.com akan mengonfirmasi ke Pelanggan untuk mengedit lagi.</li>
			<li>Data diri dan riwayat diri akan ditampilkan sebagaimana adanya, dan tidak disunting

oleh kedailoker.com. Harap kaji informasi yang dibutuhkan dan pastikan informasi mana

yang dapat dilihat oleh Pelanggan kedailoker.com.</li>
			<li>Bila Pencari Kerja melamar secara online, nama depan dan nomor telpon Anda akan

ditampilkan ke Perusahaan yang Anda lamar.</li>
			<li>Pada saat Curriculum Vitae Anda pertama kali tampil sebagai hasil pencarian, nama

depan dan nomor telpon tidak akan ditampilkan. Tetapi bila Perusahaan melakukan

pencarian dan mengirim email dari resume link, maka nama depan dan nomor telpon

akan ditampilkan seketika.</li>
			<li>Kami sarankan untuk merahasiakan kata sandi akun Anda. Kami tidak akan pernah

menanyakan kata sandi akun Anda melalui telpon ataupun email dan tidak akan

memberikan kata sandi akun Anda ke Perusahaan.</li>
			<li>Akun Anda di kedailoker.com dilindungi dengan password. Artinya, hanya Anda yang

mempunyai akses ke akun Anda dan hanya Anda yang bisa mengubah segala informasi

yang dimasukkan melalui akun Anda.</li>
			<li>Setelah Anda selesai menggunakan kedailoker.com, jangan lupa untuk keluar. Hal ini

untuk memastikan bahwa tidak ada pihak lain yang mengakses akun Anda, khususnya

bila komputer digunakan bersama-sama atau bila Anda menggunakan komputer di

tempat umum seperti perpustakaan atau kafe/warung internet.</li>
			<li>Cookies adalah serangkaian informasi yang dipindahkan dari situs ke hard disk komputer

Anda untuk penyimpanan data. Cookies memberikan keuntungan bagi situs dalam

beberapa hal dengan menyimpan informasi mengenai preferensi-preferensi Anda ketika

mengunjungi sebuah situs. Banyak situs terkemuka yang menggunakan cookies untuk

memberikan keistimewaan-keistimewaan yang menguntungkan bagi pengguna situs

mereka. Cookies dapat mengenali komputer Anda, namun tidak dapat mengenali

identitas Anda. Kebanyakan browsers dapat menerima cookies, dengan catatan browser

Anda telah diset terlebih dahulu. Apabila browser Anda tidak dapat menerima cookies,

maka Anda sama sekali tidak akan dapat mengakses ke situs kami.</li>
			<li>Apabila suatu saat nanti kami harus mengubah Polis Kerahasiaan kami, maka kami akan

mencantumkannya di sini agar para Pencari Kerja dapat mengetahui informasi apa saja

yang kami kumpulkan dan bagaimana kami menggunakan informasi tersebut. Data

pribadi Anda akan digunakan sesuai dengan polis kerahasiaan kami. Apabila, sewaktu-

waktu Anda ingin mengajukan pertanyaan ataupun memberikan komentar tentang Polis

Kerahasiaan kami, maka Anda dapat menghubungi kami lewat email cs@kedailoker.com

atau menghubungi telepon +62 21 7658 3450 dan langsung berbicara dengan salah satu

staf kami</li>
		</ol>


	</div>
</div>
</body>
</html>