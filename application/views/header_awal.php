<?php 
if ($this->session->userdata('no')==null) {
?>
<!DOCTYPE html>
<html>
<head>
	<title>KEDAI LOKER</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/bootstrap.min.css">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/sweetalert.css">

	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/script.js"></script>
	<!-- bxSlider file -->
	<script src="<?php echo base_url(); ?>dist/bxslider/jquery.bxslider.min.js"></script>
	<link href="<?php echo base_url(); ?>dist/bxslider/jquery.bxslider.css" rel="stylesheet" />
</head>
	
<body>
	<div id="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#002060;">
		<div class="col-lg-5 col-md-4 col-sm-3 col-xs-12" style="background:none;margin:10px 0;font-size:25pt;font-weight:bold;">
		<a href="<?=site_url()?>"><img src="<?= base_url('dist/img/KL3.png')?>" style="width: 200px;"></a>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12" style="background:#002060;margin-bottom:-20px;margin-top: 15px;">
			<a style="text-decoration:none" href="<?php echo base_url();?>perusahaan"><div class="tombol" style="padding:8px 5px;margin:10px 0 5px 0;">Masuk Sebagai Perusahaan</div></a>
		</div>
		<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12" style="background:#002060;margin-top:15px">
			<form action="<?php echo base_url();?>klien/login" method="post">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin:0 0 2px 1px;background:none;padding:0 2px 0 2px">
					<label class="col-lg-12 col-md-12 col-sm-12 col-xs-0" style="margin:7px 0 0 0;padding:0;color:#fff;font-weight:normal;font-size:90%;">Nomor Seluler</label>
					<input  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" type="text" name="hp" required onkeydown="return numbers(event);"> 				
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin:0 0 -20px 1px;background:none;padding:0 2px 0 2px">
					<label class="col-lg-12 col-md-12 col-sm-12 col-xs-0" style="margin:7px 0 0 0;padding:0;color:#fff;font-weight:normal;font-size:90%;">Kata Sandi</label>
					<input  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" type="password" name="sandi" required> 				
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin:0 0 5px 1px;background:none;padding:0 2px 0 2px;margin-top:24px">
					<input  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tombol" style="padding:3.5px 0;" type="submit" name="masuk" value="Masuk" > 				
				</div>
			</form>
		</div>
    </div>
    <?php }else{
	redirect('klien/home');
	} ?>