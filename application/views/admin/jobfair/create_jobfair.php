<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Buat Jobfair
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Buat Jobfair</a></li>
          </ol>
        </section>

        <section class="content">
          <?php $jml=0;foreach ($per as $perusahaan) { $jml++; } ?>
          <form method="post" action="<?php echo base_url()?>adminkl1012/jobfair/create_exc/<?php echo $jml ?>" style="margin-left:-15px">
            <div class="box-body"  style="width:98%;margin-left:10px">
              <div class="form-group">
                  <label> Nama</label>
                  <input type="text" class="form-control" maxlength="50" name="nama" >
              </div>
              <div class="form-group">
                  <label> Lokasi</label>
                  <input type="text" class="form-control" maxlength="50" name="lokasi" placeholder="Alamat tempat penyelenggaraan">
              </div>
              <div class="form-group">
                  <label> Tanggal</label>
                  <input type="date" class="form-control" name="tanggal">
              </div>
              <div class="form-group">
                  <label> Jam</label>
                  <table id="example" class="table" style="width:310px">
                    <tr style="background:#fff" align="center">
                      <td style="background:#E8E6E6;color:000;width:40%">Mulai</td>
                      <td> 
                        <div style="margin:-8px -8px -8px 5px">
                          <input type="text" name="jam_m" maxlength="2"  value="00"  style="float:left;padding-left:3px;margin-top:8px;border:none;background:none;width:20px" required onkeydown="return numbers(event);">
                          <input type="text"  value=":"  style="float:left;padding-left:0px;margin-top:8px;border:none;background:none;width:5px" readonly>
                          <input type="text" name="menit_m" maxlength="2"  value="00"  style="float:left;padding-left:3px;margin-top:8px;border:none;background:none;width:20px" required onkeydown="return numbers(event);">
                        </div>
                      </td>
                    </tr>
                    <tr style="background:#fff" align="center">
                      <td style="background:#E8E6E6;color:000;width:40%">Selesai</td>
                      <td> 
                        <div style="margin:-8px -8px -8px 5px">
                          <input type="text" name="jam_s" maxlength="2"  value="00"  style="float:left;padding-left:3px;margin-top:8px;border:none;background:none;width:20px" required onkeydown="return numbers(event);">
                          <input type="text"  value=":"  style="float:left;padding-left:0px;margin-top:8px;border:none;background:none;width:5px" readonly>
                          <input type="text" name="menit_s" maxlength="2"  value="00"  style="float:left;padding-left:3px;margin-top:8px;border:none;background:none;width:20px" required onkeydown="return numbers(event);">
                        </div>
                      </td>
                    </tr>
                </table>
              </div>
              <div class="form-group">
                  <label> Perusahaan</label><br/>
                  <table class="table table-condensed">
                    <tr>
                    <?php $jml=0; $no=1; foreach ($per as $perusahaan) {?>
                      <td>
                        <input type="checkbox" value="<?php echo $perusahaan['id'] ?>" name="cek<?php echo $no; ?>"> <span style="padding-right:50px;"><?php echo $perusahaan['nama_per'] ?></span>
                      </td>
                      <?php
                    
                      if($no%3 == 0) {
                        echo "</tr>";
                      }

                      $no++; $jml++;
                    } ?>
                  </table>
                  
              </div>
              <div class="box-footer" style="margin-left:-10px;background:none">
                 <input type="submit" name="simpan" id="simpan" class="btn btn-primary" value="Simpan">
                 <input type="button" class="btn btn-default"  value="Back" onclick="history.back(-1)" >
              </div>
            </div>
          </form>
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->