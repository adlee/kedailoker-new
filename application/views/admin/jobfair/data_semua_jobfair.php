<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Data Semua Jobfair
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Data Semua Jobfair</a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body" style="overflow:scroll">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama Jobfair</th>
                        <th>Lokasi</th>
                        <th>Tanggal</th>
                        <th>Jam</th>
                        <th>Perusahaan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody><?php

                    foreach ($jf as $jobfair) { 
                      $id_jf=$jobfair['id'];
                      $id_per=$jobfair['id'];?>
                      <tr>
                        <td><?php echo $jobfair['nama'];?></td>
                        <td><?php echo $jobfair['lokasi'];?></td>
                        <td><?php echo $jobfair['tgl'];?></td>
                        <td><?php echo $jobfair['jam'];?></td>
                        <td><?php $cek= $jobfair['perusahaan'];
                          $no_per=explode(" ", $jobfair['id_perusahaan']);
                          $cek = $jobfair['perusahaan'];
                          for($i=1; $i<=$cek ; $i++) { 
                            $x=$i-1;
                            $pr[$i]=$no_per[$x];
                            foreach ($per as $perusahaan) {
                              $per_x=$perusahaan['no_telp'];
                              if ($per_x==$pr[$i]) {
                                echo $perusahaan['nama_per'].", ";
                              }
                            }
                        }

                        ?></td>
                        <td align="right">
                          <a href="<?php echo base_url();?>adminkl1012/jobfair/edit/<?php echo $id_jf;?>" class="btn btn-info" style="padding:0 8px">edit</a>&nbsp 
                          <button class="btn btn-danger" style="padding:0 8px" onclick='swal({title: "Hapus <?php echo $jobfair['nama'];?> ?",text: "Data <?php echo $jobfair['nama'];?> akan dihapus secara permanen", type: "warning",
                              showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Hapus", closeOnConfirm: false },
                              function(){ swal("Hapus", "<?php echo $jobfair['nama'];?> berhasil di hapus.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/jobfair/delete/<?php echo $id_jf;?>"; });'>hapus
                          </button>
                        </td>
                      </tr><?php
                    } ?>  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->