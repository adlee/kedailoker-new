<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Edit Jobfair
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Edit Jobfair</a></li>
          </ol>
        </section>

        <section class="content"><?php
          foreach ($jf as $jobfair) {$jml = $jobfair['perusahaan']; }
          foreach ($jf as $dtjf) { 
            $id_job=$dtjf['id'];?>
          <form method="post" action="<?php echo base_url()?>adminkl1012/jobfair/edit_exc/<?php echo $id_job ?>/<?php echo $jml ?>" style="margin-left:-15px">
            <div class="box-body"  style="width:98%;margin-left:10px">
              <div class="form-group">
                  <label> Nama</label>
                  <input type="text" class="form-control" maxlength="50" name="nama" value="<?php echo $dtjf['nama'] ?>">
              </div>
              <div class="form-group">
                  <label> Lokasi</label>
                  <input type="text" class="form-control" maxlength="50" name="lokasi" value="<?php echo $dtjf['lokasi'] ?>">
              </div>
              <div class="form-group">
                  <label> Tanggal</label>
                  <input type="date" class="form-control" name="tanggal" value="<?php echo $dtjf['tanggal'] ?>">
              </div>
              <?php 
              $jam=$dtjf['jam'];
              $all=explode(" - ", $jam); 
                $jam1 = $all[0];
                $detjam1=explode(":", $jam1); 
                  $j_mulai = $detjam1[0];
                  $m_mulai = $detjam1[1];
                $jam2 = $all[1];
                $detjam2=explode(":", $jam2); 
                  $j_selesai = $detjam2[0];
                  $m_selesai = $detjam2[1];
              ?>
              <div class="form-group">
                  <label> Jam</label>
                  <table id="example" class="table" style="width:310px">
                    <tr style="background:#fff" align="center">
                      <td style="background:#E8E6E6;color:000;width:40%">Mulai</td>
                      <td> 
                        <div style="margin:-8px -8px -8px 5px">
                          <input type="text" name="jam_m" maxlength="2"  value="<?php echo $j_mulai ?>"  style="float:left;padding-left:3px;margin-top:8px;border:none;background:none;width:20px" required onkeydown="return numbers(event);">
                          <input type="text"  value=":"  style="float:left;padding-left:0px;margin-top:8px;border:none;background:none;width:5px" readonly>
                          <input type="text" name="menit_m" maxlength="2"  value="<?php echo $m_mulai ?>"  style="float:left;padding-left:3px;margin-top:8px;border:none;background:none;width:20px" required onkeydown="return numbers(event);">
                        </div>
                      </td>
                    </tr>
                    <tr style="background:#fff" align="center">
                      <td style="background:#E8E6E6;color:000;width:40%">Selesai</td>
                      <td> 
                        <div style="margin:-8px -8px -8px 5px">
                          <input type="text" name="jam_s" maxlength="2"  value="<?php echo $j_selesai ?>"  style="float:left;padding-left:3px;margin-top:8px;border:none;background:none;width:20px" required onkeydown="return numbers(event);">
                          <input type="text"  value=":"  style="float:left;padding-left:0px;margin-top:8px;border:none;background:none;width:5px" readonly>
                          <input type="text" name="menit_s" maxlength="2"  value="<?php echo $m_selesai?>"  style="float:left;padding-left:3px;margin-top:8px;border:none;background:none;width:20px" required onkeydown="return numbers(event);">
                        </div>
                      </td>
                    </tr>
                </table>
              </div>
              <div class="form-group">
                  <label> Perusahaan</label><br/>
                  <table class="table table-condensed">
                    <tr>
                    <?php $jml=0; $no=1; foreach ($per as $perusahaan) {
                        $checked = "";
                        if(count($jobfair_perusahaan > 0)) {
                          foreach($jobfair_perusahaan as $jf_p) {
                            if($jf_p->id_perusahaan == $perusahaan['id']) {
                              $checked = "checked";
                            }
                          }
                        }
                      ?>
                      <td>
                        <?php ?>
                        <input type="checkbox" <?= $checked?> value="<?php echo $perusahaan['id'] ?>" name="cek<?php echo $no; ?>"> <span style="padding-right:50px;"><?php echo $perusahaan['nama_per'] ?></span>
                      </td>
                      <?php
                    
                      if($no%3 == 0) {
                        echo "</tr>";
                      }

                      $no++; $jml++;
                    } ?>
                  </table>
              </div>
              <!-- <div class="form-group">
                  <label> Brosur</label>
                  <input type="text" class="form-control" maxlength="50" name="alamat">
              </div> -->
              <div class="box-footer" style="margin-left:-10px;background:none">
                 <input type="submit" name="simpan" id="simpan" class="btn btn-primary" value="Simpan">
                 <input type="button" class="btn btn-default"  value="Back" onclick="history.back(-1)" >
              </div>
            </div>
          </form>
          <?php } ?>
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->