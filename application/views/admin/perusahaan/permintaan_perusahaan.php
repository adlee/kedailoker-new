<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Permintaan Perpanjangan Masa Aktif
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Permintaan Perpanjangan Masa Aktif</a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Telepon</th>
                        <th>Lowongan</th>
                        <th>Masa Aktif</th>
                        <th>Biaya Perpanjangan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody><?php
                    foreach ($update as $perusahaan_items) { 
                      $id=$perusahaan_items['id_post'];
                      $id_perus=$perusahaan_items['id_perus'];
                      $byr=$perusahaan_items['update_masa_aktif'];
                      $sekarang=$perusahaan_items['aktif'];
                      $sts=$perusahaan_items['status'];
                      if ($byr>150000) {
                        $aktif_=90;
                      }else if ($byr>100000) {
                        $aktif_=60;
                      }else if ($byr>50000) {
                        $aktif_=30;
                      }?>
                      <tr>
                        <td><?php 
                          foreach ($per as $dataperusahaan) {
                            if ($dataperusahaan['id']==$id_perus) {
                              $nama_per=$dataperusahaan['nama_per'];
                              echo $nama_per;
                            }
                          }
                        ?></td>
                        <td><?php 
                          foreach ($per as $dataperusahaan) {
                            if ($dataperusahaan['id']==$id_perus) {
                              echo $dataperusahaan['no_telp'];
                            }
                          }
                        ?></td>
                        <td><?php echo $perusahaan_items['judul'];?></td>
                        <td><?php echo $sekarang;?> hari</td>
                        <td>Rp. <?php $updt=number_format($byr,0,",",".");
                            echo"$updt";?>
                        </td>
                        <td align="center">
                          <button class="tombolon" style="padding:1px 10px" onclick='swal({title: "Pperpanjang masa aktif ?",   
                            text: "Pastikan <?php //echo $nama_per?> telah membayar uang sejumlah Rp. <?php //$updt?> untuk menambah masa aktif selama:",   type: "input",   showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   
                            inputValue: "<?php echo $aktif_ ?>" }, function(inputValue){   if (inputValue === false) return false;      
                            if (inputValue === "") {     swal.showInputError("Perpanjangan harus diisi!");     return false   }      
                             window.location.href="<?php echo base_url();?>adminkl1012/perusahaan/permintaan/masa/aktif/<?php echo $id;?>/<?php echo $sekarang;?>/"+ inputValue; });'>proses
                          </button>
                          <button class="tombol" style="padding:1px 5px" onclick='swal({title: "Hapus permintaan?",text: "", type: "warning",
                                showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Delete", closeOnConfirm: false },
                                function(){ swal("Deleted", "Data berhasil dihapus.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/perusahaan/permintaan/masa/aktif/<?php echo $id;?>"; });'>hapus
                          </button>
                        </td>
                      </tr><?php
                    } ?>  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->