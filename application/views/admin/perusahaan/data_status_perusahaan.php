
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Manajemen Perusahaan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Manajemen Perusahaan</a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr style="text-align:center;font-weight:bold">
                        <td>Status Perusahaan</td>
                        <td>Masa aktif</td>
                        <td>Jumlah Perusahaan</td>
                        <td>Aksi</td>
                      </tr>
                    </thead>
                    <tbody><?php 



                    $jml_on=0;
                    $telepon="";
                    foreach ($perusahaan as $perusahaan_on) { 
                      if ($perusahaan_on['aktif']>3) {
                        if ($telepon==$perusahaan_on['no_telp']) {
                        }else{
                          $telepon=$perusahaan_on['no_telp'];
                          $jml_on++; 
                        }
                      }
                    } ?>
                      <tr>
                        <td align="center">
                          <button class="form-control" style="width:100%;border:none;color:#03DE03;background:transparent;font-weight:bold" data-toggle="collapse" data-target="#on">ON</button>
                          <div id="on" class="collapse" style="background:#ABFFAB;padding:2px 0 5px 5px;margin:0 0 0 5px">
                            <p>
                                <table class="table table-hover" style="background:#ABFFAB">
                                  <form method="post" action="<?php echo base_url();?>adminkl1012/perusahaan/aktif/on/csv/<?php echo $jml_on ?>" target="blank">
                                  <input type="hidden" name="perusahaan" value="0">
                                  <input type="hidden" name="hrd" value="0">
                                  <input type="hidden" name="telepon" value="0">
                                  <input type="hidden" name="lowongan" value="0">
                                  <tr style="color:black;text-align:left;font-weight:bold;background:#ABFFAB">
                                    <td class="column-check-on" style="text-align:center;border-right:4px solid #eee;border-top:none;padding:5px 10px;border-bottom:4px solid #eee"><input class="check-all-on" type="checkbox" checked></td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">Perusahaan
                                       &nbsp <input type="checkbox" class="column-check-on" value="cek" name="perusahaan" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">HRD
                                       &nbsp <input type="checkbox" class="column-check-on" value="cek" name="hrd" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">Telepon
                                       &nbsp <input type="checkbox" class="column-check-on" value="cek" name="telepon" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">Lowongan
                                       &nbsp <input type="checkbox" class="column-check-on" value="cek" name="lowongan" checked>
                                    </td>
                                  </tr>
                                  <?php 
                                  $no=1;
                                  $telepon="";
                                  foreach ($perusahaan as $perusahaan_on) { 
                                    if ($perusahaan_on['aktif']>3) {
                                      if ($telepon==$perusahaan_on['no_telp']) {
                                      }else{
                                        $telepon=$perusahaan_on['no_telp']; ?>
                                        <tr>
                                          <td align="center" style="border-right:4px solid #eee;border-top:none;padding:5px 10px">
                                            <input type="checkbox" class="column-check-on" value="<?php echo $perusahaan_on['id_perus'] ?>" name="cek<?php echo $no?>" checked>
                                          </td>
                                          <td style="border-top:none;padding:5px 10px"><?php echo $perusahaan_on['nama_per'] ?></td>  
                                          <td style="border-top:none;padding:5px 10px"><?php echo $perusahaan_on['nama_hrd'] ?></td>  
                                          <td style="border-top:none;padding:5px 10px"><?php echo $perusahaan_on['no_telp'] ?></td>  
                                          <td style="border-top:none;padding:5px 10px">
                                            <?php foreach ($perusahaan as $perusahaan_on_all) {
                                              if ($perusahaan_on_all['aktif']>3) {
                                                if ($perusahaan_on['id_perus']==$perusahaan_on_all['id_perus']) {
                                                  echo $perusahaan_on_all['judul']."<br/>";
                                                }
                                              }
                                                
                                            } ?>
                                          </td>  
                                        </tr><?php 
                                      $no++;
                                      }
                                    }
                                  } ?>
                                </table>
                            </p>
                          </div>
                        </td>
                        <td align="center">lebih dari 3</td>
                        <td align="center"><?php echo $jml_on ?></td>
                        <td align="center">
                          <button type="submit" class="form_on btn btn-default" style="padding:1px 10px">export to CSV</button>
                          </form>
                        </td>
                      </tr>
                      <?php 












                    $jml_warning=0;
                    $telepon="";
                    foreach ($perusahaan as $perusahaan_warning) { 
                      if (($perusahaan_warning['aktif']>0)and($perusahaan_warning['aktif']<4)) {
                        if ($telepon==$perusahaan_warning['no_telp']) {
                        }else{
                          $telepon=$perusahaan_warning['no_telp'];
                          $jml_warning++; 
                        }
                      }
                    } ?>
                      <tr>
                        <td align="center">
                          <button class="form-control" style="width:100%;border:none;color:#E0E000;font-weight:bold" data-toggle="collapse" data-target="#warning">WARNING</button>
                          <div id="warning" class="collapse" style="background:#E6E680;padding:2px 0 5px 5px;margin:0 0 0 5px">
                            <p>
                                <table class="table table-hover" style="background:#E6E680">
                                  <form method="POST" action="<?php echo base_url();?>adminkl1012/perusahaan/aktif/warning/csv/<?php echo $jml_warning ?>" target="blank">
                                  <input type="hidden" name="perusahaan" value="0">
                                  <input type="hidden" name="hrd" value="0">
                                  <input type="hidden" name="telepon" value="0">
                                  <input type="hidden" name="lowongan" value="0">
                                  <tr style="color:black;text-align:left;font-weight:bold;background:#E6E680">
                                    <td class="column-check-warning" style="border-right:4px solid #eee;border-bottom:4px solid #eee;border-top:none;text-align:center;padding:5px 10px"><input class="check-all-warning" type="checkbox" checked></td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">Perusahaan
                                       &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="perusahaan" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">HRD
                                       &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="hrd" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">Telepon
                                       &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="telepon" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">Lowongan
                                       &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="lowongan" checked>
                                    </td>
                                  </tr>
                                  <?php 
                                  $no=1;
                                  $telepon="";
                                  foreach ($perusahaan as $perusahaan_warning) { 
                                    if (($perusahaan_warning['aktif']>0)and($perusahaan_warning['aktif']<4)) {
                                      if ($telepon==$perusahaan_warning['no_telp']) {
                                      }else{
                                        $telepon=$perusahaan_warning['no_telp']; ?>
                                        <tr>
                                          <td align="center" style="border-right:4px solid #eee;border-top:none;padding:5px 10px">
                                            <input type="checkbox" class="column-check-warning" value="<?php echo $perusahaan_warning['id_perus'] ?>" name="cek<?php echo $no?>" checked>
                                          </td>
                                          <td style="border-top:none;padding:5px 10px;"><?php echo $perusahaan_warning['nama_per'] ?></td>  
                                          <td style="border-top:none;padding:5px 10px;"><?php echo $perusahaan_warning['nama_hrd'] ?></td>  
                                          <td style="border-top:none;padding:5px 10px;"><?php echo $perusahaan_warning['no_telp'] ?></td>  
                                          <td style="border-top:none;padding:5px 10px;">
                                            <?php foreach ($perusahaan as $perusahaan_warning_all) {
                                              if (($perusahaan_warning_all['aktif']>0)and($perusahaan_warning_all['aktif']<4)) {
                                                if ($perusahaan_warning['id_perus']==$perusahaan_warning_all['id_perus']) {
                                                  echo $perusahaan_warning_all['judul']."<br/>";
                                                }
                                              }
                                            } ?>
                                          </td>  
                                        </tr><?php 
                                      $no++;
                                      }
                                    }
                                  } ?>
                                </table>
                            </p>
                          </div>
                        </td>
                        <td align="center">1 sampai 3</td>
                        <td align="center"><?php echo $jml_warning ?></td>
                        <td align="center">
                          <button type="submit" class="form_warning btn btn-default" style="padding:1px 10px">export to CSV</button>
                          </form>
                        </td>
                      </tr><?php 
















                    $jml_off=0;
                    $telepon="";
                    foreach ($perusahaan as $perusahaan_off) { 
                      if ($perusahaan_off['aktif']==0) {
                        if ($telepon==$perusahaan_off['no_telp']) {
                        }else{
                          $telepon=$perusahaan_off['no_telp'];
                          $jml_off++; 
                        }
                      }
                    } ?>
                      <tr>
                        <td align="center">
                          <button class="form-control" style="width:100%;border:none;background:transparent;color:red;font-weight:bold" data-toggle="collapse" data-target="#off">OFF</button>
                          <div id="off" class="collapse" style="background:#FF8C8C;padding:2px 0 5px 5px;margin:0 0 0 5px">
                            <p>
                                <table class="table table-hover" style="background:#FF8C8C">
                                  <form method="post" action="<?php echo base_url();?>adminkl1012/perusahaan/aktif/off/csv/<?php echo $jml_off ?>" target="blank">
                                  <input type="hidden" name="perusahaan" value="0">
                                  <input type="hidden" name="hrd" value="0">
                                  <input type="hidden" name="telepon" value="0">
                                  <input type="hidden" name="lowongan" value="0">
                                  <tr style="color:black;text-align:left;font-weight:bold;background:#FF8C8C;border-bottom:2px solid #eee">
                                    <td class="column-check-off" style="border-right:4px solid #eee;text-align:center;border-top:none;border-bottom:4px solid #eee;padding:5px 10px"><input class="check-all-off" type="checkbox" checked></td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">Perusahaan
                                       &nbsp <input type="checkbox" class="column-check-off" value="cek" name="perusahaan" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">HRD
                                       &nbsp <input type="checkbox" class="column-check-off" value="cek" name="hrd" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">Telepon
                                       &nbsp <input type="checkbox" class="column-check-off" value="cek" name="telepon" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-bottom:4px solid #eee;border-top:none;">Lowongan
                                       &nbsp <input type="checkbox" class="column-check-off" value="cek" name="lowongan" checked>
                                    </td>
                                  </tr>
                                  <?php 
                                  $no=1;
                                  $telepon="";
                                  foreach ($perusahaan as $perusahaan_off) { 
                                    if ($perusahaan_off['aktif']==0) {
                                      if ($telepon==$perusahaan_off['no_telp']) {
                                      }else{
                                        $telepon=$perusahaan_off['no_telp']; ?>
                                        <tr>
                                          <td align="center" style="border-right:4px solid #eee;border-top:none;padding:5px 10px">
                                            <input type="checkbox" class="column-check-off" value="<?php echo $perusahaan_off['id_perus'] ?>" name="cek<?php echo $no?>" checked>
                                          </td>
                                          <td style="border-top:none;padding:5px 10px"><?php echo $perusahaan_off['nama_per'] ?></td>  
                                          <td style="border-top:none;padding:5px 10px"><?php echo $perusahaan_off['nama_hrd'] ?></td>  
                                          <td style="border-top:none;padding:5px 10px"><?php echo $perusahaan_off['no_telp'] ?></td>  
                                          <td style="border-top:none;padding:5px 10px">
                                            <?php foreach ($perusahaan as $perusahaan_off_all) {
                                              if ($perusahaan_off_all['aktif']==0) {
                                                if ($perusahaan_off['id_perus']==$perusahaan_off_all['id_perus']) {
                                                  echo $perusahaan_off_all['judul']."<br/>";
                                                }
                                              }
                                            } ?>
                                          </td>  
                                        </tr><?php 
                                      $no++;
                                      }
                                    }
                                  } ?>
                                </table>
                            </p>
                          </div>
                        </td>
                        <td align="center">0</td>
                        <td align="center"><?php echo $jml_off ?></td>
                        <td align="center">
                          <button type="submit" class="form_off btn btn-default" style="padding:1px 10px">export to CSV</button>
                          </form>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
      <script>
      $(document).ready(function() {
        $('.check-all-on').on('click', function(){
          if($(this).prop('checked')){
            $('.column-check-on').prop('checked', true);
          }
          else {
            $('.column-check-on').prop('checked', false);
          }
        });
        // $('.form_on').on('click', function(){
        //     alert('klik csv status on');
        // })




        $('.check-all-warning').on('click', function(){
          if($(this).prop('checked')){
            $('.column-check-warning').prop('checked', true);
          }
          else {
            $('.column-check-warning').prop('checked', false);
          }
        });
        $('.check-all-off').on('click', function(){
          if($(this).prop('checked')){
            $('.column-check-off').prop('checked', true);
          }
          else {
            $('.column-check-off').prop('checked', false);
          }
        });
      })
        
      </script>