<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <?php foreach ($perusahaan as $eachper) {?>
          <h1>Data <?php echo $eachper['nama_per'];?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Data <?php echo $eachper['nama_per'];?></a></li>
          </ol><?php 
          } ?>
        </section>

        <section class="content">
          <div class="row" style="margin:-15px 0 0 -20px"><?php foreach ($loker as $dtloker) { 
              $g1=$dtloker['gaji_min'];
              $g2=$dtloker['gaji_max'];?>  
            <h3 style="padding-left:20px;color:#7C0000">Lowongan <?php echo $dtloker['judul']?></h3>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive">
                <table class="table table-hover">
                    <tr><td style="width:250px">Tanggal post</td><td>:</td><td>&nbsp <?php echo $dtloker['tgl'] ?></td></tr>
                    <tr><td>Minimal Pendidikan</td><td>:</td><td>&nbsp <?php echo $dtloker['pendidikan'] ?></td></tr>
                    <tr><td>Syarat ketentuan</td><td>:</td><td>&nbsp <?php echo $dtloker['isi'] ?></td></tr>
                    <tr><td>Gaji</td><td>:</td><td>&nbsp <?php echo "Rp. $g1 - Rp. $g2";?></td></tr>
                </table>
              </div>
            </div><?php
          } ?>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <h3 style="padding-left:10px;color:#7C0000">Data Pelamar</h3>
                <div class="box-body" style="overflow:scroll">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Telepon</th>
                        <th>Email</th>
                        <th>Foto</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody><?php $no=1;
                    foreach ($pelamar as $dtpelamar) {
                      $id_pelamar=$dtpelamar['idpel'];
                      $sts=$dtpelamar['status'];?>
                      <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $dtpelamar['tgl'];?></td>
                        <td><?php echo $dtpelamar['username'];?></td>
                        <td><?php if ($dtpelamar['jk']=='L') { echo "Laki-Laki";
                                  }else{ echo "Perempuan";} ?>
                        </td>
                        <td><?php echo $dtpelamar['telp'];?></td>
                        <td><?php echo $dtpelamar['email'];?></td>
                        <td><?php $img=$dtpelamar['foto'];
                                  $data=explode(".", $dtpelamar['foto']);
                                  $foto=$data[0];
                                  $type=$data[1];
                                  if ($type=="jpeg") { ?>
                                    <img src="<?php echo base_url(); ?>dist/img/<?php echo $foto?>.jpg" title="foto" width=150px id="pp"></a><?php
                                  }else{ ?>
                                    <img src="<?php echo base_url(); ?>dist/img/<?php echo $img?>" title="foto" width=150px id="pp"></a><?php
                                  } ?>
                        </td>
                        <td><?php if ($sts=="menunggu") { ?>
                                    <span style="color:orange;font-weight:bold;padding:5px 10px;border-radius:5px"><?php echo $sts ?></span> <?php
                                  }else{?>
                                    <span style="color:blue;font-weight:bold;padding:5px 10px;border-radius:5px"><?php echo $sts ?></span> <?php
                                  } ?>
                        </td>
                      </tr><?php
                    $no++;
                    } ?>  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
          <button class="btn btn-default" onclick="history.back(-1)">kembali</button>

        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->