<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Data Semua Perusahaan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Data Semua Perusahaan</a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body" style="overflow:scroll">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Telepon</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody><?php 
                    foreach ($perusahaan as $perusahaan_items) { 
                      $id_per=$perusahaan_items['id'];?>
                      <tr>
                        <td><?php echo $perusahaan_items['nama_per'];?></td>
                        <td><?php echo $perusahaan_items['alamat'];?></td>
                        <td><?php echo $perusahaan_items['no_telp'];?></td>
                        <td><?php echo $perusahaan_items['email_hrd'];?></td>
                        <td><?php echo $perusahaan_items['password'];?></td>
                        <td><?php echo $perusahaan_items['status'];?></td>
                        <td align="right"><?php 
                          if ($perusahaan_items['status']=="off") {?>
                            <button class="btn btn-success" style="padding:0 8px" onclick='swal({title: "Aktifkan <?php echo $perusahaan_items['nama_per'];?> ?",text: "Anda akan mengaktifkan akun <?php echo $perusahaan_items['nama_per'];?>, sehingga perusahaan tersebut dapat mulai menggunakan fitur yang ada di kedailoker", type: "warning",
                                showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Aktif", closeOnConfirm: false },
                                function(){ swal("Aktif", "<?php echo $perusahaan_items['nama_per'];?> berhasil di aktifkan.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/perusahaan/on/<?php echo $id_per;?>"; });'>on
                            </button><?php
                           }else{ ?>
                            <button class="btn btn-warning" style="padding:0 8px" onclick='swal({title: "Nonaktifkan <?php echo $perusahaan_items['nama_per'];?> ?",text: "Anda akan menonaktifkan akun <?php echo $perusahaan_items['nama_per'];?>, sehingga perusahaan tersebut tidak dapat menggunakan fitur yang ada di kedailoker", type: "warning",
                                showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Nonaktif", closeOnConfirm: false },
                                function(){ swal("Nonaktif", "<?php echo $perusahaan_items['nama_per'];?> berhasil di nonaktifkan.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/perusahaan/off/<?php echo $id_per;?>"; });'>off
                            </button><?php
                           } ?>
                           <a href="<?php echo base_url();?>adminkl1011/perusahaan/<?php echo $id_per;?>" class="btn btn-info" style="padding:0 8px">detail</a>
                           <button class="btn btn-danger" style="padding:0 8px" onclick='swal({title: "Hapus <?php echo $perusahaan_items['nama_per'];?> ?",text: "Data <?php echo $perusahaan_items['nama_per'];?> akan dihapus secara permanen", type: "warning",
                              showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Hapus", closeOnConfirm: false },
                              function(){ swal("Hapus", "<?php echo $perusahaan_items['nama_per'];?> berhasil di hapus.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/perusahaan/delete/<?php echo $id_per;?>"; });'>hapus
                            </button>
        
                        </td>
                      </tr><?php
                    } ?>  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->