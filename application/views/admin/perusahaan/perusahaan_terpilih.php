
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <?php foreach ($perusahaan as $eachper) {?>
          <h1>Data <?php echo $eachper['nama_per'];?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Data <?php echo $eachper['nama_per'];?></a></li>
          </ol><?php 
          } ?>
        </section>

        <section class="content">
          <div class="row" style="margin:-15px 0 0 -20px">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive">
                <table class="table table-hover"><?php 
                  foreach ($perusahaan as $data_perusahaan) { 
                    $id_perus=$data_perusahaan['id'];?>
                    <tr><td>Region</td><td>:&nbsp <?php echo $data_perusahaan['region']?> negeri</td></tr>
                    <tr><td>Alamat</td><td id="alamat_">:&nbsp <?php echo $data_perusahaan['alamat'] ?></td></tr>
                    <tr><td>Email</td><td id="email_">:&nbsp <?php echo $data_perusahaan['email_hrd'] ?></td></tr>
                    <tr><td>Telepon</td><td id="no_telp">:&nbsp <?php echo $data_perusahaan['no_telp'] ?></td></tr>
                    <tr><td>Nama HRD</td><td id="hrd_">:&nbsp <?php echo $data_perusahaan['nama_hrd'] ?></td></tr>
                    <tr><td>No. Telp HRD</td><td id="telp_hrd_">:&nbsp <?php echo $data_perusahaan['no_hrd'] ?></td></tr>
                    <tr><td>Password</td><td>:&nbsp <?php echo $data_perusahaan['password'] ?></td></tr>
                    <tr><td>Logo</td><td>&nbsp&nbsp 
                    <?php 
                      foreach ($perusahaan as $data_perusahaan) {
                        $img=$data_perusahaan['foto'];
                        $data=explode(".", $data_perusahaan['foto']);
                              $foto=$data[0];
                              $type=$data[1];
                              if ($type=="jpeg") {?>
                                <img src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $foto?>.jpg" title="foto" width=150px id="pp"></a>
                              <?php
                              }else{?>
                                <img src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $img?>" title="foto" width=150px id="pp"></a>
                              <?php
                              }
                      } ?>
                    </td>
                    </tr><?php
                  }?>
                </table>
              </div>
            </div>
            <center>
              <a href="#" id="show_modal" class="btn btn-big btn-warning">Edit Data Perusahaan</a><br>
            </center>
            <p></p>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <h3 style="padding-left:10px;color:#7C0000">Lowongan Pekerjaan</h3>
                <div class="box-body" style="overflow:scroll">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <!-- <th>cek</th> -->
                        <th>Tanggal Post</th>
                        <th>Jenis Lowongan</th>
                        <th>Pelamar</th>
                        <th>Status</th>
                        <th>Tanggungan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody><?php 
                    foreach ($loker as $dtloker) { 
                      $id_lowongan=$dtloker['id_post'];
                      $minpendidikan=$dtloker['pendidikan'];
                      $jml=0;
                      foreach ($pelamar as $datapelamar) {
                        $id_lokerpelamar=$datapelamar['id_low'];
                        if ($id_lowongan==$id_lokerpelamar) {
                          $jml++;
                        }
                      }
                      $jml_all_pend=0;
                      $jml_pend=0;
                      if ($minpendidikan=="SD") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="SD")or($dtpendidikan['pend_terakhir']=="SMP")or($dtpendidikan['pend_terakhir']=="SMA")or($dtpendidikan['pend_terakhir']=="D1")or($dtpendidikan['pend_terakhir']=="D2")or($dtpendidikan['pend_terakhir']=="D3")or($dtpendidikan['pend_terakhir']=="D4")or($dtpendidikan['pend_terakhir']=="S1")or($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      }else if ($minpendidikan=="SMP") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="SMP")or($dtpendidikan['pend_terakhir']=="SMK")or($dtpendidikan['pend_terakhir']=="SMA")or($dtpendidikan['pend_terakhir']=="D1")or($dtpendidikan['pend_terakhir']=="D2")or($dtpendidikan['pend_terakhir']=="D3")or($dtpendidikan['pend_terakhir']=="D4")or($dtpendidikan['pend_terakhir']=="S1")or($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      }else if ($minpendidikan=="SMK") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="SMK")or($dtpendidikan['pend_terakhir']=="D1")or($dtpendidikan['pend_terakhir']=="D2")or($dtpendidikan['pend_terakhir']=="D3")or($dtpendidikan['pend_terakhir']=="D4")or($dtpendidikan['pend_terakhir']=="S1")or($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      }else if ($minpendidikan=="SMA") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="SMA")or($dtpendidikan['pend_terakhir']=="D1")or($dtpendidikan['pend_terakhir']=="D2")or($dtpendidikan['pend_terakhir']=="D3")or($dtpendidikan['pend_terakhir']=="D4")or($dtpendidikan['pend_terakhir']=="S1")or($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      }else if ($minpendidikan=="D1") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="D1")or($dtpendidikan['pend_terakhir']=="D2")or($dtpendidikan['pend_terakhir']=="D3")or($dtpendidikan['pend_terakhir']=="D4")or($dtpendidikan['pend_terakhir']=="S1")or($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      }else if ($minpendidikan=="D2") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="D2")or($dtpendidikan['pend_terakhir']=="D3")or($dtpendidikan['pend_terakhir']=="D4")or($dtpendidikan['pend_terakhir']=="S1")or($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      }else if ($minpendidikan=="D3") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="D3")or($dtpendidikan['pend_terakhir']=="D4")or($dtpendidikan['pend_terakhir']=="S1")or($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      }else if ($minpendidikan=="D4") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="D4")or($dtpendidikan['pend_terakhir']=="S1")or($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      }else if ($minpendidikan=="S1") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="S1")or($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      }else if ($minpendidikan=="S2") {
                          foreach ($pendidikan as $dtpendidikan) {
                           if (($dtpendidikan['pend_terakhir']=="S2")) {
                             $jml_pend++;
                           } $jml_all_pend++;
                          }  
                      } ?>
                      <tr>
                        <!-- <td><?php 
                            
                            echo $dtloker['tgl_aktif'];?> | <?php echo date('Y-m-d');?> | <?php echo $dtloker['aktif'];?> sisa=
                        </td> -->
                        <td><?php echo $dtloker['tgl'];?></td>
                        <td><?php echo $dtloker['judul'];?></td>
                        <td><?php echo $jml;?></td>
                        <td><?php echo $dtloker['status'];?></td>
                        <td><?php 
                            if ($dtloker['status']=='shared') {
                              echo "LUNAS";
                            }else{
                              $masa_aktif = $dtloker['masa_aktif'];
                              if ($masa_aktif>150000) {
                                $aktif = 3;
                              }else if ($masa_aktif>100000) {
                                $aktif = 2;
                              }else if ($masa_aktif>50000) {
                                $aktif = 1;
                              }else{
                                $aktif = "sample data invalid";
                              }
                              $bayar=number_format($masa_aktif,0,",",".");
                              echo "$aktif bulan = Rp $bayar";
                            }
                          ?>
                        </td>
                        <td align="left">
                           <a class="btn btn-info" style="padding:0 8px" href="<?php echo base_url();?>adminkl1012/perusahaan/<?php echo $id_perus;?>/<?php echo $id_lowongan;?>">detail</a>
                           <button class="btn btn-danger" style="padding:0 8px" onclick='swal({title: "Hapus <?php echo $dtloker['judul'];?> ?",text: "Data lowongan <?php echo $dtloker['judul'];?> akan dihapus secara permanen", type: "warning",
                              showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Hapus", closeOnConfirm: false },
                              function(){ swal("Hapus", "<?php echo $dtloker['judul'];?> berhasil di hapus.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/posting/delete/<?php echo $id_perus;?>/<?php echo $id_lowongan;?>"; });'>hapus
                           </button><?php 
                                                     if ($dtloker['status']=="pending") {?>
                            <button class="btn btn-success" style="padding:0 8px" onclick='swal({title: "Share <?php echo $dtloker['judul'];?> (aktif <?php echo $aktif." bulan" ?>) ?",
                              text: "Lowongan akan dishare kepada pengguna dengan syarat : memiliki kuota sms minimal 1 dan minimal pendidikan <?php echo $minpendidikan;?>, pastikan perusahaan telah membayar uang sejumlah Rp <?php echo $bayar ?> ", type: "warning",
                              showCancelButton: true, confirmButtonColor: "#48AB00", confirmButtonText: "Share", closeOnConfirm: false },
                              function(){ swal("Share", "<?php echo $dtloker['judul'];?> berhasil di bagikan.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/perusahaan/share/<?php echo $id_perus;?>/<?php echo $id_lowongan;?>/<?php echo $minpendidikan;?>/<?php echo $masa_aktif ?>"; });'>share
                            </button><?php
                          } ?>
                        </td>
                      </tr><?php
                    } ?>  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
<!-- begin modal -->
     <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <form action="<?= site_url('adminkl1012/update_data_perusahaan')?>" method="post" enctype='multipart/form-data'>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Data Perusahaan</h4>
                </div>

                <div class="modal-body">
                  <div class="form-group">
                    <label for="">Alamat</label>
                    <input type="hidden" name="id_p" value="<?= $this->uri->segment(3,0) ?>">
                    <input type="text" placeholder="alamat perusahaan" required name="alamat_p" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" placeholder="email perusahaan" required name="email_p" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">No Telpon</label>
                    <input type="text" placeholder="telp perusahaan" required name="telp_p" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Nama HRD</label>
                    <input type="text" placeholder="hrd perusahaan" required name="hrd_p" class="form-control">
                  </div> 
                  <div class="form-group">
                    <label for="">No. Telp HRD</label>
                    <input type="text" placeholder="No Telp HRD" required name="telp_hrd_p" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Ganti Logo (digunakan jika hanya mengganti logo)</label>
                    <input type="file" placeholder="ganti logo" name="image" accept="image/*" class="form-control">
                  </div>
                  <?php 
                      foreach ($perusahaan as $data_perusahaan) {
                        $img=$data_perusahaan['foto'];
                        $data=explode(".", $data_perusahaan['foto']);
                              $foto=$data[0];
                              $type=$data[1];
                              if ($type=="jpeg") {?>
                                <img src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $foto?>.jpg" title="foto" width=150px id="pp"></a>
                              <?php
                              }else{?>
                                <img src="<?php echo base_url(); ?>dist/img/perusahaan/<?php echo $img?>" title="foto" width=150px id="pp"></a>
                              <?php
                              }
                      } ?>
                </div>
                <div class="modal-footer">
                    <center>
                    <button type="submit" class="btn btn-primary" >Simpan Perubahan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" >Tutup</button>
                    </center> 
                </div>
            </div>
          </form>
        </div>
    </div>
<!-- end modal -->
<script>
  $(document).ready(function() {
    $('#show_modal').click(function(e) {
      e.preventDefault();
      $("#myModal").modal('show');
      var alamat, email, hrd, no_telp, telp_hrd;
      // get data from html dom
      alamat   = $("#alamat_").html().trim().replace(':&nbsp;', '');
      email    = $("#email_").html().trim().replace(':&nbsp;', '');
      hrd      = $("#hrd_").html().trim().replace(':&nbsp;', '');
      no_telp  = $("#no_telp").html().trim().replace(':&nbsp;', '');
      telp_hrd = $("#telp_hrd_").html().trim().replace(':&nbsp;', '');

      // update data to form modal
      $("input[name=alamat_p]").val(alamat);
      $("input[name=email_p]").val(email);
      $("input[name=hrd_p]").val(hrd);
      $("input[name=telp_p]").val(no_telp)
      $("input[name=telp_hrd_p]").val(telp_hrd);
    });
  });
</script>

<!-- /.content-wrapper