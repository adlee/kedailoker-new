<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            HOME
            <small>KEDAILOKER</small>
          </h1>
          <ol class="breadcrumb">
            <li><a><i class="fa fa-dashboard"></i> Home</a></li>
          </ol>
        </section>

        <section class="content" style="font-size:120%">
        Kedailoker
        <ul style="font-size:100%">
          <li style="text-align:justify">
          Membantu pencarian kerja mendapatkan informasi lowongan kerja langsung dari perusahaan yang membuka 
          lowongan kerja melalui SMS langsung ke seluler, dan pencari kerja dapat langsung mengirim lamaran ke 
          Perusahaan
          </li>
          <li style="text-align:justify">
            Membantu HRD perusahaan mendapatkan tenaga kerja sesuai klasifikasi yang dibutuhkan secara cepat dan 
            tepat
          </li>
        </ul>
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->