<!DOCTYPE html>
<html>
  <head>
    <title>ADM KEDAILOKER</title>
    <link rel="stylesheet" href="<?php echo base_url();?>dist/css/admin-bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>dist/css/admin-all-skins.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>dist/plugins/iCheck/square/blue.css">
  </head>
  <body class="hold-transition login-page" style="background:#E3CACA">
    <div class="login-box">
      <div class="login-logo">
        <a href="../../index2.html"><b>ADM </b> KEDAILOKER</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <form action="<?php echo base_url();?>adminkl1012/login" method="post">
          <div class="form-group has-feedback" style="margin-top:15px">
            <input type="text" class="form-control" name="username" placeholder="Username">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label style="margin-left:25px">
                  <input type="checkbox"> Remember Me
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat" style="background:#A83232">Login</button>
            </div><!-- /.col -->
          </div>
        </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url();?>dist/plugins/jQuery/jQuery_1.2.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url();?>dist/js/admin_bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>dist/plugins/iCheck/icheck.min.js"></script>
    <script>
      $(document).ready(function(){

        $("input[name=username]").focus();

        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
