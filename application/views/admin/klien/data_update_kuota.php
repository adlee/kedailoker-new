<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Permintaan Kuota SMS
            <small>(calon/pekerja)</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Permintaan Kuota SMS</a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Telepon</th>
                        <th>Kuota</th>
                        <th>Biaya update</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody><?php
                    foreach ($update as $klien_items) { 
                      $id=$klien_items['id'];
                      $byr=$klien_items['update_kuota'];
                      $sekarang=$klien_items['kuota_sms'];
                      $sts=$klien_items['status'];
                      if ($byr<30000) {
                        $sms=30;
                      }else if ($byr<=50000) {
                        $sms=60;
                      }else if ($byr<=100000) {
                        $sms=100;
                      }?>
                      <tr>
                        <td><?php echo $klien_items['username'];?></td>
                        <td><?php echo $klien_items['telp'];?></td>
                        <td><?php echo $sekarang;?></td>
                        <td>Rp. <?php $updt=number_format($byr,0,",",".");
                            echo"$updt";?>,-
                        </td>
                        <td align="center">
                            <button class="tombolon" style="padding:1px 10px" onclick='swal({title: "Aktifkan akun ?",   
                              text: "Pastikan <?php echo $klien_items['username'];?> telah membayar uang sejumlah Rp. <?php $harga=number_format($byr,0,",","."); echo"$harga,00";?> untuk mendapatkan kuota SMS sejumlah :",   type: "input",   showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   
                              inputValue: "<?php echo $sms ?>" }, function(inputValue){   if (inputValue === false) return false;      
                              if (inputValue === "") {     swal.showInputError("Kuota SMS harus diisi!");     return false   }      
                               window.location.href="<?php echo base_url();?>adminkl1012/klien/updatekuota/<?php echo $id;?>/<?php echo $sekarang;?>/"+ inputValue; });'>proses
                            </button>
                          <button class="tombol" style="padding:1px 5px" onclick='swal({title: "Hapus permintaan kuota SMS <?php echo $klien_items['username'];?> ?",text: "", type: "warning",
                                showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Delete", closeOnConfirm: false },
                                function(){ swal("Deleted", "Data berhasil dihapus.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/klien/updatekuota/<?php echo $id;?>"; });'>hapus
                          </button>
                        </td>
                      </tr><?php
                    } ?>  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->