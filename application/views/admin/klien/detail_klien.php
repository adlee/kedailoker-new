<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
    <h1>
      Detail Klien
      <small>(calon/pekerja)</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a>Semua Klien</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Tgl Gabung:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->tgl_gabung?></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Status:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->status?></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Status Cari Kerja:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->status_cari_kerja?></span>
                </div>
              </div> 
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Username:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->username?></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Jenis Kelamin:</label>
                <div class="col-sm-10">
                <span class="form-control"><?php echo ($klien[0]->jk == 'L') ? 'Laki-laki' : 'Perempuan' ; ?></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Alamat:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->alamat?></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->email?></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Telp:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->telp?></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">TTL:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->ttl?></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Pendidikan Terkahir:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->pend_terakhir?></span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Bayar:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->bayar?></span>
                </div>
              </div> 
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Kouta SMS:</label>
                <div class="col-sm-10">
                <span class="form-control"><?=$klien[0]->kuota_sms?></span>
                </div>
              </div>
               <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                  <a href="#" onclick="history.go(-1)" class="btn btn-info">Kembali</a>
                </div>
              </div>
            </form>

          </div><!-- /.box-body -->
        </div><!-- /.box --> 
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->