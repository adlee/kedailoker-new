<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>Data <?php echo $klien_detail->username;?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Data <?php echo $klien_detail->username;?></a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row" style="margin:-15px 0 0 -20px">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive">
                <table class="table table-hover">
                    <tr><td>Nama</td><td id="email_">:</td><td> <?= $klien_detail->username ?></td></tr>
                    <tr><td>Password</td><td id="telp_">:</td><td> <?= $klien_detail->password ?></td></tr>
                    <tr><td>Alamat</td><td id="hrd_">:</td><td> <?= $klien_detail->alamat ?></td></tr>
                    <tr><td>Telpon</td><td>:</td><td> <?= $klien_detail->telp ?></td></tr>
                    <tr><td>Jenis Kelamin</td><td>:</td><td><?= $klien_detail->jk == 'P' ? 'Perempuan' : 'Laki-laki' ?></td></tr>
                    <tr><td>Tempat Tanggal Lahir</td><td>:</td><td><?= date('d M Y', strtotime($klien_detail->ttl)) ?></td></tr>
                    <tr><td>Pendidikan Terakhir</td><td>:</td><td><?= $klien_detail->pend_terakhir ?></td></tr>
                    <tr><td>Foto</td><td>:</td><td><?= $klien_detail->foto ?></td></tr>
                    <tr><td>Bayar</td><td>:</td><td><?= $klien_detail->bayar ?></td></tr>
                    <tr><td>Kuota SMS</td><td>:</td><td><?= $klien_detail->kuota_sms ?></td></tr>
                    <tr><td>Update Kuota</td><td>:</td><td><?= $klien_detail->update_kuota ?></td></tr>
                    <tr><td>CV</td><td>:</td><td><?= $klien_detail->cv ?></td></tr>
                    <tr><td>KTP</td><td>:</td><td><?= $klien_detail->ktp ?></td></tr>
                    <tr><td>Ijazah Depan</td><td>:</td><td><?= $klien_detail->ijazah_depan ?></td></tr>
                    <tr><td>Ijazah belakang</td><td>:</td><td><?= $klien_detail->ijazah_belakang ?></td></tr>
                    <tr><td>pas</td><td>:</td><td><?= $klien_detail->pas ?></td></tr>
                    <tr><td>Berkas 1</td><td>:</td><td><?= $klien_detail->berkas1 ?></td></tr>
                    <tr><td>Berkas 2</td><td>:</td><td><?= $klien_detail->berkas2 ?></td></tr>
                    <tr><td>Berkas 3</td><td>:</td><td><?= $klien_detail->berkas3 ?></td></tr>
                </table>
              </div>
            </div>
            <center>
              <a style="display: none;" href="#" id="show_modal" class="btn btn-big btn-warning">Edit Data Perusahaan</a><br>
            </center>
            <p></p>
          </div>
        </section><!-- /.content -->
      </div>