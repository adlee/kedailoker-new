<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Form Set Kuota SMS
            <small>(semua calon/pekerja)</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url();?>adminkl1012/klien/setkuota">Set Kuota SMS</a></li>
            <li><a>Form Set Kuota SMS</a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <form action="<?php echo base_url();?>adminkl1012/klien/setkuota/process" method="post">
                    <div class="form-group">
                      <label>Tentukan jumlah kuota SMS untuk masing-masing klien</label>
                      <input type="text" class="form-control" name="off" style="margin-bottom:10px" placeholder="Jumlah untuk klien berstatus OFF" required onkeydown="return numbers(event);">
                      <input type="text" class="form-control" name="warning" style="margin-bottom:10px" placeholder="Jumlah untuk klien berstatus WARNING" required onkeydown="return numbers(event);">
                      <input type="text" class="form-control" name="on" style="margin-bottom:10px" placeholder="Jumlah untuk klien berstatus ON" required onkeydown="return numbers(event);">
                    </div>
                    <div >
                       <input type="submit" name="submit" class="btn btn-primary" style="margin-bottom:10px;" value="Simpan">  &nbsp 
                       <input type="button" class="btn btn-primary" style="margin-bottom:10px;color:white;font-weight:bold;background:#6B6B6B" value="Kembali" onclick="history.back(-1)" >
                       <button class="btn btn-primary" style="color:white;font-weight:bold;background:#6B6B6B;float:right" onclick='swal({title: "Deskripsi Status",   
                          text: " &nbsp &nbsp OFF = Pencari kerja dengan status kuota 0, &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp WARNING = Pencari kerja dengan status kuota antara 1 - 5,  &nbsp &nbsp &nbsp &nbsp &nbsp ON = Pencari kerja dengan status kuota lebih dari 5",  showCancelButton: false,   closeOnConfirm: false,   animation: "slide-from-top",   
                          });'>Deskripsi Status
                       </button>
                    </div>
                  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->