<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Klien Baru
            <small>(calon/pekerja)</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Klien Baru</a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Daftar</th>
                        <th>Email</th>
                        <th>Status Pencaker</th>
                        <th>Telepon</th>
                        <th>Bayar</th>
                        <th>Status</th>
                        <th height="10%">Aksi</th>
                      </tr>
                    </thead>
                    <tbody><?php
                    $no=1;
                    foreach ($klien as $klien_items) { 
                      $id=$klien_items['id'];
                      $byr=$klien_items['bayar'];
                      $sts=$klien_items['status'];
                      if ($byr<30000) {
                        $sms=30;
                      }else if ($byr<=50000) {
                        $sms=60;
                      }else if ($byr<=100000) {
                        $sms=100;
                      }?>
                      <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $klien_items['gabung'];?></td>
                        <td><?php echo $klien_items['email'];?></td>
                        <td><?php echo $klien_items['status_cari_kerja'];?></td>
                        <td><?php echo $klien_items['telp'];?></td>
                        <td>Rp. <?php $harga=number_format($byr,0,",",".");
                            echo"$harga";?>,-
                        </td>
                        <td><?php echo $sts;?></td>
                        <td align="center"> 
                          <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Opsi
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                              <li><a href="<?=site_url('adminkl1012/klien/detail/'.$id)?>" style="padding:1px 5px">Detail</a></li>
                              <?php 
                              if ($sts=="pending" or $sts == "off" ) { ?>
                                <li><a href="#" class="" style="padding:1px 10px" onclick='swal({title: "Aktifkan akun ?",   
                                  text: "Pastikan <?php echo $klien_items['username'];?> telah membayar uang sejumlah Rp. <?php $harga=number_format($byr,0,",","."); echo"$harga,00";?> untuk mendapatkan kuota SMS sejumlah :",   type: "input",   showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   
                                  inputValue: "<?php echo $sms ?>" }, function(inputValue){   if (inputValue === false) return false;      
                                  if (inputValue === "") {     swal.showInputError("Kuota SMS harus diisi!");     return false   }      
                                   window.location.href="<?php echo base_url();?>adminkl1012/klien/on/<?php echo $id;?>/"+ inputValue; });'>On
                                </a></li>
                              <?php
                              }?>
                              <li><a href="#" class="" style="padding:1px 5px" onclick='swal({title: "Hapus klien <?php echo $klien_items['username'];?> ?",text: "", type: "warning",
                                    showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Delete", closeOnConfirm: false },
                                    function(){ swal("Deleted", "Data berhasil dihapus.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/klien/delete/<?php echo $id;?>"; });'>Delete
                              </a></li>
                            </ul>
                          </div> 
                        </td>
                      </tr><?php
                      $no++;
                    } ?>  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <button class="tombolon" style=";background:gray;padding:1px 10px" onclick='swal({title: "Deskripsi Status",   
                text: "OFF(user mendaftar sebagai member baru, belum memilih dan melakukan pembayaran untuk aktivasi akun)  PENDING(user sudah membayar untuk mendapatkan kuota sms, menunggu konfirmasi admin untuk aktivasi akun)  ON(user aktif, dapat menikmati fasilitas kedailoker)",  showCancelButton: false,   closeOnConfirm: false,   animation: "slide-from-top",   
                });'>Deskripsi Status
              </button>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->