<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Semua Klien
            <small>(calon/pekerja)</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Semua Klien</a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <a class="btn btn-default pull-right" style="padding:3px 20px;margin-bottom:10px" href="<?php echo base_url();?>adminkl1012/klien/csv" target="blank">
                    export to CSV
                  </a>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama / Username</th>
                        <th>Status Pencaker</th>
                        <th>Pendidikan</th>
                        <th>Email</th>
                        <th>Telepon</th>
                        <th>Kuota</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody><?php $no=1;
                    foreach ($klien as $klien_items) { 
                      $id=$klien_items['id'];
                      $sts=$klien_items['status'];
                      $byr=$klien_items['bayar'];
                      if ($byr<30000) {
                        $sms=30;
                      }else if ($byr<=50000) {
                        $sms=60;
                      }else if ($byr<=100000) {
                        $sms=100;
                      }?>
                      <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $klien_items['username'];?></td>
                        <td><?php echo $klien_items['status_cari_kerja'];?></td>
                        <td><?php echo $klien_items['pend_terakhir'];?></td>
                        <td><?php echo $klien_items['email'];?></td>
                        <td><?php echo $klien_items['telp'];?></td>
                        <td><?php echo $klien_items['kuota_sms'];?></td>
                        <td><?php if ($klien_items['kuota_sms']>5) {
                          echo "ON";
                        }else if ($klien_items['kuota_sms']>0) {
                          echo "WARNING";
                        }else {echo "OFF";}
                        ?>
                        </td>
                        <td align="center">
                          
                          <a class="tombol" style="padding:1px 5px" href="<?= site_url('adminkl1012/klien_detail/'.$klien_items['id'])?>">Detail
                          </a>
                          &nbsp;
                          <button class="tombol" style="padding:1px 5px; background: red;" onclick='swal({title: "Hapus klien <?php echo str_replace("'", "", $klien_items['username']);?> ?",text: "", type: "warning",
                                showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Delete", closeOnConfirm: false },
                                function(){ swal("Deleted", "Data berhasil dihapus.", "success"); window.location.href="<?php echo base_url();?>adminkl1012/klien/delete/<?php echo $id;?>"; });'>delete
                          </button>
                          
                        </td>
                      </tr><?php
                    $no++;
                    } ?>  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <button class="tombolon" style=";background:gray;margin-bottom:15px;padding:5px 10px" onclick='swal({title: "Deskripsi Status",   
                text: " &nbsp &nbsp OFF = Pencari kerja dengan status kuota 0, &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp WARNING = Pencari kerja dengan status kuota antara 1 - 5,  &nbsp &nbsp &nbsp &nbsp &nbsp ON = Pencari kerja dengan status kuota lebih dari 5",  showCancelButton: false,   closeOnConfirm: false,   animation: "slide-from-top",   
                });'>Deskripsi Status
              </button>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->