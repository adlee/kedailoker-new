
      <div class="content-wrapper set_kuota">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Set Kuota SMS
            <small>(calon/pekerja)</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Set Kuota SMS</a></li>
          </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr style="text-align:center;font-weight:bold">
                        <td>Status Klien</td>
                        <td>Kuota SMS</td>
                        <td>Jumlah Klien</td>
                        <td>Aksi</td>
                      </tr>
                    </thead>
                    <tbody><?php 



                    $jml_on=0;
                    foreach ($on as $klien_on) { $jml_on++; } ?>
                      <tr>
                        <td align="center">
                          <button class="form-control" style="width:100%;border:none;background:transparent;color:#00ED00;font-weight:bold" data-toggle="collapse" data-target="#on">ON</button>
                          <div id="on" class="collapse" style="background:#ABFFAB;padding:2px 0 5px 5px;margin:0 0 0 5px">
                            <p>
                              <div style="background:#eee;text-align:left;margin-bottom:20px;margin-right:5px;padding:5px">Filter Pendidikan<br/>
                                <div style="margin:5px 0">
                                <?php 
                                $cek_pend=1;
                                $pendidikanx="";
                                foreach ($on as $pendidikan_n) {
                                  if ($pendidikan_n['pend_terakhir']==$pendidikanx) {
                                  }else{
                                    $pendidikanx=$pendidikan_n['pend_terakhir'];
                                    if ($pendidikan_n['pend_terakhir']=='SD') {?>
                                    <input type="checkbox" class="pend_on_all_SD" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SD &nbsp  &nbsp <?php
                                    }else if ($pendidikan_n['pend_terakhir']=='SMP') {?>
                                    <input type="checkbox" class="pend_on_all_SMP" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SMP &nbsp  &nbsp <?php
                                    }else if ($pendidikan_n['pend_terakhir']=='SMA') {?>
                                    <input type="checkbox" class="pend_on_all_SMA" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SMA &nbsp  &nbsp <?php
                                    }else if ($pendidikan_n['pend_terakhir']=='SMK') {?>
                                    <input type="checkbox" class="pend_on_all_SMK" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SMK &nbsp  &nbsp <?php
                                    }else if ($pendidikan_n['pend_terakhir']=='D1') {?>
                                    <input type="checkbox" class="pend_on_all_D1" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D1 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_n['pend_terakhir']=='D2') {?>
                                    <input type="checkbox" class="pend_on_all_D2" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D2 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_n['pend_terakhir']=='D3') {?>
                                    <input type="checkbox" class="pend_on_all_D3" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D3 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_n['pend_terakhir']=='D4') {?>
                                    <input type="checkbox" class="pend_on_all_D4" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D4 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_n['pend_terakhir']=='S1') {?>
                                    <input type="checkbox" class="pend_on_all_S1" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>S1 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_n['pend_terakhir']=='S2') {?>
                                    <input type="checkbox" class="pend_on_all_S2" value="<?php echo $pendidikan_n['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>S2 &nbsp  &nbsp <?php
                                    }$cek_pend++;
                                  }
                                } ?>
                                </div>
                              </div>
                                <table class="table table-hover tabel_klien_on" style="background:#ABFFAB">
                                  <form method="post" action="<?php echo base_url();?>adminkl1012/klien/kuota/on/csv/<?php echo $jml_on ?>" target="blank">
                                  <input type="hidden" name="nama" value="0">
                                  <input type="hidden" name="telepon" value="0">
                                  <input type="hidden" name="pendidikan" value="0">
                                  <input type="hidden" name="kuota" value="0">
                                  <tr style="color:black;text-align:left;font-weight:bold;background:#ABFFAB">
                                    <td class="column-check-on" style="text-align:center;border-right:4px solid #eee;border-bottom:4px solid #eee;padding:5px 10px;border-top:none;margin-right:10px"><input class="check-all-on" type="checkbox" checked></td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Nama &nbsp <input type="checkbox" class="column-check-on" value="cek" name="nama" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Telepon &nbsp <input type="checkbox" class="column-check-on" value="cek" name="telepon" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Pendidikan &nbsp <input type="checkbox" class="column-check-on" value="cek" name="pendidikan" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Kuota &nbsp <input type="checkbox" class="column-check-on" value="cek" name="kuota" checked>
                                    </td>
                                  </tr>
                                  
                                  <?php 
                                  $no=1;
                                  foreach ($on as $klien_on) { ?>
                                  <tr class="pend_on_<?php echo $klien_on['pend_terakhir'] ?>"> 
                                    <td align="center" style="padding:5px 10px;border-top:none;border-right:4px solid #eee">
                                      <input type="checkbox" class="column-check-on pend_on_<?php echo $klien_on['pend_terakhir'] ?>" value="<?php echo $klien_on['id'] ?>" name="cek<?php echo $no?>" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none"><?php echo $klien_on['username'] ?></td>  
                                    <td style="padding:5px 10px;border-top:none"><?php echo $klien_on['telp'] ?></td>  
                                    <td style="padding:5px 10px;border-top:none"><?php echo $klien_on['pend_terakhir'] ?></td>  
                                    <td style="padding:5px 10px;border-top:none"><?php echo $klien_on['kuota_sms'] ?></td>  
                                  </tr><?php 
                                  $no++;
                                  } ?>
                                </table>
                            </p>
                          </div>
                        </td>
                        <td align="center">lebih dari 5</td>
                        <td align="center"><?php echo $jml_on ?></td>
                        <td align="center">
                          <button type="submit" class="form_on btn btn-default" style="padding:1px 10px">export to CSV</button>
                          </form>
                          <button class="btn btn-success" style="color:#fff;padding:1px 10px" onclick='swal({title: "Set Kuota SMS",   
                            text: "Anda akan melakukan set kuota sms terhadap semua klien dimana status klien adalah ON, masukan kuota sms pada kolom dibawah ini",   type: "input",   showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   
                            inputPlaceholder: "jumlah kuota sms" }, function(inputValue){   if (inputValue === false) return false;      
                            if (inputValue === "") {     swal.showInputError("Kuota SMS harus diisi!");     return false   }      
                            window.location.href="<?php echo base_url();?>adminkl1012/klien/setkuota/on/"+ inputValue; });'>set kuota sms
                          </button>
                        </td>
                      </tr>
                      <?php 




                    $jml_warning=0;
                    foreach ($warning as $klien_warning) { $jml_warning++; } ?>
                      <tr>
                        <td align="center">
                          <button class="form-control" style="width:100%;border:none;background:transparent;color:#EBEB00;font-weight:bold" data-toggle="collapse" data-target="#warning">Warning</button>
                          <div id="warning" class="collapse" style="background:#E6E680;padding:2px 0 5px 5px;margin:0 0 0 5px">
                            <p><div style="background:#eee;text-align:left;margin-bottom:20px;margin-right:5px;padding:5px">Filter Pendidikan<br/>
                                <div style="margin:5px 0">
                                <?php 
                                $cek_pend=1;
                                $pendidikanx="";
                                foreach ($warning as $pendidikan_w) {
                                  if ($pendidikan_w['pend_terakhir']==$pendidikanx) {
                                  }else{
                                    $pendidikanx=$pendidikan_w['pend_terakhir'];
                                    if ($pendidikan_w['pend_terakhir']=='SD') {?>
                                    <input type="checkbox" class="pend_warning_all_SD" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SD &nbsp  &nbsp <?php
                                    }else if ($pendidikan_w['pend_terakhir']=='SMP') {?>
                                    <input type="checkbox" class="pend_warning_all_SMP" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SMP &nbsp  &nbsp <?php
                                    }else if ($pendidikan_w['pend_terakhir']=='SMA') {?>
                                    <input type="checkbox" class="pend_warning_all_SMA" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SMA &nbsp  &nbsp <?php
                                    }else if ($pendidikan_w['pend_terakhir']=='SMK') {?>
                                    <input type="checkbox" class="pend_warning_all_SMK" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SMK &nbsp  &nbsp <?php
                                    }else if ($pendidikan_w['pend_terakhir']=='D1') {?>
                                    <input type="checkbox" class="pend_warning_all_D1" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D1 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_w['pend_terakhir']=='D2') {?>
                                    <input type="checkbox" class="pend_warning_all_D2" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D2 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_w['pend_terakhir']=='D3') {?>
                                    <input type="checkbox" class="pend_warning_all_D3" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D3 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_w['pend_terakhir']=='D4') {?>
                                    <input type="checkbox" class="pend_warning_all_D4" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D4 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_w['pend_terakhir']=='S1') {?>
                                    <input type="checkbox" class="pend_warning_all_S1" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>S1 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_w['pend_terakhir']=='S2') {?>
                                    <input type="checkbox" class="pend_warning_all_S2" value="<?php echo $pendidikan_w['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>S2 &nbsp  &nbsp <?php
                                    }$cek_pend++;
                                  }
                                } ?>
                                </div>
                              </div>
                                <table class="table table-hover tabel_klien_warning" style="background:#E6E680">
                                  <form method="POST" action="<?php echo base_url();?>adminkl1012/klien/kuota/warning/csv/<?php echo $jml_warning ?>" target="blank">
                                  <input type="hidden" name="nama" value="0">
                                  <input type="hidden" name="telepon" value="0">
                                  <input type="hidden" name="pendidikan" value="0">
                                  <input type="hidden" name="kuota" value="0">
                                  <tr style="color:black;text-align:left;font-weight:bold;background:#E6E680">
                                    <td class="column-check-warning" style="border-right:4px solid #eee;text-align:center;border-top:none;border-bottom:4px solid #eee;padding:5px 10px"><input class="check-all-warning" type="checkbox" checked></td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Nama &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="nama" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Telepon &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="telepon" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Pendidikan &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="pendidikan" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Kuota &nbsp <input type="checkbox" class="column-check-warning" value="cek" name="kuota" checked>
                                    </td>
                                  </tr>
                                  <?php 
                                  $now=1;
                                  foreach ($warning as $klien_warning) { ?>
                                  <tr class="pend_warning_<?php echo $klien_warning['pend_terakhir'] ?>">
                                    <td align="center" style="border-right:4px solid #eee;border-top:none;padding:5px 10px">
                                      <input type="checkbox" class="column-check-warning pend_warning_<?php echo $klien_warning['pend_terakhir'] ?>" value="<?php echo $klien_warning['id'] ?>" name="cek<?php echo $now?>" checked>
                                    </td>
                                    <td style="border-top:none;padding:5px 10px"><?php echo $klien_warning['username'] ?></td>  
                                    <td style="border-top:none;padding:5px 10px"><?php echo $klien_warning['telp'] ?></td>  
                                    <td style="border-top:none;padding:5px 10px"><?php echo $klien_warning['pend_terakhir'] ?></td>  
                                    <td style="border-top:none;padding:5px 10px"><?php echo $klien_warning['kuota_sms'] ?></td>  
                                  </tr><?php 
                                  $now++;
                                  } ?>
                                </table>
                            </p>
                          </div>
                        </td>
                        <td align="center">1 sampai 5</td>
                        <td align="center"><?php echo $jml_warning ?></td>
                        <td align="center">
                          <button type="submit" class="form_warning btn btn-default" style="padding:1px 10px">export to CSV</button>
                          </form>
                          <button class="btn btn-warning" style="color:#fff;padding:1px 10px" onclick='swal({title: "Set Kuota SMS",   
                            text: "Anda akan melakukan set kuota sms terhadap semua klien dimana status klien adalah WARNING, masukan kuota sms pada kolom dibawah ini",   type: "input",   showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   
                            inputPlaceholder: "jumlah kuota sms" }, function(inputValue){   if (inputValue === false) return false;      
                            if (inputValue === "") {     swal.showInputError("Kuota SMS harus diisi!");     return false   }      
                            window.location.href="<?php echo base_url();?>adminkl1012/klien/setkuota/warning/"+ inputValue; });'>set kuota sms
                          </button>
                        </td>
                      </tr><?php 





                    $jml_off=0;
                    foreach ($off as $klien_off) { $jml_off++; } ?>
                      <tr>
                        <td align="center">
                          <button class="form-control" style="width:100%;border:none;background:transparent;color:red;font-weight:bold" data-toggle="collapse" data-target="#off">OFF</button>
                          <div id="off" class="collapse" style="background:#FF8C8C;padding:2px 0 5px 5px;margin:0 0 0 5px">
                            <p><div style="background:#eee;text-align:left;margin-bottom:20px;margin-right:5px;padding:5px">Filter Pendidikan<br/>
                                <div style="margin:5px 0">
                                <?php 
                                $cek_pend=1;
                                $pendidikanx="";
                                foreach ($off as $pendidikan_f) {
                                  if ($pendidikan_f['pend_terakhir']==$pendidikanx) {
                                  }else{
                                    $pendidikanx=$pendidikan_f['pend_terakhir'];
                                    if ($pendidikan_f['pend_terakhir']=='SD') {?>
                                    <input type="checkbox" class="pend_off_all_SD" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SD &nbsp  &nbsp <?php
                                    }else if ($pendidikan_f['pend_terakhir']=='SMP') {?>
                                    <input type="checkbox" class="pend_off_all_SMP" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SMP &nbsp  &nbsp <?php
                                    }else if ($pendidikan_f['pend_terakhir']=='SMA') {?>
                                    <input type="checkbox" class="pend_off_all_SMA" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SMA &nbsp  &nbsp <?php
                                    }else if ($pendidikan_f['pend_terakhir']=='SMK') {?>
                                    <input type="checkbox" class="pend_off_all_SMK" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>SMK &nbsp  &nbsp <?php
                                    }else if ($pendidikan_f['pend_terakhir']=='D1') {?>
                                    <input type="checkbox" class="pend_off_all_D1" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D1 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_f['pend_terakhir']=='D2') {?>
                                    <input type="checkbox" class="pend_off_all_D2" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D2 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_f['pend_terakhir']=='D3') {?>
                                    <input type="checkbox" class="pend_off_all_D3" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D3 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_f['pend_terakhir']=='D4') {?>
                                    <input type="checkbox" class="pend_off_all_D4" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>D4 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_f['pend_terakhir']=='S1') {?>
                                    <input type="checkbox" class="pend_off_all_S1" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>S1 &nbsp  &nbsp <?php
                                    }else if ($pendidikan_f['pend_terakhir']=='S2') {?>
                                    <input type="checkbox" class="pend_off_all_S2" value="<?php echo $pendidikan_f['pend_terakhir'] ?>" name="cek_pend<?php echo $cek_pend?>" checked>S2 &nbsp  &nbsp <?php
                                    }$cek_pend++;
                                  }
                                } ?>
                                </div>
                              </div>
                                <table class="table table-hover tabel_klien_off" style="background:#FF8C8C">
                                  <form method="post" action="<?php echo base_url();?>adminkl1012/klien/kuota/off/csv/<?php echo $jml_off ?>" target="blank">
                                  <input type="hidden" name="nama" value="0">
                                  <input type="hidden" name="telepon" value="0">
                                  <input type="hidden" name="pendidikan" value="0">
                                  <input type="hidden" name="kuota" value="0">
                                  <tr style="color:black;text-align:left;font-weight:bold;background:#FF8C8C">
                                    <td class="column-check-off" style="border-right:4px solid #eee;text-align:center;border-bottom:4px solid #eee;border-top:none;padding:5px 10px;"><input class="check-all-off" type="checkbox" checked></td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Nama &nbsp <input type="checkbox" class="column-check-off" value="cek" name="nama" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Telepon &nbsp <input type="checkbox" class="column-check-off" value="cek" name="telepon" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Pendidikan &nbsp <input type="checkbox" class="column-check-off" value="cek" name="pendidikan" checked>
                                    </td>
                                    <td style="padding:5px 10px;border-top:none;border-bottom:4px solid #eee">
                                      Kuota &nbsp <input type="checkbox" class="column-check-off" value="cek" name="kuota" checked>
                                    </td>
                                  </tr>
                                  <?php 
                                  $nof=1;
                                  foreach ($off as $klien_off) { ?>
                                  <tr class="pend_off_<?php echo $klien_off['pend_terakhir'] ?>">
                                    <td align="center" style="border-right:4px solid #eee;border-top:none;padding:5px 10px">
                                      <input type="checkbox" class="column-check-off pend_off_<?php echo $klien_off['pend_terakhir'] ?>" value="<?php echo $klien_off['id'] ?>" name="cek<?php echo $nof?>" checked>
                                    </td>
                                    <td style="border-top:none;padding:5px 10px"><?php echo $klien_off['username'] ?></td>  
                                    <td style="border-top:none;padding:5px 10px"><?php echo $klien_off['telp'] ?></td>  
                                    <td style="border-top:none;padding:5px 10px"><?php echo $klien_off['pend_terakhir'] ?></td>  
                                    <td style="border-top:none;padding:5px 10px"><?php echo $klien_off['kuota_sms'] ?></td>  
                                  </tr><?php 
                                  $nof++;
                                  } ?>
                                </table>
                            </p>
                          </div>
                        </td>
                        <td align="center">0</td>
                        <td align="center"><?php echo $jml_off ?></td>
                        <td align="center">
                          <button type="submit" class="form_off btn btn-default" style="padding:1px 10px">export to CSV</button>
                          </form>
                          <button class="btn btn-danger" style="color:#fff;padding:1px 10px" onclick='swal({title: "Set Kuota SMS",   
                            text: "Anda akan melakukan set kuota sms terhadap semua klien dimana status klien adalah OFF, masukan kuota sms pada kolom dibawah ini",   type: "input",   showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   
                            inputPlaceholder: "jumlah kuota sms" }, function(inputValue){   if (inputValue === false) return false;      
                            if (inputValue === "") {     swal.showInputError("Kuota SMS harus diisi!");     return false   }      
                            window.location.href="<?php echo base_url();?>adminkl1012/klien/setkuota/off/"+ inputValue; });'>set kuota sms
                          </button>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <button class="tombolon" style=";background:gray;margin-bottom:15px;padding:5px 10px" onclick='swal({title: "Deskripsi Status",   
                text: " &nbsp &nbsp OFF = Pencari kerja dengan status kuota 0, &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp WARNING = Pencari kerja dengan status kuota antara 1 - 5,  &nbsp &nbsp &nbsp &nbsp &nbsp ON = Pencari kerja dengan status kuota lebih dari 5",  showCancelButton: false,   closeOnConfirm: false,   animation: "slide-from-top",   
                });'>Deskripsi Status
              </button><br/>
              <button class="tombolon" style=";background:gray;margin-bottom:15px;padding:5px 10px" onclick='swal({title: "EXPORT DATA",   
                text: "Anda dapat melakukan export(download) data klien berstatus ON/WARNING/OFF dimana setiap data export terhadap klien bersangkutan akan dikurangi kuota sms -1",  showCancelButton: false,   closeOnConfirm: false,   animation: "slide-from-top",   
                });'>Ketentuan export
              </button><br/>
              <a href="<?php echo base_url();?>adminkl1012/klien/setkuota/all" class="tombol" style="padding:8px 10px;clear:both">Set Kuota SMS Semua Klien</a>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
      <script>
      $(document).ready(function() {

        $('.check-all-on').on('click', function(){
          if($(this).prop('checked')){
            $('.column-check-on').prop('checked', true);
          }
          else {
            $('.column-check-on').prop('checked', false);
          }
        });
        $('.pend_on_all_SD').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_SD').prop('checked', true); $('.pend_on_SD').fadeIn(300); }
          else { $('.pend_on_SD').prop('checked', false); $('.pend_on_SD').hide('checked', false); }
        });
        $('.pend_on_all_SMP').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_SMP').prop('checked', true); $('.pend_on_SMP').fadeIn(300); }
          else { $('.pend_on_SMP').prop('checked', false); $('.pend_on_SMP').hide('checked', false); }
        });
        $('.pend_on_all_SMA').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_SMA').prop('checked', true); $('.pend_on_SMA').fadeIn(300); }
          else { $('.pend_on_SMA').prop('checked', false); $('.pend_on_SMA').hide('checked', false); }
        });
        $('.pend_on_all_SMK').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_SMK').prop('checked', true); $('.pend_on_SMK').fadeIn(300); }
          else { $('.pend_on_SMK').prop('checked', false); $('.pend_on_SMK').hide('checked', false); }
        });
        $('.pend_on_all_D1').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_D1').prop('checked', true); $('.pend_on_D1').fadeIn(300); }
          else { $('.pend_on_D1').prop('checked', false); $('.pend_on_D1').hide('checked', false); }
        });
        $('.pend_on_all_D2').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_D2').prop('checked', true); $('.pend_on_D2').fadeIn(300); }
          else { $('.pend_on_D2').prop('checked', false); $('.pend_on_D2').hide('checked', false); }
        });
        $('.pend_on_all_D3').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_D3').prop('checked', true); $('.pend_on_D3').fadeIn(300); }
          else { $('.pend_on_D3').prop('checked', false); $('.pend_on_D3').hide('checked', false); }
        });
        $('.pend_on_all_D4').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_D4').prop('checked', true); $('.pend_on_D4').fadeIn(300); }
          else { $('.pend_on_D4').prop('checked', false); $('.pend_on_D4').hide('checked', false); }
        });
        $('.pend_on_all_S1').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_S1').prop('checked', true); $('.pend_on_S1').fadeIn(300); }
          else { $('.pend_on_S1').prop('checked', false); $('.pend_on_S1').hide('checked', false);}
        });
        $('.pend_on_all_S2').on('click', function(){
          $('.check-all-on').hide();
          if($(this).prop('checked')){ $('.pend_on_S2').prop('checked', true); $('.pend_on_S2').fadeIn(300); }
          else { $('.pend_on_S2').prop('checked', false); $('.pend_on_S2').hide('checked', false); }
        });



        $('.check-all-warning').on('click', function(){
          if($(this).prop('checked')){
            $('.column-check-warning').prop('checked', true);
          }
          else {
            $('.column-check-warning').prop('checked', false);
          }
        });
        $('.pend_warning_all_SD').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_SD').prop('checked', true); $('.pend_warning_SD').fadeIn(300); }
          else { $('.pend_warning_SD').prop('checked', false); $('.pend_warning_SD').hide('checked', false); }
        });
        $('.pend_warning_all_SMP').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_SMP').prop('checked', true); $('.pend_warning_SMP').fadeIn(300); }
          else { $('.pend_warning_SMP').prop('checked', false); $('.pend_warning_SMP').hide('checked', false); }
        });
        $('.pend_warning_all_SMA').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_SMA').prop('checked', true); $('.pend_warning_SMA').fadeIn(300); }
          else { $('.pend_warning_SMA').prop('checked', false); $('.pend_warning_SMA').hide('checked', false); }
        });
        $('.pend_warning_all_SMK').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_SMK').prop('checked', true); $('.pend_warning_SMK').fadeIn(300); }
          else { $('.pend_warning_SMK').prop('checked', false); $('.pend_warning_SMK').hide('checked', false); }
        });
        $('.pend_warning_all_D1').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_D1').prop('checked', true); $('.pend_warning_D1').fadeIn(300); }
          else { $('.pend_warning_D1').prop('checked', false); $('.pend_warning_D1').hide('checked', false); }
        });
        $('.pend_warning_all_D2').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_D2').prop('checked', true); $('.pend_warning_D2').fadeIn(300); }
          else { $('.pend_warning_D2').prop('checked', false); $('.pend_warning_D2').hide('checked', false); }
        });
        $('.pend_warning_all_D3').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_D3').prop('checked', true); $('.pend_warning_D3').fadeIn(300); }
          else { $('.pend_warning_D3').prop('checked', false); $('.pend_warning_D3').hide('checked', false); }
        });
        $('.pend_warning_all_D4').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_D4').prop('checked', true); $('.pend_warning_D4').fadeIn(300); }
          else { $('.pend_warning_D4').prop('checked', false); $('.pend_warning_D4').hide('checked', false); }
        });
        $('.pend_warning_all_S1').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_S1').prop('checked', true); $('.pend_warning_S1').fadeIn(300); }
          else { $('.pend_warning_S1').prop('checked', false); $('.pend_warning_S1').hide('checked', false);}
        });
        $('.pend_warning_all_S2').on('click', function(){
          $('.check-all-warning').hide();
          if($(this).prop('checked')){ $('.pend_warning_S2').prop('checked', true); $('.pend_warning_S2').fadeIn(300); }
          else { $('.pend_warning_S2').prop('checked', false); $('.pend_warning_S2').hide('checked', false); }
        });


        $('.check-all-off').on('click', function(){
          if($(this).prop('checked')){
            $('.column-check-off').prop('checked', true);
          }
          else {
            $('.column-check-off').prop('checked', false);
          }
        });
        $('.pend_off_all_SD').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_SD').prop('checked', true); $('.pend_off_SD').fadeIn(300); }
          else { $('.pend_off_SD').prop('checked', false); $('.pend_off_SD').hide('checked', false); }
        });
        $('.pend_off_all_SMP').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_SMP').prop('checked', true); $('.pend_off_SMP').fadeIn(300); }
          else { $('.pend_off_SMP').prop('checked', false); $('.pend_off_SMP').hide('checked', false); }
        });
        $('.pend_off_all_SMA').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_SMA').prop('checked', true); $('.pend_off_SMA').fadeIn(300); }
          else { $('.pend_off_SMA').prop('checked', false); $('.pend_off_SMA').hide('checked', false); }
        });
        $('.pend_off_all_SMK').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_SMK').prop('checked', true); $('.pend_off_SMK').fadeIn(300); }
          else { $('.pend_off_SMK').prop('checked', false); $('.pend_off_SMK').hide('checked', false); }
        });
        $('.pend_off_all_D1').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_D1').prop('checked', true); $('.pend_off_D1').fadeIn(300); }
          else { $('.pend_off_D1').prop('checked', false); $('.pend_off_D1').hide('checked', false); }
        });
        $('.pend_off_all_D2').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_D2').prop('checked', true); $('.pend_off_D2').fadeIn(300); }
          else { $('.pend_off_D2').prop('checked', false); $('.pend_off_D2').hide('checked', false); }
        });
        $('.pend_off_all_D3').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_D3').prop('checked', true); $('.pend_off_D3').fadeIn(300); }
          else { $('.pend_off_D3').prop('checked', false); $('.pend_off_D3').hide('checked', false); }
        });
        $('.pend_off_all_D4').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_D4').prop('checked', true); $('.pend_off_D4').fadeIn(300); }
          else { $('.pend_off_D4').prop('checked', false); $('.pend_off_D4').hide('checked', false); }
        });
        $('.pend_off_all_S1').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_S1').prop('checked', true); $('.pend_off_S1').fadeIn(300); }
          else { $('.pend_off_S1').prop('checked', false); $('.pend_off_S1').hide('checked', false);}
        });
        $('.pend_off_all_S2').on('click', function(){
          $('.check-all-off').hide();
          if($(this).prop('checked')){ $('.pend_off_S2').prop('checked', true); $('.pend_off_S2').fadeIn(300); }
          else { $('.pend_off_S2').prop('checked', false); $('.pend_off_S2').hide('checked', false); }
        });

        
      })
        
      </script>