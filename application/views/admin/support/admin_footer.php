<?php
  if ($this->session->userdata('username')==null) {
    redirect('adminkl1011');
  }else{
?>
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>KEDAILOKER</b>
        </div>
        <strong>&nbsp</strong>
      </footer>
    </div>

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url();?>dist/plugins/jQuery/jQuery_1.2.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url();?>dist/js/admin_app.min.js"></script>
    <script src="<?php echo base_url();?>dist/js/admin_bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>dist/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>dist/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- script -->
    <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/script.js"></script>

    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
    <script>   
    $("#jobfair2").hide() 
    $('#jenisloker').on('change',function(){
            if( $('#jenisloker').val() != "jobfair"){
            $("#jobfair2").hide()
            }
            else{
            $("#jobfair2").show()
            }
        });
    </script>
  </body>
</html>
<?php
  }
?>
