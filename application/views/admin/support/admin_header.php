<?php
  if ($this->session->userdata('username')==null) {
    redirect('adminkl1011');
  }else{
?>
<!DOCTYPE html>
<html>
  <head>
    <title>ADM KEDAILOKER</title>
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>dist/css/admin-bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>dist/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>dist/css/admin-all-skins.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>dist/css/style.css">
    <!-- sweet alert -->
    <script src="<?php echo base_url();?>dist/js/jquery.js"></script>
    <script src="<?php echo base_url();?>dist/js/sweetalert.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>dist/js/tinymce/jquery.tinymce.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>dist/js/tinymce/tinymce.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/sweetalert.css">
        <script>
    tinymce.init({
      selector: '#mytextarea',
      menubar:false,
      statusbar: false
    });
    </script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini"> 
    <div class="wrapper" style="background:#290000">

      <header class="main-header"> 
        <!-- Logo -->
        <a href="<?=base_url('adminkl1012/home')?>" class="logo" style="background:#6F0B0B">
          <span class="logo-mini"><b>ADM</b></span>
          <span class="logo-lg"><b>ADMINISTARTOR</b></span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation" style="background:#6F0B0B">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="background:#6F0B0B">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <?php
             foreach ($count_klien as $klien) { ?>
              <li class="dropdown messages-menu">
                <a class="dropdown-toggle" data-toggle="dropdown" style="font-size:90%">
                  Klien
                  <span class="label label-danger"><?php echo $klien['count'] ?></span>
                </a>
              </li><?php 
            }
            foreach ($count_perusahaan as $perusahaan) { ?>
              <li class="dropdown messages-menu">
                <a class="dropdown-toggle" data-toggle="dropdown" style="font-size:90%;margin-left:-10px">
                  Perusahaan
                  <span class="label label-danger"><?php echo $perusahaan['count'] ?></span>
                </a>
              </li><?php 
            }
            foreach ($count_jobfair as $jmljf) {?>
              <li class="dropdown messages-menu">
                <a class="dropdown-toggle" data-toggle="dropdown" style="font-size:90%;margin-left:-10px">
                  Jobfair
                  <span class="label label-danger"><?php echo $jmljf['count'] ?></span>
                </a>
              </li>
              
              </li>
              <li>
                <a href="<?php echo site_url('logout');?>">
                  <span class="hidden-xs">LOGOUT</span>
                </a>
              </li> <?php
            } ?>
            </ul>
          </div>
        </nav>
      </header>

      <aside class="main-sidebar" style="background:#290000">
        <section class="sidebar">
          <ul class="sidebar-menu">
            <li><a href="<?php echo base_url();?>logout" style="background:#290000"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
            <li><a href="<?php echo base_url();?>adminkl1012/home" style="background:#290000"><i class="fa fa-home"></i> <span>Home</span></a></li>
            
            <li class="header" style="background:#1D0000;font-weight:bold;color:#fff;margin-top:10px">KLIEN</li>
            <li class="treeview">
              <a href="" style="background:#290000">
                <i class="fa fa-users"></i> <span>Data Klien</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu" style="background:#290000">
                <li><a href="<?php echo base_url();?>adminkl1012/klien/semua" style="background:#290000"><i class="fa fa-caret-right"></i>Semua Klien</a></li>
                <li><a href="<?php echo base_url();?>adminkl1012/klien/baru" style="background:#290000"><i class="fa fa-caret-right"></i>Klien Baru <?php 
                    foreach ($count_new_klien as $klien_baru) {
                      $baru= $klien_baru['count'];
                      if ($baru>0) {?>
                        <span class="label label-warning"><?php echo $baru ?></span>
                      <?php
                      }
                    }?></a>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="" style="background:#290000">
                <i class="fa fa-send-o"></i> <span>Manajemen SMS</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu" style="background:#290000">
                <li><a href="<?php echo base_url();?>adminkl1012/klien/setkuota" style="background:#290000"><i class="fa fa-caret-right"></i>Set Kuota SMS</a>
                </li>
                <li><a href="<?php echo base_url();?>adminkl1012/klien/updatekuota" style="background:#290000"><i class="fa fa-caret-right"></i>Permintaan Kuota SMS</a>
                </li>
              </ul>
            </li>
            
            <li class="header" style="background:#1D0000;font-weight:bold;color:#fff;margin-top:20px">PERUSAHAAN</li>
            <li class="treeview">
              <a href="" style="background:#290000">
                <i class="fa fa-file-text-o"></i> <span>Data Perusahaan</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu" style="background:#290000">
                <li><a href="<?php echo base_url();?>adminkl1012/perusahaan/data" style="background:#290000"><i class="fa fa-caret-right"></i> <span>Semua Perusahaan</span></a></li>
                <li><a href="<?php echo base_url();?>adminkl1012/baru/perusahaan_baru" style="background:#290000"><i class="fa fa-caret-right"></i>Perusahaan Baru <?php
                    foreach ($count_new_perusahaan as $perusahaan_baru) {
                      $baru= $perusahaan_baru['count'];
                      if ($baru>0) {?>
                        <span class="label label-warning"><?php echo $baru ?></span>
                      <?php
                      }
                    }?></a>
                </li>
                <li><a href="<?php echo base_url();?>adminkl1012/perusahaan/data_status" style="background:#290000"><i class="fa fa-caret-right"></i> <span>Manajemen Perusahaan</span></a></li>
                <li><a href="<?php echo base_url();?>adminkl1012/perusahaan/permintaan/masa/aktif" style="background:#290000"><i class="fa fa-caret-right"></i> <span>Permintaan Perusahaan</span><?php
                    foreach ($count_permintaan_perusahaan as $permintaan) {
                      $baru= $permintaan['count'];
                      if ($baru>0) {?>
                        <span class="label label-warning"><?php echo $baru ?></span>
                      <?php
                      }
                    }?>
                    </a>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="" style="background:#290000">
                <i class="fa fa-bank"></i> <span>Informasi Perusahaan</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu" style="background:#290000"><?php 
              foreach ($per as $perusahaan_items) {  
                $idper=$perusahaan_items['id']; 
                $jml=0;
                foreach ($loker_pending as $pending) {
                  if ($idper==$pending['id_perus']) {
                    $jml++;
                  }
                }
                ?>
                <li><a href="<?php echo base_url();?>adminkl1012/perusahaan/<?php echo $idper;?>" style="background:#290000"><i class="fa fa-caret-right"></i><?php echo $perusahaan_items['nama_per'];?> &nbsp 
                      <?php if ($jml>0) {?>
                        <span class="label label-warning"><?php echo $jml ?></span><?php
                      } ?>
                    </a>
                </li><?php
               } ?>
                
              </ul>
            </li>  
            
            
            <li class="header" style="background:#1D0000;font-weight:bold;color:#fff;margin-top:20px">JOBFAIR</li>
            <li><a href="<?php echo base_url();?>adminkl1012/jobfair/create" style="background:#290000"><i class="fa fa-file-text-o"></i> <span>Buat Jobfair</span></a></li>
            <li><a href="<?php echo base_url();?>adminkl1012/jobfair/data" style="background:#290000"><i class="fa fa-file-text-o"></i> <span>Data Semua Jobfair</span></a></li>

            <li class="header" style="background:#1D0000;font-weight:bold;color:#fff;margin-top:20px">LOKER</li>
            <li><a href="<?php echo base_url();?>adminkl1012/loker/create" style="background:#290000"><i class="fa fa-file-text-o"></i> <span>Buat Loker</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<?php
  }
?>
