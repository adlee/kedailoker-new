<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background:#FFF1F1;padding-bottom:12px;border-bottom:2px solid #FBE4E4">
          <h1>
            Buat Lowongan Pekerjaan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>adminkl1012"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Buat Lowongan Pekerjaan</a></li>
          </ol>
        </section>

        <section class="content">
          <?php $jml=0;foreach ($per as $perusahaan) { $jml++; } ?>
          <form method="post" action="<?php echo base_url()?>adminkl1012/loker/create/addloker" style="margin-left:-15px">
            <div class="box-body"  style="width:98%;margin-left:10px">
              <div class="form-group">
                  <label> Lowongan</label>
                  <input type="text" class="form-control" maxlength="50" name="lowongan" placeholder="jenis pekerjaan">
              </div>
              <div class="form-group">
                  <label> Kuota</label>
                  <input type="text" class="form-control" maxlength="50" name="kuota" placeholder="Banyak orang yang dibutuhkan" onkeypress="return numbers(event)">
              </div>
              <div class="form-group">
                  <label> Minimal Pendidikan</label><br>
                  <input type="checkbox" name="pend[]" value="SD"><span style="padding-right:50px;">&nbsp;SD</span>
                  <input type="checkbox" name="pend[]" value="SMP"><span style="padding-right:50px;">&nbsp;SMP</span>
                  <input type="checkbox" name="pend[]" value="SMA"><span style="padding-right:50px;">&nbsp;SMA</span>
                  <input type="checkbox" name="pend[]" value="SMK"><span style="padding-right:50px;">&nbsp;SMK</span>
                  <input type="checkbox" name="pend[]" value="D1"><span style="padding-right:50px;">&nbsp;D1</span>
                  <input type="checkbox" name="pend[]" value="D2"><span style="padding-right:50px;">&nbsp;D2</span>
                  <input type="checkbox" name="pend[]" value="D3"><span style="padding-right:50px;">&nbsp;D3</span>
                  <input type="checkbox" name="pend[]" value="D4"><span style="padding-right:50px;">&nbsp;D4</span>
                  <input type="checkbox" name="pend[]" value="S1"><span style="padding-right:50px;">&nbsp;S1</span>
                  <input type="checkbox" name="pend[]" value="S2"><span style="padding-right:50px;">&nbsp;S2</span>
             </div>
              <br>
              <div class="form-group">
                  <label> Syarat dan ketentuan</label>
                  <textarea id="mytextarea" class="form-control" name="syarat" cols="30" rows="5" placeholder="menguasai x, menguasi y, dll"></textarea>
              </div>
              <div class="form-group">
	              <label> Range gaji</label>
	              <div class="input-group">
	                <div class="input-group-addon">
	                  RP
	                </div>
	                <input type="text" class="form-control" name="gaji1" value="" placeholder="gaji minimum"  onkeypress="return numbers(event)" >
	              </div>
	              <span>s/d</span>
	              <div class="input-group">
	                <div class="input-group-addon">
	                  RP
	                </div>
	                <input type="text" class="form-control" name="gaji2" placeholder="gaji maximum" value=""  onkeypress="return numbers(event)">
	              </div>
              </div>
              <div class="form-group">
              	<label>Masa berlaku</label>
              	<div class="input-group">
					                <div class="input-group-addon">
					                  Bulanan
					                </div> 
					                <select name="masa_aktif" class="form-control">
					                	<?php 	$a1 = (rand()%9);	$a11 = (rand()%9);	$a12 = (rand()%9);
												$a2 = (rand()%9);	$a21 = (rand()%9);	$a22 = (rand()%9);
												$a3 = (rand()%9);	$a31 = (rand()%9);	$a32 = (rand()%9);
										 ?>
										<option value="<?php echo "50$a1$a11$a12";?>">1 bulan = Rp <?php echo "50.$a1$a11$a12";?></option>
										<option value="<?php echo "100$a2$a21$a22";?>">2 bulan = Rp <?php echo "100.$a2$a21$a22";?></option>
										<option value="<?php echo "150$a3$a31$a32";?>">3 bulan = Rp <?php echo "150.$a3$a31$a32";?></option>
					                </select>
					              </div>
              </div>
              <div class="form-group" >
              	<label>Jenis Loker</label>
              	<select name="jenisloker" class="form-control" id="jenisloker">
              		<option value="biasa">Biasa</option>
              		<option value="jobfair">Jobfair</option>
              	</select>
              </div>
              <div class="form-group" id="jobfair2">
              	<label>Jobfair</label>
              	<select name="jobfair" class="form-control">
              		<?php if ($job != 0 ):?>
                    <?php foreach ($jobfair as $jf):?>
                			<option value="<?php echo $jf->id ?>"><?php echo $jf->nama ?></option>
                    <?php endforeach ?>
                    <?php else: ?>
                      <option>Tidak Ada Jobfair</option>
                		<?php endif ?>
              	</select>
              </div>
              <div class="form-group">
                  <label> Perusahaan</label><br/>
                  <table class="table table-condensed">
                    <tr>
                    <?php $jml=0; $no=1; foreach ($per as $perusahaan) {
                          if($jml%3 == 0)
                            echo "</tr><tr>";
                      ?>
                      <td><input type="radio" value="<?php echo $perusahaan['id'] ?>" name="perusahaan"> <span style="padding-right:50px;"><?php echo $perusahaan['nama_per'] ?></span></td><?php
                    $no++; $jml++;
                    } ?>
                  </table>
              </div>
              <div class="box-footer" style="margin-left:-10px;background:none">
                 <input type="submit" name="simpan" id="simpan" class="btn btn-primary" value="Simpan">
                 <input type="button" class="btn btn-default"  value="Back" onclick="history.back(-1)" >
              </div>
            </div>
          </form>
        </section><!-- /.content -->
      </div>
<!-- /.content-wrapper -->

