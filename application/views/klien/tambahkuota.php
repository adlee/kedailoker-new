<!DOCTYPE html>
<html>
<head>
	<title>KEDAI LOKER</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/style.css">
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/script.js"></script>	
</head>
<body style="background-color:#FFF">
	<form action="<?php echo base_url();?>klien/tambahkuotaexc" method="post">
		<div id="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#002060;">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="kedai" style="padding:5px 0 10px 0;text-align:center">Tambah Kuota SMS</div>
			</div>
	    </div>
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:15px;color:#000">
		  
		    <center>Silahkan melakukan pembayaran untuk menerima informasi lowongan pekerjaan lewat SMS</center><br/>
		    <table border="0" align="center" style="background:#002060;text-align:center;margin-top:20px;margin-bottom:20px;color:#fff">
		    	<tbody><tr><td style="padding:7px 10px 5px 10px;">Jumlah yang harus dibayar</td>
		    	</tr>
		    	<tr><td style="padding:5px 0 7px 0">
						<select class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;height:30px;font-size:170%;font-weight:bold;background:#002060;border:none;width:350px;padding-left:18px;margin-left:5px" name="bayar" required="">
														<option value="15408">Rp. 15.408 (30 sms)</option>
							<option value="30140">Rp. 30.140 (50 sms)</option>
							<option value="50861">Rp. 50.861 (100 sms)</option>
						</select>
		    		</td>
		    	</tr>
		    </tbody></table>
		    <style>
			.sms{
				background-color:white;
				margin: 5px;
				text-align: center;
				padding: 5px;
				margin-bottom: 10px;

			}
		    </style>

		    <div class="row">
		    	<div class="col-md-1 col-lg-1"></div>
		    	<div class="col-md-3 col=lg-3 sms">Untuk 30 SMS+bonus 10 SMS jika Anda mentransfer sesuai pembayaran dan 3 kode unik di belakangnya</div>
		    	<div class="col-md-3 col-lg-3 sms">Untuk 50 SMS+bonus 15 SMS jika Anda mentransfer sesuai pembayaran dan 3 kode unik di belakangnya</div>
		    	<div class="col-md-3 col-lg-3 sms">Untuk 100 SMS+bonus 30 SMS jika Anda mentransfer sesuai pembayaran dan 3 kode unik di belakangnya</div>
		    	<div class="col-md-1 col-lg-1"></div>
		    </div>
		    Agar akun anda dapat diproses dan diaktifkan oleh admin kedailoker, maka ikuti langkah berikut ini:
		    <table border="0" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    	<tr>
		    		<td valign="top" style="padding:5px 10px 0 0">1.</td>
		    		<td style="padding-top:5px;text-align:justify">Mohon segera lakukan pembayaran sebelum (H-2) November 2016 jam WIB ke salah satu rekening resmi KedaiLoker.com dibawah ini :</td>
		    	</tr>
		    	<tr>
		    		<td>&nbsp</td>
		    		<td  style="padding-top:15px">
		    		<?php $no=1;
		    		foreach ($bank as $data_bank) { 
		    			if ($no==1) { ?>
		    				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:5px">
		    				<img src="<?php echo base_url();?>dist/img/bank_bca.jpg" width="100%">
		    			<?php
		    			}else if ($no==2) { ?>
		    				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:5px">
		    				<img src="<?php echo base_url();?>dist/img/bank_bni.jpg" width="100%">
		    			<?php
		    			}else if ($no==3) { ?>
		    				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:5px">
		    				<img src="<?php echo base_url();?>dist/img/bank_bri.jpg" width="100%">
		    			<?php
		    			}else if ($no==4) { ?>
		    				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:5px">
		    				<img src="<?php echo base_url();?>dist/img/bank_mandiri.jpg" width="100%">
		    			<?php
		    			}
		    			?>
<pre style="position:absolute;background:none;border:none;top:28%;left:7%;width:85%">
BANK <?php echo $data_bank['nama'] ?>

Cab. <?php echo $data_bank['cabang'] ?>

<?php echo $data_bank['rekening'] ?>

a/n <?php echo $data_bank['atas_nama'] ?>
</pre>
							</div>
		    		<?php
		    		$no++;
		    		} ?>
		    		</td>
		    	</tr>
		    	<tr >

		    		<td valign="top" style="padding-top:25px">2. </td>
		    		<td style="padding-top:25px;text-align:justify">Pembayaran harus menyertakan kode unik yang ditambahkan setelah jumlah pendaftaran untuk mempercepat konfirmasi.
					</td>
		    	</tr>
		    	<tr>
		    		<td valign="top" style="padding-top:15px">3. </td>
		    		<td style="padding-top:15px;text-align:justify">Pembayaran tanpa kode unik akan menyebabkan keterlambatan konfirmasi.<br> Silahkan SMS kami di nomor ..... dengan format NAMA LENGKAP_NOMOR HP_JUMLAH PEMBAYARAN <br/>
		    			</td>
		    	</tr>

		    	<tr>
		    		<td valign="top" style="padding-top:15px">4. </td>
		    		<td style="padding-top:15px;text-align:justify">Akun akan otomatis dibatalkan dalam waktu 2 x 24 jam jika Anda tidak melakukan pembayaran dan konfirmasi pembayaran tanpa kode unik.<br/>
		    			</td>
		    	</tr>
		    	
		    	<tr>
		    		<td valign="top" style="padding-top:15px">5. </td>
		    		<td style="padding-top:15px;text-align:justify">Butuh bantuan? silahkan hubungi kami di :<br/>
		    			<ol>
		    				<li>08...</li>
		    				<li>email : penawaranterbaik@kedailoker.com</li>
		    				<li>Telp 024 76583450</li>
		    			</ol>
		    			</td>
		    	</tr>
		    </table>
	    </div>
	    <div style="margin:10px 20px 5px 0;float:right;padding:10px 30px">
	    	<a href="<?php echo base_url();?>klien" style="padding:10px 15px" class="btn btn-danger">Kembali</a>
	    	<input type="submit" class="btn btn-primary" style="padding:9px 15px" value="Bayar">
	    </div>
	 </form>
</body>
</html>