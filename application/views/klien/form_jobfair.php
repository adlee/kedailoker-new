<!DOCTYPE html>
<html>
<head>
	<title>KEDAI LOKER (Jobfair)</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/style.css">
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/script.js"></script>	
	<!-- editor -->
    <script type="text/javascript" src="<?php echo base_url();?>dist/js/tinymce/jquery.tinymce.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/tinymce/tinymce.min.js"></script>
    <script>
	  tinymce.init({
	    selector: '#mytextarea',
	    menubar:false,
	    statusbar: false
	  });
  	</script>
  	<script>
	  tinymce.init({
	    selector: '#mytextarea2',
	    menubar:false,
	    statusbar: false
	  });
  	</script>
</head>
<body style="background-color:#FFE0E0">
	<form action="<?php echo base_url();?>klien/jobfair/create" method="post">
		<div id="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#002060;">
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
				<div class="kedai" style="padding:5px">kedailoker</div>
			</div>
			<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style="background:#002060;background:red;background:#002060;color:#fff;font-weight:bold;font-size:117%;padding:17px 5px 23px 20px">
				Pendaftaran JOBFAIR GRATIS & INFORMASI JADWAL JOBFAIR GRATIS Se. JATENG
			</div>
	    </div>
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:30px">
	    	<?php foreach ($user as $pelamar) {?>
			<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" value="<?php echo $pelamar['username'] ?>" required readonly name="nama">
			<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" maxlength="12" value="<?php echo $pelamar['telp'] ?>" required readonly name="telepon" onkeydown="return numbers(event);">
			<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" value="<?php echo $pelamar['alamat'] ?>" required readonly name="alamat">
			<p style="clear:both">Jenis Kelamin</p>
			<div id="jkelamin" style="clear:both">
				<?php $jk= $pelamar['jk'];
					if ($jk="L") { ?>
						<label class="control control--radio">
					  		<input value="Perempuan" type="radio" required readonly name="jkelamin">Perempuan 1
					  		<div class="control__indicator"></div>
						</label>
						<label class="control control--radio">
					  		<input value="Laki-laki" type="radio" required readonly name="jkelamin" checked>Laki-laki 1
					  		<div class="control__indicator"></div>
						</label><?php
					}else { ?>
						<label class="control control--radio">
					  		<input value="Perempuan" type="radio" required readonly name="jkelamin" checked>Perempuan
					  		<div class="control__indicator"></div>
						</label>
						<label class="control control--radio">
					  		<input value="Laki-laki" type="radio" required readonly name="jkelamin">Laki-laki
					  		<div class="control__indicator"></div>
						</label><?php
					}
				 ?>
			</div>
			<p style="margin-top:10px">Pendidikan Terakhir</p>
			<select class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" readonly style="height:35px;text-align:center;margin-right:90px" name="pendidikan"  required>
				<?php
$selected = $pelamar['pend_terakhir'];
$options= array("SD", "SMP","SMK","SMA","D1","D2","D3","D4","S1","S2");
    foreach($options as $option){
        if($selected == $option){
            echo "<option selected='selected' value='$option'>$option</option>" ;
        }else{
            echo "<option value='$option'>$option</option>" ;
        }
    }
?>
			</select>
			<p style="clear:both">Pendidikan informal/ kursus/ pelatihan</p>
			<textarea id="mytextarea" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" readonly style="height:225px;" name="informal">
				<?php $no=1; foreach ($informal as $pendinformal) {
				 	echo $no.". ".$pendinformal['pendidikan']." <br/>&nbsp &nbsp Di ".$pendinformal['institusi']."<br/>&nbsp &nbsp Tahun ".$pendinformal['masuk']." - ".$pendinformal['keluar']."<br/><br/>";
				 	$no++;
				}?>
			</textarea>
			<?php } ?>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:25px;margi-green">
			<div>
				<p style="padding:8px 0 8px 0">Pengalaman Kerja</p>
				<textarea id="mytextarea2" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" readonly style="height: 100px;" name="pengalaman" >
					<?php $no=1; foreach ($pengalaman as $peng) {
					 	echo $no.". ".$peng['jabatan']." <br/>&nbsp &nbsp Di ".$peng['institusi']."<br/>&nbsp &nbsp Tahun ".$peng['masuk']." - ".$peng['keluar']."<br/><br/>";
					 	$no++;
					}?>
				</textarea>
			</div>
			<br/>
			<p style="clear:both;padding:0">PILIH DAFTAR JOBFAIR GRATIS</p>
			<div style="background:#D22E2E;height:225px;px;overflow:scroll;margin:0 0 10px 0px" class="pilihdaftar">
			  	<div style="background:#D22E2E;margin-left:-15px" >
				<ul style="background:none">
					<?php $jml=0; $no=1; foreach ($jobfair as $jf) { ?>
					<li><input type="radio" required name="cek" value="<?php echo $jf['id'] ?>" style="margin:0 10px 0 -20px"><?php echo $jf['nama']." (".$jf['lokasi'].")";?></li>
					<?php
					$jml++; $no++;
					} ?>
				</ul>
				<?php 	
				$a1 = (rand()%9);	$a11 = (rand()%9);	$a12 = (rand()%9); 
				$urutan = "$a1$a11$a12"
				?>
				<input type="hidden" name="urutan" value="<?php echo $urutan ?>">
				<input type="hidden" name="jml" value="<?php echo $jml ?>">
				</div>
			</div>
			<input type="submit" style="width:100%;padding:10px 0;margin:0 0 5px 0" class="tombol" name="daftar" value="DAFTAR">
			<input type="button" class="kembali" style="width:100%;padding:10px 0;margin:0 0 10px 0" onclick="history.back(-1)" value="kembali">
		</div>
	</form>
</body>
</html>