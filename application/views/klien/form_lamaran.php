<!DOCTYPE html>
<html>
<head>
	<title>KEDAI LOKER</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dist/css/style.css">
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/image_preview.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/script.js"></script>
</head>
<body style="background-color:#FFE0E0">
	<div id="main" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#C20000">
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-bottom:20px">
			<div class="kedai">kedailoker</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12" style="text-align:center">
			<div style="padding:0px;margin:2px 0 -5px 0;font-size: 20pt;font-weight: bold;color: #FFC600;">DATA DIRI</div>
			<div style="padding:0px;margin:0 0 4px 0;font-size: 18pt;font-weight: bold;color: #000;">Halo, Anindita</div>
		</div>
		<div class="col-lg-2 col-md-3 col-sm-4 col-xs-11">
			<div style="padding:0 0 0 11%;margin:10px 0;text-align:center;font-size: 10pt;font-weight: bold;color: #FFC600;">Kuota Anda 30 sms</div>
		</div>
    </div>
	<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12" style="margin-top:20px">
		<form action="" method="post" style="padding:2px">
			<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Nama lengkap sesuai KTP / Kartu Identitas lainya" required name="nama">
			<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" maxlength="12" placeholder="Nomor seluler aktif" required name="nomor" onkeydown="return numbers(event);">
			<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" maxlength="12" placeholder="Masukan ulang nomor seluler aktif" required name="repeatnomor" onkeydown="return numbers(event);">
			<input type="password" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Kata sandi" required name="sandi">
			<input type="text" class="col-lg-6 col-md-6 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Alamat lengkap" required name="alamat">
			<input type="text" class="col-lg-5 col-md-5 col-sm-11 col-xs-12 input" style="margin-bottom:10px;text-align:center;" placeholder="Kota/ Kabupaten" required name="alamat">
			<p style="clear:both">Tempat, Tanggal Lahir</p>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin:0 15px 0 -15px">
				<input class="input" style="padding-left:10px;width:100%" type="text" placeholder="tempat" required onkeydown="return numbersonly(event);">	
			</div>
			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-11" style="margin:0 0 0 -15px">
				<select class="col-lg-4 col-md-4 col-sm-4 col-xs-4 input" style="height:35px" name="" required>
					<option>tgl</option>
					<?php for ($i=1; $i<=31 ; $i++) { 
						?><option value="<?php echo $i; ?> "><?php echo $i; ?> </option><?php
					} ?>
				</select>
				<select class="col-lg-4 col-md-4 col-sm-4 col-xs-4 input" style="height:35px;" name="" required>
					<option>bln</option>
					<?php for ($i=1; $i<=12 ; $i++) { 
						?><option value="<?php echo $i; ?> "><?php echo $i; ?> </option><?php
					} ?>
				</select>
				<select class="col-lg-4 col-md-4 col-sm-4 col-xs-4 input" style="height:35px" name="" required>
					<option>thn</option>
					<?php for ($i=1900; $i<=date('Y') ; $i++) { 
						?><option value="<?php echo $i; ?> "><?php echo $i; ?> </option><?php
					} ?>
				</select>
			</div>
			<p style="clear:both">Jenis Kelamin</p>
			<div id="jkelamin" style="clear:both">
				<label class="control control--radio">
			  		<input type="radio" required name="jkelamin"/>Perempuan
			  		<div class="control__indicator"></div>
				</label>
				<label class="control control--radio">
				  	<input type="radio" required name="jkelamin"/>Laki-laki
				  	<div class="control__indicator"></div>
				</label>
			</div>
			<p style="margin-top:10px">Pendidikan Terakhir</p>
			<select class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="height:35px;text-align:center;margin-right:90px" name=""  required>
				<option>SD</option><option>SMP</option><option>SMA</option>
				<option>SMK</option><option>D1</option><option>D2</option>
				<option>D3</option><option>D4</option><option>S1</option>
			</select>
			<p style="clear:both">Pendidikan informal/ kursus/ pelatihan</p>
			<textarea class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="height: 105px;" placeholder="Kursus ..........................................................."></textarea>
		</form>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top:18px;margi-green">
		<div>
			<p style="padding:8px 0 8px 0">Pengalaman Kerja</p>
			<textarea class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="height: 155px;" placeholder="........................................................................"></textarea>
			<p class="col-lg-11 col-md-11 col-sm-11 col-xs-12" style="margin:5px 0 8px 0;padding:0">Gaji Yang Diinginkan</p>
			<select class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="height:35px;text-align:center;margin-right:90px" name=""  required>
				<option value="1000000-3000000">Rp 1000.000 - Rp 3.000.000</option>
				<option value="3000000-5000000">Rp 3000.000 - Rp 5.000.000</option>
				<option value="5000000-10000000">Rp 5000.000 - Rp 10.000.000</option>
			</select>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12" style="margin-top:18px;">
		<p style="padding:8px 0 8px 0">Unggah Foto</p>
		<input class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="line-height: 30px;margin:0 0 5px -10px;border:2px solid #EC7575;border-radius: 4px;height:37px;padding:5px;color:#C40000;margin-left:0;background:#fff" type="file" id="fupload" name="fupload">
		<input type="text" style="position:absolute;height:30px;border:none;background:#fff;margin:4px 0 0 2px;width:97px;" readonly="">
		<div><i id="i"></i></div>
		<input type="submit" style="width:100%;padding:10px 0;margin:10px 0" class="tombol" name="selanjutnya" value="SELANJUTNYA">
	</div>
</body>
</html>