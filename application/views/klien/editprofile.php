<?php
  if ($this->session->userdata('no')==null) {
    redirect('');
  }else{
  	foreach ($status as $status) {
  		if ($status['status']=="off") {
  			redirect('klien/pembayaran/keanggotaan');	
  		}else if ($status['status']=="pending") {
  			redirect('klien/pending');	
  		}else{
?>
<!DOCTYPE html>
<html>

<body class="begron">
		<div class="row" style="margin-top: -560px">
			<div class="col-md-3 col-xs-12">
				<a href="<?php echo base_url();?>klien/profile"><i class="btn btn-primary form-control">Info Akun</i></a>
				<ul class="nav nav-tabs nav-stacked">
				 
				  <!-- <li><a class="active" data-toggle="tab" href="#pend" style="color:#FFF;font-size:16px">Edit Profile</a></li> -->
				</ul>

			</div>
			<div class="column col-lg-9 col-md-9 col-sm-9 col-xs-12" id="main" style="text-align:left;background: #FFF;font-size: 100%;box-shadow: -1px -1px 1px 1px;">
				<div class="scrollkonten" style="padding: 0 0 0 15px ">

				<div class="tab-content">
			  

		  <div id="pend" class="tab-pane fade in active" >
		    <h3>Edit Profile</h3>
		     <?php  foreach ($pend as $pends) {
		    			$id=$pends['id'];
		   } ?>
		    <form action="<?php echo base_url();?>klien/edit_profile_execute" method="post">
		    
		    
		   <?php
			    			foreach ($user as $eachuser) {
			    				?>
			    				
			    			<div class="form-group"><input type="text" value="<?php echo $eachuser['username'];?>" name="username" placeholder="Nama Institusi" class="form-control"></div>
					    	<div class="form-group"><input type="text" value="<?php echo $eachuser['email'];?>" name="email" placeholder="Email" class="form-control" disabled></div>
					    	<div class="form-group"><input type="text" value="<?php echo $eachuser['telp'];?>" name="telp" placeholder="Nomor Telepon" class="form-control" disabled></div>
					    	<div class="form-group">
					    		<select name="pendidikan" class="form-control">

<?php
$selected = $eachuser['pend_terakhir'];
$options= array("SD", "SMP","SMK","SMA","D1","D2","D3","D4","S1","S2");
    foreach($options as $option){
        if($selected == $option){
            echo "<option selected='selected' value='$option'>$option</option>" ;
        }else{
            echo "<option value='$option'>$option</option>" ;
        }
    }
?>
</select>
					    		<br>
					    	
						   
							<input type="submit" class="btn btn-primary" Value="Simpan"> <a href="<?php echo base_url(); ?>klien/profile" class="btn btn-danger"> Batal</a></td>
			    				
			    				
			    				
			    				<?php
			    				
			    			}
			    		 ?>
			   
			    </form>
		    	</div>
		    </div>
		  </div>
		    
		    </div>
		    </div>
		  </div>
		</div>
				
			
				</div>
		</div>
	 <!-- Modal -->
    
    <script>
    	$('#gambar').on('click', function() {
    $('#profile-image-upload').click();
});
    </script>
</body>
</html>
<?php } } } ?>