<?php
   if ($this->session->userdata('no')==null) {
     redirect('');
   }else{
   	foreach ($status as $status) {
   		if ($status['status']=="off") {
   			redirect('klien/pembayaran/keanggotaan');	
         }else if ($status['status']=="pending" && $status['status_cari_kerja'] == 'free') {
            redirect('klien/pembersihan_session');
   		}else if ($status['status']=="pending" && $status['status_cari_kerja'] == 'premium') {
   			redirect('klien/pending');	
   		}else{
?>
<body class="begron">
   <div class="row"  >
      <div class="col-md-3 col-xs-12 begron">
         <a href="<?php echo base_url();?>klien/profile"><i class="btn btn-primary form-control">Info Akun</i></a>
         <p style="text-align: center;color: #fff;padding: 7px 0  7px 0;border-bottom: 1px solid #eee;margin-bottom: 0px;">
            Permintaan<br/>Kirim Lamaran
         </p>
         <div class="scroll begron">
            <ul>
               <?php foreach ($lamar as $eachlamar) { ?>
               <a href="">
                  <li style="color: white"><?php echo $eachlamar['judul'] ." (". $eachlamar['statuslam'].")"; ?></li>
               </a>
               <?php } ?>
            </ul>
         </div>
         <p style="text-align: center;color: #fff;padding: 7px 0  7px 0;border-bottom: 1px solid #eee;margin-top: 10px;">
            Semua Lowongan <br>	
         </p>
         <?php
            if ($status['status_cari_kerja']== 'free') { ?>
         <img src="<?php echo base_url('dist/img/notice.png')?>" style="width: 300px">
         <?php }elseif ($status['status_cari_kerja']== 'premium') {?>
         <div class="scroll begron">
            <ul>
               <?php foreach ($lowong as $eachlowongs) { ?>
               <li>
                  <a href="<?php echo base_url();?>klien/lamaran-minggu-ini/<?php echo $eachlowongs['id_post']; ?>"><i class="btn btn-primary form-control"><?php echo $eachlowongs['judul']; ?></i></a>
               </li>
               <?php } ?>
            </ul>
         </div>
         <?php }
            ?>
      </div>
      <div class="column col-lg-6 col-md-6 col-sm-12 col-xs-12" id="main" style="text-align:left;font-size: 100%;box-shadow: -1px -1px 1px 1px; height:712px">
         <div class="scrollkonten" style="background-color: #FFFFFF">
         </div>
         <!--a href="<?php echo base_url();?>klien/lamaran/buat"><div class="col-lg-3 col-md-4 col-sm-4 col-xs-10 kirim">Kirim Lamaran</div-->
      </div>
      <div class="col-md-3 col-xs-12 begron">
         <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#dn">Dalam Negeri</a></li>
            <li><a data-toggle="tab" href="#ln">Luar Negeri</a></li>
         </ul>
         <p style="text-align: center;color: #fff;padding: 7px 0  7px 0;border-bottom: 1px solid #eee;margin-bottom: 0px;">
            Lamaran Kerja minggu ini
         </p>
         <div class="scroll begron" style="height:283px;margin-bottom:23px;">
            <div class="tab-content">
               <div id="dn" class="tab-pane fade in active" >
                  <?php
                     if ($status['status_cari_kerja']== 'free') { ?>
                  <img src="<?php echo base_url('dist/img/notice.png') ?>" style="width: 270px">
                  <?php }elseif ($status['status_cari_kerja']== 'premium') {?>
                  <ul>
                     <?php foreach ($lowongseven as $eachlowongseven) { ?>
                     <a href="<?php echo base_url();?>klien/lamaran-minggu-ini/<?php echo $eachlowongseven['id_post']; ?>">
                        <li style="color: white"><?php echo $eachlowongseven['judul']; ?></li>
                     </a>
                     <?php } ?>
                  </ul>
                  <?php }
                     ?>
               </div>
               <div id="ln" class="tab-pane fade">
                  <?php
                     if ($status['status_cari_kerja']== 'free') { ?>
                  <img src="<?php echo base_url('dist/img/notice.png') ?>" style="width: 270px">
                  <?php }elseif ($status['status_cari_kerja']== 'premium') {?>
                  <ul>
                     <?php foreach ($lowongsevenluar as $eachlowongsevenluar) { ?>
                     <a href="<?php echo base_url();?>klien/lamaran-minggu-ini/<?php echo $eachlowongsevenluar['id_post']; ?>">
                        <li><?php echo $eachlowongsevenluar['judul']; ?></li>
                     </a>
                     <?php } ?>
                  </ul>
                  <?php }
                     ?>
               </div>
            </div>
         </div>
         <p style="text-align: center;color: #fff;padding: 7px 0  7px 0;border-bottom: 1px solid #eee;margin-top: 10px;">
            Jadwal Jobfair Gratis
         </p>
         <div class="scroll begron" style="height:220px">
            <ul>
               <?php
               
               foreach ($jobfair as $eachjob) {?>
               <a href="<?php echo base_url();?>klien/detjobfair/<?php echo $eachjob['id']; ?>">
                  <li><i class="btn btn-primary form-control"><?php echo $eachjob['nama']; ?></i></li>
               </a>
               <?php } ?>
            </ul>
         </div>
         <a href="<?php echo base_url();?>klien/jobfair">
            <div class="btn btn-primary form-control" style="vertical-align: middle;">PENDAFTARAN JOBFAIR GRATIS</div>
         </a>
      </div>
   </div>
   </div>
   <!-- Modal -->
   <?php } } } ?>