<?php
  if ($this->session->userdata('no')==null) {
    redirect('');
  }else
  {
  	foreach ($status as $status) {
  		if ($status['status']=="off") {
  			redirect('klien/pembayaran/keanggotaan');	
  		}else if ($status['status']=="pending") {
  			redirect('klien/pending');	
  		}else{
?>
<!DOCTYPE html>
<html>

<body class="begron">
	
	
		<div class="row" style="margin-top: -560px">
			<div class="col-md-3 col-xs-12">
				<a href="<?php echo base_url();?>klien/profile"><i class="btn btn-primary form-control">Info Akun</i></a>
				<ul class="nav nav-tabs nav-stacked">
				<br>
				  <li class="active"> <a data-toggle="tab" href="#datadiri" style="font-size:16px" >Data Diri</a></li>
				  <li><a data-toggle="tab" href="#peng" style="font-size:16px">Pengalaman Kerja</a></li>
				  <li><a data-toggle="tab" href="#pend" style="font-size:16px">Pendidikan Informal</a></li>
				</ul>
				<form action="<?php echo base_url()?>upload/foto" method="post" enctype="multipart/form-data">

				<p style="padding:8px 0 8px 0;color:black">Unggah Foto</p>
				<input class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="line-height: 30px;margin:-10px 0 5px -10px;border-radius:10px;margin-left:0;background:#fff" type="file" id="fupload" name="fupload" accept="image/*">
				  <div><i id="i"></i></div>
				<input type="submit" class="btn btn-success" value="Simpan">
			</form>
			</div>
			<div class="column col-lg-9 col-md-9 col-sm-9 col-xs-12" id="main" style="text-align:left;background: #FFF;font-size: 100%;box-shadow: -1px -1px 1px 1px; padding: 0 0 0 10px">
				<div>
				<div class="tab-content">
			  <div id="datadiri" class="tab-pane fade in active">
			    <h3>Profile</h3>
			    <div class="row">
			    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			    <div class="table-responsive">
			    	<table class="table table-hover"> 
			    	
			    		<?php 
			    			foreach ($user as $eachuser) {

			    				echo "<tr><td>Nama</td><td>".$eachuser['username']."</tr></td>";
			    				echo "<tr><td>Email</td><td>".$eachuser['email']."</tr></td>";
			    				echo "<tr><td>No HP</td><td>".$eachuser['telp']."</tr></td>";
			    				echo "<tr><td>Pendidikan Terakhir</td><td>".$eachuser['pend_terakhir']."</tr></td>";
			    			
			    		 ?>
			    		 <tr> <td colspan=2><a href="<?php echo base_url();?>klien/edit_profile" class="btn btn-info">Edit</a> </td></tr>
			    		

			    		<tr><td colspan=2><h3>Berkas Pendukung</h3></td> </tr>
			    		<br>
			    		<tr>
			    		 	<td>Pas Foto</td>
			    		 	<?php if($eachuser['pas']==""){
			    		 		echo "<td> Silahkan upload Pas Foto anda terlebih dahulu </td>";
			    		 	} 
			    		 	else {?>
			    		 	<?php if(substr($eachuser['pas'], -3)=="peg") { ?>

			    		 	<td> <a href="<?php echo base_url();?>dist/berkas/<?php echo substr($eachuser['pas'], 0, -4); ?>jpg" target="_blank">Download </a></td>
			    		 	<?php }
			    		 	else {?>
			    		 		<td> <a href="<?php echo base_url();?>dist/berkas/<?php echo $eachuser['pas']; ?>" target="_blank">Download </a></td>
			    		 	<?php } ?>
			    		 	<?php } ?>
			    		  </tr>

						<tr>
			    		 	<td>KTP</td>
			    		 	<?php if($eachuser['ktp']==""){
			    		 		echo "<td> Silahkan upload KTP anda terlebih dahulu </td>";
			    		 	} 
			    		 	else {?>
			    		 	<td> <a href="<?php echo base_url();?>dist/berkas/<?php echo $eachuser['ktp']; ?>" target="_blank">Download </a></td>
			    		 	<?php } ?>
			    		  </tr>

			    		 <tr>
			    		 	<td>CV</td>
			    		 	<?php if($eachuser['cv']==""){
			    		 		echo "<td> Silahkan upload cv anda terlebih dahulu </td>";
			    		 	} 
			    		 	else {?>
			    		 	<td> <a href="<?php echo base_url();?>dist/berkas/<?php echo $eachuser['cv']; ?>" target="_blank">Download </a></td>
			    		 	<?php } ?>
			    		  </tr>

			    		  <tr>
			    		 	<td>Ijazah Depan</td>
			    		 	<?php if($eachuser['ijazah_depan']==""){
			    		 		echo "<td> Silahkan upload Ijazah anda terlebih dahulu </td>";
			    		 	} 
			    		 	else {?>
			    		 	<td> <a href="<?php echo base_url();?>dist/berkas/<?php echo $eachuser['ijazah_depan']; ?>" target="_blank">Download </a></td>
			    		 	<?php } ?>
			    		  </tr>

			    		  <tr>
			    		 	<td>Ijazah Belakang</td>
			    		 	<?php if($eachuser['ijazah_belakang']==""){
			    		 		echo "<td> Silahkan upload Ijazah anda terlebih dahulu </td>";
			    		 	} 
			    		 	else {?>
			    		 	<td> <a href="<?php echo base_url();?>dist/berkas/<?php echo $eachuser['cv']; ?>" target="_blank">Download </a></td>
			    		 	<?php } ?>
			    		  </tr>
						

			    		   <tr>
			    		 	<td>Berkas/Sertifikat 1</td>
			    		 	<?php if($eachuser['berkas1']==""){
			    		 		echo "<td> Silahkan upload Berkas anda terlebih dahulu </td>";
			    		 	} 
			    		 	else {?>
			    		 	<td> <a href="<?php echo base_url();?>dist/berkas/<?php echo $eachuser['berkas1']; ?>" target="_blank">Download </a></td>
			    		 	<?php } ?>
			    		  </tr>
			    		  <tr>
			    		 	<td>Berkas/Sertifikat 2</td>
			    		 	<?php if($eachuser['berkas2']==""){
			    		 		echo "<td> Silahkan upload Berkas anda terlebih dahulu </td>";
			    		 	} 
			    		 	else {?>
			    		 	<td> <a href="<?php echo base_url();?>dist/berkas/<?php echo $eachuser['berkas2']; ?>" target="_blank">Download </a></td>
			    		 	<?php } ?>
			    		  </tr>
			    		  <tr>
			    		 	<td>Berkas/Sertifikat 3</td>
			    		 	<?php if($eachuser['berkas3']==""){
			    		 		echo "<td> Silahkan upload Berkas anda terlebih dahulu </td>";
			    		 	} 
			    		 	else {?>
			    		 	<td> <a href="<?php echo base_url();?>dist/berkas/<?php echo $eachuser['berkas3']; ?>" target="_blank">Download </a></td>
			    		 	<?php } ?>
			    		  </tr>
			    		</table>
<?php } ?>
<hr>
<table border="0">
	<tr>
		<th colspan="2">Upload CV *format pdf</th>
	</tr>
	<tbody>
		<tr>
			<form action="<?php echo base_url();?>upload/cv" method="post" enctype="multipart/form-data">
				<td><input type="file" accept="application/pdf" name="fupload" id="fupload" style="
    padding: 0 0 0 10px;"></td>
				<td><input type="submit" class="btn btn-primary" value="Upload"></td>
			</form>
		</tr>
	</tbody>
</table>

			    		 <hr>
<table border="0">
	<tr>
		<th colspan="2">Upload KTP *format pdf</th>
	</tr>
	<tbody>
		<tr>
			<form action="<?php echo base_url();?>upload/ktp" method="post" enctype="multipart/form-data">
				<td><input type="file" accept="application/pdf" name="fupload" id="fupload" style="
    padding: 0 0 0 10px;"></td>
				<td><input type="submit" class="btn btn-primary" value="Upload"></td>
			</form>
		</tr>
	</tbody>
</table>

<hr>
<table border="0">
	<tr>
		<th colspan="2">Upload Ijazah (depan) *format pdf</th>
	</tr>
	<tbody>
		<tr>
			<form action="<?php echo base_url()?>upload/ijazah-dp" method="post" enctype="multipart/form-data">
				<td><input type="file" accept="application/pdf" name="fupload" id="fupload" style="
    padding: 0 0 0 10px;"></td>
				<td><input type="submit" class="btn btn-primary" value="Upload"></td>
			</form>
		</tr>
	</tbody>
</table>

			    		 <hr>
<table border="0">
	<tr>
		<th colspan="2">Upload Ijazah (belakang) *format pdf</th>
	</tr>
	<tbody>
		<tr>
			<form action="<?php echo base_url()?>upload/ijazah-blk" method="post" enctype="multipart/form-data">
				<td><input type="file" accept="application/pdf" name="fupload" id="fupload" style="
    padding: 0 0 0 10px;"></td>
				<td><input type="submit" class="btn btn-primary" value="Upload"></td>
			</form>
		</tr>
	</tbody>
</table>

			    		 <hr>
<table border="0">
	<tr>
		<th colspan="2">Upload Pas Foto (*format jpg / png ukuran 3x4)</th>
	</tr>
	<tbody>
		<tr>
			<form action="<?php echo base_url()?>upload/pas-foto" method="post" enctype="multipart/form-data">
				<td><input type="file" accept="application/pdf" name="fupload" id="fupload" style="
    padding: 0 0 0 10px;"></td>
				<td><input type="submit" class="btn btn-primary" value="Upload"></td>
			</form>
		</tr>
	</tbody>
</table>

			    		 <hr>
			    		 <h3>Silahkan Upload 3 Sertifikat Terbaik yang anda miliki</h3>
<hr>
<table border="0">
	<tr>
		<th colspan="2">Berkas 1 / Serifikat 1</th>
	</tr>
	<tbody>
		<tr>
			<form action="<?php echo base_url()?>upload/berkas1" method="post" enctype="multipart/form-data">
				<td><input type="file" accept="application/pdf" name="fupload" id="fupload" style="
    padding: 0 0 0 10px;"></td>
				<td><input type="submit" class="btn btn-primary" value="Upload"></td>
			</form>
		</tr>
	</tbody>
</table>

			    		 <hr>
<table border="0">
	<tr>
		<th colspan="2">Berkas 2 / Serifikat 2</th>
	</tr>
	<tbody>
		<tr>
			<form action="<?php echo base_url()?>upload/berkas2" method="post" enctype="multipart/form-data">
				<td><input type="file" accept="application/pdf" name="fupload" id="fupload" style="
    padding: 0 0 0 10px;"></td>
				<td><input type="submit" class="btn btn-primary" value="Upload"></td>
			</form>
		</tr>
	</tbody>
</table>

			    		 <hr>
<table border="0">
	<tr>
		<th colspan="2">Berkas 3 / Serifikat 3</th>
	</tr>
	<tbody>
		<tr>
			<form action="<?php echo base_url()?>upload/berkas3" method="post" enctype="multipart/form-data">
				<td><input type="file" accept="application/pdf" name="fupload" id="fupload" style="
    padding: 0 0 0 10px;"></td>
				<td><input type="submit" class="btn btn-primary" value="Upload"></td>
			</form>
		</tr>
	</tbody>
</table>

			    		 <hr>
			    		 </div>
			    	</div>
			    	</div>
			    </div>
		  
		  <div id="pend" class="tab-pane fade">
		    <h3>Pendidikan Informal</h3>
		    
		   <a href="<?php echo base_url();?>klien/formtambahpendidikan" class="btn btn-primary" > Tambah </a>
		    <br>
		    <div class="row">
		    	<div class="table-responsive">
		    	<table class="table table-hover">
		    	<tr style="background-color: #FBDADA">
		    	
		    		<td width="5%"><b>No</b></td>
		    		<td><b>Instisusi</b></td>
		    		<td><b>Nama Pendidikan</b></td>
		    		<td><b>Tahun Masuk</b></td>
		    		<td><b>Tahun Keluar</b></td>
		    		
		    		<td>Aksi</td>
		    		
		    		

		    	</tr>
		    		
			    		 <?php $no=1;
			    		foreach ($user as $eachuser) {
			    			foreach ($pend as $pend) {
			    				if ($eachuser['telp']==$pend['id_pencari']) {
			    					$id_pencari=$pend['id'];?>
				    				<tr>
					    				<td width="5%"><?php echo "<p>$no".".</p>"; ?></td>
					    				<td><?php echo "<p>".$pend['institusi']."</p>";?></td>
					    				<td><?php echo "<p>".$pend['pendidikan']."</p>";?></td>
					    				<td><?php echo "<p>".$pend['masuk']."</p>";?></td>
					    				<td><?php echo "<p>".$pend['keluar']."</p>";?></td>
					    				
					    				
					    				<td><a href="<?php echo base_url();?>klien/edit-pendidikan/<?php echo $id_pencari;?>" class="btn btn-info">Edit </a> <a 		href="<?php echo base_url();?>klien/deletependidikan/<?php echo $id_pencari;?>" class="btn btn-danger"> Hapus</a></td>
				    				</tr>
				    				<?php
				    				$no++;			
			    				}			
			    			}			
			    		}
			    		 ?>
			    </table>
		    	</div>
		    </div>
		  </div>
		  <div id="peng" class="tab-pane fade">
		  <!--form action="<?php echo base_url();?>klien/tambahpengalaman" method="post"-->
		    <h3>Pengalaman Kerja</h3>
		   
		    <a href="<?php echo base_url();?>klien/formtambahpengalaman" class="btn btn-primary" >Tambah </a>
		   
		    <div class="row">
		    	<div class="table-responsive">
		    	<table class="table table-hover">
		    	<tr style="background-color: #FBDADA">
		    	
		    		<td width="5%"><b>No</b></td>
		    		<td><b>Jabatan</b></td>
		    		<td><b>Instisusi</b></td>
		    		<td><b>Tahun Masuk</b></td>
		    		<td><b>Tahun Keluar</b></td>
		    		
		    		
		    		<td>Aksi</td>

		    	</tr>
		    		<?php $no=1;
			    		foreach ($user as $eachuser) {
			    			foreach ($peng as $peng) {
			    				if ($eachuser['telp']==$peng['id_pencari']) {
			    					$id_pencari=$peng['id'];?>
				    				<tr>
					    				<td width="5%"><?php echo "<p>$no".".</p>"; ?></td>
					    				<td><?php echo "<p>".$peng['jabatan']."</p>";?></td>
					    				<td><?php echo "<p>".$peng['institusi']."</p>";?></td>
					    				<td><?php echo "<p>".$peng['masuk']."</p>";?></td>
					    				<td><?php echo "<p>".$peng['keluar']."</p>";?></td>
					    				
					    				
					    				<td><a href="<?php echo base_url();?>klien/edit-pengalaman-kerja/<?php echo $id_pencari;?>" class="btn btn-info">Edit </a> <a href="<?php echo base_url();?>klien/deletepengalaman/<?php echo $id_pencari;?>" class="btn btn-danger"> Hapus</a></td>
				    				</tr>
				    				<?php
				    				$no++;			
			    				}			
			    			}			
			    		}
			    		 ?>
			    </table>
		    	</div>
		    </div>
		    </div>
		  </div>
		</div>
				
			
				</div>
		</div>
	
<?php } } } ?>

