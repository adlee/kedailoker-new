<?php
  if ($this->session->userdata('no')==null) {
    redirect('');
  }else
  {
  	foreach ($status as $status) {
  		if ($status['status']=="off") {
  			redirect('klien/pembayaran/keanggotaan');	
  		}else if ($status['status']=="pending") {
  			redirect('klien/pending');	
  		}else{
?>
<!DOCTYPE html>
<html>
<style>
	p{
		margin-left: 5px;
	}
</style>
<body class="begron">
		<div class="row" style="margin-top: -560px">
			<div class="col-md-3 col-xs-12">
				<a href="<?php echo base_url();?>klien/profile"><i class="btn btn-primary form-control">Info Akun</i></a>
				<ul class="nav nav-tabs nav-stacked">
				<br>
				  <li><a data-toggle="tab" href="#start" style="font-size:16px">Bagaimana Saya memulainya?</a></li>
				   <li><a data-toggle="tab" href="#lamar" style="font-size:16px">Bagaimana saya melamar kerja?</a></li>
				  <li><a data-toggle="tab" href="#akun" style="font-size:16px">Bagaimana saya mereset password saya?</a></li>
				  <li><a data-toggle="tab" href="#kuota" style="font-size:16px">Bagaimana saya menambah kuota sms?</a></li>
				  <li><a data-toggle="tab" href="#Jobfair" style="font-size:16px">Bagaimana saya mendaftar jobfair?</a></li>
				</ul>
				
			</div>
			<div class="column col-lg-9 col-md-9 col-sm-9 col-xs-12" id="main" style="text-align:left;font-size: 100%;box-shadow: -1px -1px 1px 1px;">
				<div class="scrollkonten" style="padding: 0 0 0 10;">
				<div class="tab-content">

			  
		  
		  <div id="start" class="tab-pane fade">
		    <h3>Bagaimana saya memulainya?</h3>
		    <ul class="nav nav-tabs">
				  <li class="active"> <a data-toggle="tab" href="#datadiri" style="color:#360202;font-size:16px" >Mengatur Akun dan data diri</a></li>
				  <li><a data-toggle="tab" href="#cv" style="color:#360202;font-size:16px">Unggah CV</a></li>
				  <li><a data-toggle="tab" href="#pkpi" style="color:#360202;font-size:16px">menambah pengalaman kerja dan pendidikan informal</a></li>
				</ul>
				<div class="tab-content">
    <div id="datadiri" class="tab-pane fade in active">
      <h3>Mengatur Akun dan data diri</h3>
      <p>Untuk mengatur akun klik <b>Akun Info</b> lalu akan ada 3 sub menu yaitu <b>data diri, pengalaman kerja,pendidikan informal </b>silahkan lengkapi data diri anda disitu</p>
    </div>
    <div id="cv" class="tab-pane fade">
      <h3>Unggah CV</h3>
      <p>Untuk mengunggah CV klik <b>Akun Info</b> -> pilih menu <b>Data Diri</b> lalu klik <b>unggah CV</b> , file CV yang akan di unggah harus berformat pdf dan isi di dalamnya harus bisa di pertanggung jawabkan</p>
    </div>
    <div id="pkpi" class="tab-pane fade">
      <h3>Menambah pengalaman kerja dan pendidikan informal</h3>
      <p>Untuk Menambah Pengalaman kerja dan Pendidikan Informal yang anda miliki klik <b>Akun Info</b> -> Pilih Pengalaman Kerja jika ingin melengkapi data <b>Pengalaman Kerja</b> atau pilih <b>Pendidikan Informal</b> untuk melengkapi data pendidikan Informal.</p>
    </div>
    
  </div>
		   
		    <br>
		    
		  </div>



	<div id="akun" class="tab-pane fade">
		  <!--form action="<?php echo base_url();?>klien/tambahpengalaman" method="post"-->
		  <h3>Bagaimana saya mereset password saya?</h3>
		   <p>Untuk mereset Password anda klik tanda <span class='caret'></span> disamping kanan foto profile lalu pilih menu <b>Setting Akun</b> setelah itu isi password lama anda setelah itu ketikan password baru anda setelah itu klik <b>Simpan</b></p>
		    
		   
		    
	</div>
	<div id="lamar" class="tab-pane fade">
		  <!--form action="<?php echo base_url();?>klien/tambahpengalaman" method="post"-->
		  <h3>Bagaimana saya melamar kerja?</h3>
		   <p>Untuk melamar pekerjaan silahkan pilih pekerjaan yang anda inginkan pada menu <b>Home</b> setelah itu silahkan klik judul lowongan yang anda inginkan tadi, lalu pilih kirim lamaran</p>
		   <p>Setelah itu lamaran anda sudah terkirim di perusahaan yang bersangkutan anda tinggal menunggu panggilan dari perusahaan yang bersangkutan tadi di menu <b>Respon Perusahaan</b></p>
		    
		   
		    
	</div>

	<div id="kuota" class="tab-pane fade">
		  <!--form action="<?php echo base_url();?>klien/tambahpengalaman" method="post"-->
		  <h3>Bagaimana saya menambah kuota SMS saya?</h3>
		   <p>Untuk menambah SMS silahkan klik menu <b>Tambah Kuota</b></p>
		   <p>Lalu ikuti instruksi yang ada di menu itu</p>
		   <p>Lakukan Konfirmasi Pembayaran</p>
		   <p>Informasi status pembayaran bisa dilihat di menu <b>Pembayaran Update Kuota</b></p>
		    
		   
		    
	</div>

	<div id="Jobfair" class="tab-pane fade">
		  <!--form action="<?php echo base_url();?>klien/tambahpengalaman" method="post"-->
		  <h3>Bagaimana saya mendaftar jobfair?</h3>
		   <p>Untuk mendaftar jobfair anda bisa memilih jadwal jobfair yang tersedia di pojok kanan bawah halaman KedaiLoker</b></p>
		   <p>Lalu klik <b>Daftar Jobfair</b></p>
		   <p>Setelah itu isi data diri anda dan klik <b>Daftar</b></p>
		   <p>Setelah mendaftar anda akan mendapatkan tiket jobfair</b></p>
		    
		   
		    
	</div>






		  </div>
		</div>
				
			
				</div>
		</div>
	
<?php } } } ?>

