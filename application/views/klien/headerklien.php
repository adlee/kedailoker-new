<head>
	<title>KEDAI LOKER (KLIEN)</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/sweetalert.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script type="text/javascript" src="<?php echo base_url();?>dist/js/image_preview.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/script.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/sweetalert.min.js"></script>
	<style type="text/css">
	.begron{
		background-color: #002060;
	}
</style>
</head>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<div id="main" style="background-color:#C20000" >
		<div class="col-md-3" style="height:100px" >
			<center><a href="<?php echo base_url();?>klien/home" class="kedai">kedailoker</a></center>
			<div class="row">
				<div class="col-md-12">
			<?php 
				if(!$user){
		    	foreach ($user as $eachuser) {
		    		$foto=$eachuser['foto'];
		    		$data=explode(".", $foto);
		    		$nama=$data[0];
		    		$type=$data[1];
		    	?>
					 <div class="dropdown">
					 <a href="#aboutModal" data-toggle="modal" data-target="#myModal">
					<?php if ($type=="jpeg") {?>
						<img src="<?php echo base_url(); ?>dist/img/<?php echo "$nama.jpg"?>" title="foto" width=40px height=40px  id=pp>
					<?php
						
					}else{ ?>
						<img src="<?php echo base_url(); ?>dist/img/<?php echo $eachuser['foto']?>" title="foto" width=45px height=45px  id=pp>
					<?php
					} ?> 
					 </a>
						  <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown"><p style="padding-top:0px;font-size:100%;font-weight:bold;color:#000">Hello, 
					<?php
		    				$nama= explode(" ", $eachuser['username']);
		    				$namadepan=$nama[0];
		    				
		    				if($eachuser['kuota_sms']<=5){
		    					echo $namadepan." <span style='color:#FC7803'>(".$eachuser['kuota_sms'].")</span> <span class='caret'></span></p>";
		    				}
		    				echo $namadepan." <span >(".$eachuser['kuota_sms'].")</span> <span class='caret'></span></p>";
		    			}
		    		}
		    		 ?>
						 </button>
						  <ul class="dropdown-menu">
						  	<?php
		$jml=0;
		 foreach ($pesanbaru as $eachpesanbaru) {
			$jml++;}
?>							<li><a href="<?php echo base_url();?>klien/tambahkuota"> Tambah Kuota SMS</a></li>
						  	<li><a href="<?php echo base_url();?>klien/pesan">Respon Perusahaan (<?php echo $jml;  ?>)</a></li>
						  	<li><a href="<?php echo base_url();?>klien/help"> Bantuan</a></li>
						  	<li><a href="<?php echo base_url();?>klien/setting"> Setting Akun</a></li>
						  	 	<li><a href="<?php echo base_url();?>klien/statuspembayaran">Pembayaran Update Kuota</a></li>
						  	
						    <li><a href="<?php echo base_url();?>logout">LOGOUT</a></li>
						  </ul>
					</div>	
			</div>
		</div>
	</div>
	
		<div class="col-md-6" style="margin-bottom:-30px">
			<style>
				.navbarz{

				}
				.navbarz:hover{
					
					
					background: none;
				}
			</style>
			<table class="table" >
					<tr>
					<td><a href="<?php echo base_url();?>klien/pesan" style="color:white"  class="navbarz" data-toggle="tooltip" data-placement="bottom" title="Respon Perusahaan"><i class="fa fa-envelope-o" aria-hidden="true"></i> (<?php echo $jml;  ?>)
</a></td>
					<td><a href="<?php echo base_url();?>klien/help" style="color:white" class="navbarz" data-toggle="tooltip" data-placement="bottom" title="Bantuan"><i class="fa fa-question-circle" aria-hidden="true"></i> Bantuan 
</a></td>
					<td><a href="<?php echo base_url();?>klien/setting" style="color:white" class="navbarz" data-toggle="tooltip" data-placement="bottom" title="Setting"><i class="fa fa-wrench" aria-hidden="true"></i> Setting
  </a></td>
					<td><a href="<?php echo base_url();?>klien/tambahkuota" style="color:white" class="navbarz" data-toggle="tooltip" data-placement="bottom" title="Tambah Kuota SMS"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Kuota
 </a></td>
					<td><a href="<?php echo base_url();?>logout" style="color:white" class="navbarz" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout
 </a></td>
				</tr>
			</table>
		</div>
	
		<div class="col-md-6 col-xs-12">
			<?php foreach ($status as $status): 
				if ($status['status_cari_kerja']== 'free') { ?>
			<table style="margin:4px 0 -4px 0;">
				<br>	
				<tr>
					<td class=""><input type="text" class="form-control" name="judul" placeholder="Judul Lowongan" style="width:99.5%;padding:0 10px 0 10px;height:30px" name="" readonly></td>
					<td align="right" >
						
						<input type="text" class="form-control" style="width:99%;height:30px" name="min" placeholder="Gaji min" readonly >
					</td>
					<td align="right">

						<input type="text" class="form-control" style="width:99.5%;height:30px ;" name="max" placeholder="gaji max" readonly ></td>
				</tr>
			
				
			</table>
			
		</div>
		<div class="col-md-3 col-xs-12">
			<table style="width:100%;margin-top: 25px">
				<tr><td colspan="3"><input type="submit" class="col-lg-12 col-md-12 btn btn-primary" style="width:100%;padding:5px 0" name="cari" value="Cari Lowongan" disabled></td>
				
				</tr>
			</table>
				<?php } elseif ($status['status_cari_kerja']== 'premium') { ?>
					<form action="<?php echo base_url();?>klien/cari" method="post">
			<table style="margin:4px 0 -4px 0;">
				<br>	
				<tr>
					<td class=""><input type="text" class="form-control" name="judul" placeholder="Judul Lowongan" style="width:99.5%;padding:0 10px 0 10px;height:30px" name="" ></td>
					<td align="right" >
						
						<input type="text" class="form-control" style="width:99%;height:30px" name="min" placeholder="Gaji min"   >
					</td>
					<td align="right">

						<input type="text" class="form-control" style="width:99.5%;height:30px ;" name="max" placeholder="gaji max"   ></td>
				</tr>
			
				
			</table>
			
		</div>
		<div class="col-md-3 col-xs-12">
			<table style="width:100%;margin-top: 25px">
				<tr><td colspan="3"><input type="submit" class="col-lg-12 col-md-12 btn btn-primary" style="width:100%;padding:5px 0" name="cari" value="Cari Lowongan"></td>
				
				</tr>
			</table>
			</form>
				<?php }
			?>

			<?php endforeach ?>
		</div>
	</div>