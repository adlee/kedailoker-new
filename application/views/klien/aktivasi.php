<div class="container" align="center">
    <div class="row">
<h3><center>Silahkan pilih paket-paket yang kami sediakan</center></h3>
<h4><center>Dapatkan keuntungan lebih dengan menjadi menjadi anggota PREMIUM</center></h4><br>
        <div class="col-xs-12 col-md-4 col-md-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Gratis</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                Lamar kerja Job Fair Disnaker se – Jawa Tengah
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Kesempatan kirim lamaran kerja Terbatas
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Posisi Lowongan Terbatas
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Kesempatan mendapatkan pekerjaan Kecil
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="<?php echo base_url('klien/pembayaran_keanggotaan_biasa')?>" class="btn btn-success btn-block" role="button">Daftar</a></div>
            </div>
        </div>
        
        <div class="col-xs-12 col-md-4">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Premium</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                Lamar kerja Job Fair Disnaker se – Jawa Tengah
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Kesempatan kirim lamaran kerja
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Posisi Lowongan Tak Terbatas
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Info via SMS 30 SMS
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Info Lowongan Terbaru
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Info Job Fair Disnaker se – Jawa Tengah
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Kesempatan mendapatkan pekerjaan Lebih Besar
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="<?php echo base_url('klien/pembayaran_keanggotaan_premium')?>" class="btn btn-success btn-block" role="button">Daftar</a></div>
            </div>
        </div>
    </div>
    <a class="btn btn-info" href="<?= site_url('home')?>">Kembali ke Halaman Utama</a>
    </br>
    <p></p>
</div>