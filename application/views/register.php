<div style="color:black">
		<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12" style="font-family: arial;text-align: left;padding: 10px 17px 0 25px;">
			Register
			<ul>
			  <li style="text-align:justify">
				Membantu pencarian kerja mendapatkan informasi lowongan kerja langsung dari perusahaan yang membuka 
				lowongan kerja melalui SMS langsung ke seluler, dan pencari kerja dapat langsung mengirim lamaran ke 
				Perusahaan
			  </li>
			  <li style="text-align:justify">
			  	Membantu HRD perusahaan mendapatkan tenaga kerja sesuai klasifikasi yang dibutuhkan secara cepat dan 
			  	tepat
			  </li>
			</ul>
			<img src="<?php echo base_url();?>dist/img/promo.png" width="99%">
		</div>
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
			<h2 style="margin:10px 0 0 0;">Daftar</h2>
			<p style="padding:2px 2px 0 2px">Dengan identitas asli untuk Curiculum Vitae yang digunakan untuk melamar ke Perusahaan</p>

		<form action="klien/daftar" method="post" class="formdaftar" style="padding:2px;margin-top:-7px" name="formdaftar" id="formdaftar">

				<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Nama lengkap sesuai KTP / Kartu Identitas lainnya"  name="nama" id="nama">
				
				<input type="email" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;"  placeholder="Email" name="email" id="email">
				

				<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" maxlength="12" placeholder="Nomor seluler aktif" required name="nomor" onkeydown="return numbers(event);" id="nomor">
				
				<input type="text" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" maxlength="12" placeholder="Masukan ulang nomor seluler aktif" required name="repeatnomor" id="repeatnomor" onkeydown="return numbers(event);">
				
				<input type="password" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Kata sandi" required name="pass" id="pass">

				<input type="password" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;" placeholder="Masukan Ulang Kata sandi" required name="konfirmSandi" id="konfirmSandi">
				<p style="clear:both">Tanggal lahir</p>
				
				<select class="col-lg-3 col-md-3 col-sm-3 col-xs-3 input" style="height:35px;margin:-5px 5px 0 0" name="tanggal" required >
					<option>tgl</option>
					<?php for ($i=1; $i<=31; $i++) { 
						?><option value="<?php echo $i;?>"><?php echo $i; ?> </option><?php
					} ?>
				</select>
				<select class="input col-lg-3 col-md-3 col-sm-3 col-xs-3 input" style="height:35px;margin:-5px 5px 0 0" name="bulan" required >
					<option>bln</option>
					<?php for ($i=1; $i<=12; $i++) { 
						?><option value="<?php echo $i;?>"><?php echo $i; ?> </option><?php
					} ?>
				</select>
				<select class="input col-lg-3 col-md-3 col-sm-3 col-xs-3 input" style="height:35px;margin:-5px 5px 5px 0" name="tahun" required >
					<option>thn</option>
					<?php for ($i=1900; $i<=date('Y') ; $i++) { 
						?><option value="<?php echo $i;?>"><?php echo $i; ?> </option><?php
					} ?>
				</select>
				<div style=" margin: 5px 0 5px -15px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label class="control control--radio">
					  <input type="radio" value="P" required name="jkelamin" />Perempuan
					  <div class="control__indicator"></div>
					</label>
					<label class="control control--radio">
					  <input type="radio" value="L" required name="jkelamin" />Laki-laki
					  <div class="control__indicator"></div>
					</label>
				</div>
				<p style="clear:both">Pendidikan Terakhir</p>
				<select class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="height:35px;text-align:center;margin:-5px 90px 0 0" name="pend"  required>
					<option>SD</option><option>SMP</option><option>SMA</option>
					<option>SMK</option><option>D1</option><option>D2</option>
					<option>D3</option><option>D4</option><option>S1</option>
					<option>S2</option>
				</select>
				<p style="clear:both">Alamat</p>
				<textarea name="alamat" class="col-lg-11 col-md-11 col-sm-11 col-xs-12 input" style="text-align:center;"></textarea>
				<p style="clear:both"></p>
				<div class="form-group">
					<select class="input col-lg-3 col-md-3 col-sm-3 col-xs-3 input" style="height:35px;margin:-5px 5px 5px 0" name="provinsi" id="provinsi" required >
						<option>Provinsi</option>
						<?php foreach ($sql as $s) { ?>
							<option value="<?php echo $s['lokasi_propinsi']?>"><?php echo $s['lokasi_nama']?></option>
						<?php }?>
					</select>
					<select class="input col-lg-4 col-md-3 col-sm-3 col-xs-3 input" style="height:35px;margin:-5px 5px 5px 0" name="kabupaten" id="kabupaten" required>
						<option>Kabupaten</option>
					</select>
					<select class="input col-lg-4 col-md-3 col-sm-3 col-xs-3 input" style="height:35px;margin:-5px 5px 5px 0" name="kecamatan" id="kecamatan" required>
						<option>kecamatan</option>
					</select>
				</div>
				<div style="margin:15px 0;display:block">
					
					<p>Dengan mengklik daftar, anda kami nyatakan setuju dengan <a href="<?= site_url('sk_pencaker')?>">Syarat dan Ketentuan Pencari Kerja</a></p>
				</div>
				<input type="submit" style="padding:5px 30px;margin:7px 0 7px 0" class="col-lg-11 col-md-3 col-sm-3 col-xs-3 tombol" name="daftar" value="Daftar">
			</form>
			<a href="<?php echo base_url();?>klien/jobfairumum"><div class="col-lg-11 col-md-11 col-sm-11 col-xs-12" id="btninfo" style="padding:8px;margin-bottom:5px">Pendaftaran formulir JOBFAIR GRATIS dan info JOBFAIR <br/>GRATIS Non Member</div>
			</a>
		</div>
	</div>
</body>


<script type="text/javascript">

$(function(){

	$("#provinsi").change(function(){
		
		var value=$(this).val();
		if(value>0){
			$.ajax({
				data:{modul:'kabupaten',id:value},
				type:"POST",
				url: "<?php echo site_url('home/ambil_data') ?>",
				success: function(respond){
					var _this = $("#kabupaten");
					_this.html(respond);
					_this.change(function(){
						var value=$(this).val();
						if(value != 0){
							$.ajax({
								url: "<?php echo site_url('home/ambil_data') ?>",
								data:{modul:'kecamatan',id:$("#provinsi").val(),idkab:$("#kabupaten").val()},
								type:"POST",
								success: function(respond){
									$("#kecamatan").html(respond);
								}
							})
						}
					});
				}
			});
		}
	});
});

</script>
<script>
	$().ready(function() {
		// validate the comment form when it is submitted
		$("#formdaftar").validate({
			rules: {
				
				nomor: {
					required: true,
					minlength: 3,
					remote: {
                        url: "<?php echo base_url();?>dist/support/cek_nomor_klien.php",
                        type: "post"
                     }
				},
				repeatnomor: {
					required: true,
					minlength: 3,
					equalTo: "#nomor"
				},
				pass: {
					required: true,
					minlength: 5
				},
				konfirmSandi: {
					required: true,
					minlength: 5,
					equalTo: "#pass"
				},
				email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "<?php echo base_url();?>dist/support/cek_email_klien.php",
                        type: "post"
                     }
                },
                nama: {
                	required: true
                }
			},
			messages: {
				nomor: {
					required: "Nomor seluler tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp ",
					minlength: "Nomor harus 3 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					remote: "Nomor sudah dipakai &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
				},
				repeatnomor: {
					required: "Kolom ini tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					minlength: "Nomor harus 5 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					equalTo: "Konfirmasi nomor harus sama &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
				},
				pass: {
					required: "Lengkapi sandi unik anda &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					minlength: "Password harus 5 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
				},
				konfirmSandi: {
					required: "Kolom ini tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp",
					minlength: "Password harus 5 karakter atau lebih &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
					equalTo: "Konfirmasi password harus sama &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp"
				},
				 email: {
                    required: "Email tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
                    email: "Email ini tidak valid &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp",
                    remote: "Email sudah dipakai &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp "
                },
                nama: {
                    required: "Nama tidak boleh kosong &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp"
                  
                }
			}
		});
	});
	</script>
</html>