<div style="color:black">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-family: arial;text-align: left;padding: 10px 17px 0 25px;">
		<h3>Syarat dan Ketentuan Pencari Kerja</h3>
		<h4>Definisi</h4>
		<p>Yang dimaksud dengan:</p>
		<ol>
			<li>kedailoker.com adalah website penyedia info lowongan kerja dengan induk perusahaan PT. Anindita Muda Group</li>
			<li>Perusahaan adalah organisasi atau individu yang menggunakan kedailoker.com untuk keperluan :
				<ul>
					<li>Rekrutmen, memasang iklan atau kepentingan lainnya.</li>
					<li>Memasang iklan lowongan atau peluang kerja, atau untuk mendapat informasi secara umum.</li>
				</ul>
			</li>
			<li>Pencari kerja adalah pihak atau individu yang menggunakan kedailoker.com untuk melamar pekerjaan dan mendapatkan informasi yang tersedia di kedailoker.com atau tautan – tautan lain yang terkait dengan kedailoker.com.</li>
			<li>Curriculum Vitae atau disingkat CV adalah surat lamaran kerja yang diisi secara online berisi data diri dan riwayat hidup yang wajib diisi oleh Pencari Kerja sebelum melamar pekerjaan di Perusahaan yang terdapat di kedailoker.com.</li>
			<li>Premium Service adalah layanan Perusahaan bagi Pencari Kerja dan Perusahaan yang membayar sejumlah biaya tertentu. Bagi Pencari Kerja yang menggunakan Premium Service akan mendapatkan fasilitas tambahan berupa kesempatan melamar kerja lebih besar dan layanan lainnya yang akan ditambahkan oleh kedailoker.com. Bagi Perusahaan yang menggunakan Premium Service akan mendapatkan fasilitas tambahan berupa layanan akses mendapatkan pencari kerja yang spesifik sesuai kualifikasi yang diharapkan.</li>
		</ol>

		<h4>Definisi</h4>
		<ol>
			<li>kedailoker.com tidak bertanggung-jawab atas isi iklan lowongan kerja atau informasi

apapun yang dipasang oleh Perusahaan di kedailoker.com.</li>

		
		

<li>kedailoker.com berhak untuk melihat Curriculum Vitae, memblokir akun dan menolak

memberikan layanan kepada Pencari Kerja yang dianggap melanggar kebijakan

kedailoker.com, di mana interpretasinya menjadi hak kedailoker.com sepenuhnya.

Keputusan kedailoker.com dalam hal ini bersifat mutlak dan tidak dapat diganggu-gugat.

Pencari Kerja yang telah melakukan pembayaran Premium Service kepada

kedailoker.com tetapi ditolak untuk dilayani, berhak untuk meminta kembali

pembayaran, dengan dikenakan pemotongan biaya sebagaimana ditentukan oleh

kedailoker.com.</li>

<li>Pencari Kerja mengetahui dan menyetujui bahwa Curriculum Vitae akan ditampilkan

dan digunakan oleh Perusahaan untuk keperluan rekrutmen tenaga kerja.</li>

<li>Pencari Kerja setuju untuk tidak mencantumkan informasi yang tidak benar,

menyesatkan, melecehkan, membangkitkan kebencian, memfitnah, bersifat

diskriminatif terhadap suku, agama dan ras tertentu (SARA) ataupun menyinggung

prinsip keagamaan.</li>

<li>Pencari Kerja merupakan satu-satunya pihak yang bertanggung-jawab penuh atas

informasi yang dicantumkan dalam Curriculum Vitae.</li>

<li>Pencari Kerja setuju untuk tidak menuntut kedailoker.com dan/atau seluruh

karyawannya atas kerugian apapun yang terjadi akibat penggunaan kedailoker.com atau

link-link lain yang terkait.</li>
<li>Pencari Kerja tidak diperkenankan untuk menggunakan informasi yang diperoleh dari

kedailoker.com atau link-link lain yang terkait untuk tujuan yang melanggar hukum, atau

melanggar undang-undang hak cipta dan hak intelektual. Pelanggaran terhadap

ketentuan ini dapat diperkarakan ke pengadilan oleh kedailoker.com dan/atau pihak-

pihak lain yang dirugikan.</li>

<li>Pencari Kerja sepakat untuk menyetujui syarat dan ketentuan lain yang mungkin akan

ditambahkan oleh kedailoker.com dari waktu ke waktu tanpa pemberitahuan

sebelumnya dari kedailoker.com.</li>
</ol>
	</div>
	
</div>
</body>
</html>