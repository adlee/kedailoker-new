<?php
class klien_model extends CI_Model{

 	public function __construct(){
		$this->load->database();
	}

	public function up_pencaker_free($status){
		$nohp = $this->session->userdata('no');
		$status = $this->input->get('status');
		$data = array('status_cari_kerja' => $status, 'status');
		$this->db->where('telp', $nohp);
    	$this->db->update('pencari_lowongan', $data);
	}

	public function up_pencaker_free_pending($status){
		$nohp = $this->session->userdata('no');
		$status = $this->input->get('status');
		$data = array('status_cari_kerja' => $status, 'status' => 'pending');
		$this->db->where('telp', $nohp);
    	$this->db->update('pencari_lowongan', $data);
	}

	public function get_klien_detail($id_klien) {
		$sql = "SELECT pl.*, prov.lokasi_nama provinsi, kab.lokasi_nama kabupaten, kec.lokasi_nama kecamatan
				FROM pencari_lowongan pl
				INNER JOIN inf_lokasi prov on (prov.lokasi_ID = pl.idprov)
				INNER JOIN inf_lokasi kab on (kab.lokasi_ID = pl.idkab)
				INNER JOIN inf_lokasi kec on (kec.lokasi_ID = pl.idkec)
				where pl.id = ".$id_klien;
		$res = $this->db->query($sql);
		return $res->row();
	}


	public function get_jobfair(){
		$query = $this->db->query('select * from jobfair');
		return $query -> result_array();
	}

	public function lisjob($id){
		$query = $this->db->query("SELECT j.id, p.nama_per, l.judul, l.id_post FROM `jobfair` j LEFT JOIN lowongan l ON l.id_jobfair = j.id LEFT JOIN perusahaan p ON p.id = l.id_perus WHERE j.id = '$id'");
		return $query -> result();
	}

	public function tampil_jobfair(){
		$todayDate = date("Y-m-d");
	   	$now = strtotime(date("Y-m-d"));
	   	$date = date('Y-m-d', strtotime('-9 day', $now));
		$query = $this->db->query("SELECT * FROM `jobfair` WHERE tanggal BETWEEN '$date' AND '$todayDate'");

		if ($query->num_rows() != 0 ) {
			return $query->result();
		}else{
			return false;
		}
	}

	public function get_provinsi(){
		$query = $this->db->query('SELECT * FROM `inf_lokasi` WHERE lokasi_kabupatenkota =0 AND lokasi_kecamatan=0 and lokasi_kelurahan =0 ORDER BY lokasi_nama ASC');
		return $query -> result_array();
	}

	function get_kabupaten($provId){

	$kabupaten="<option value='0'>--pilih--</pilih>";

	$kab= $this->db->get_where('inf_lokasi',array('lokasi_propinsi'=>$provId,'lokasi_kecamatan'=>'0','lokasi_kelurahan'=>'0'));

	foreach ($kab->result_array() as $data ){
	$kabupaten.= "<option value='$data[lokasi_kabupatenkota]'>$data[lokasi_nama]</option>";
	}

	return $kabupaten;

	}

	function get_kecamatan($id, $idkab){

	$kecamatan="<option value='0'>--pilih--</pilih>";

	$kec= $this->db->get_where('inf_lokasi',array('lokasi_propinsi'=>$id,'lokasi_kabupatenkota'=>$idkab,'lokasi_kelurahan'=>'0'));

	foreach ($kec->result_array() as $data ){
	$kecamatan.= "<option value='$data[lokasi_kecamatan]'>$data[lokasi_nama]</option>";
	}

	return $kecamatan;
	}

	public function get_informal($no){
		$query = $this->db->query("select * from pendidikan_informal where id_pencari='$no' ");
		return $query -> result_array();
	}
	public function get_pengalaman($no){
		$query = $this->db->query("select * from pengalaman_kerja where id_pencari='$no' ");
		return $query -> result_array();
	}
	public function join_jf(){
		$this->load->helper('url');
		$this->load->library('form_validation');
		$nama		= $this->input->post('nama');
		$telp 		= $this->input->post('telepon');
		$alamat		= $this->input->post('alamat');
		$jkelamin 	= $this->input->post('jkelamin');
		$pendidikan = $this->input->post('pendidikan');
		$informal 	= $this->input->post('informal');
		$pengalaman = $this->input->post('pengalaman');
		$urutan 	= $this->input->post('urutan');
		$jml 		= $this->input->post('jml');
		$id_jf 		= $this->input->post('cek');

		$jf="";
	    
		$data = array(	'nama' => $nama,
						'telepon' =>  $telp,
						'alamat' => $alamat,
						'jk' => $jkelamin,
						'pendidikan' =>  $pendidikan,
						'informal' => $informal,
						'pengalaman' => $pengalaman,
						'urutan' => $urutan,
						'id_jf' => $id_jf
		);
		return $this->db->insert('join_jobfair', $data);
	}
	public function get_join($no){
		$query = $this->db->query("select jobfair.nama as jnama ,join_jobfair.nama,join_jobfair.alamat,join_jobfair.jk,join_jobfair.pendidikan,join_jobfair.telepon,join_jobfair.urutan from jobfair,join_jobfair where telepon='$no' and join_jobfair.id_jf=jobfair.id ");
		return $query -> result_array();
	}

	public function get_join_umum($no){
		$query = $this->db->query("select jobfair.nama as jnama ,join_jobfair.nama,join_jobfair.alamat,join_jobfair.jk,join_jobfair.pendidikan,join_jobfair.telepon,join_jobfair.urutan from jobfair,join_jobfair where telepon='$no' and join_jobfair.id_jf=jobfair.id ");
		return $query -> result_array();
	}		

	public function add_klien($data){

		$this->db->insert('pencari_lowongan', $data);
		?>
		<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.js"></script>
		 <script type="text/javascript"> 
	       $(document).ready(function(){

	           $("#myModals").modal("show");
	       });
	     </script>
      	<?php
		return $this->db->insert('users_login', array('email' => $data['email']));
	}

	public function get_id_pencari($no){
		$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		
		//$query = $this->db->query("select * from pencari_lowongan ");
			return $query -> result_array();
	}

	public function insertlowong($id_pencari,$id_post){
		$data = array('id_pencari'=>$id_pencari,
						'id_low'=>$id_post);
		return $this->db->insert('lamaran',$data);
	}

	public function add_pendidikan($no){
		//$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		$this->load->helper('url');
		$this->load->library('form_validation');
		//$idpekerja		= $this->input->post('nama');
		$pendidikan 	= $this->input->post('pendidikan');
		$institusi =$this->input->post('institusi'); 
		$masuk =$this->input->post('masuk');
		$keluar =$this->input->post('keluar');
		

		$data = array(	'pendidikan' => $pendidikan,
						'id_pencari' =>  $no,
						'institusi' => $institusi,
						'masuk' => $masuk,
						'keluar' => $keluar
		);

		$this->db->insert('pendidikan_informal', $data);
		redirect('klien/profile-edit-pi-sukses');
		
	}

	public function add_pengalaman($no){
		//$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		$this->load->helper('url');
		$this->load->library('form_validation');
		//$idpekerja		= $this->input->post('nama');
		$jabatan 	= $this->input->post('jabatan');
		$institusi =$this->input->post('institusi'); 
		$masuk =$this->input->post('masuk');
		$keluar =$this->input->post('keluar');
		

		$data = array(	'jabatan' => $jabatan,
						'id_pencari' =>  $no,
						'institusi' => $institusi,
						'masuk' => $masuk,
						'keluar' => $keluar
		);

		$this->db->insert('pengalaman_kerja', $data);
		redirect('klien/profile-edit-pk-sukses');
		
	}
	

	public function get_login($hp, $pass){
			$query = $this->db->get_where('pencari_lowongan', array('telp' => $hp, 'password' => $pass));
			if ($query->num_rows()>0) {
				$this->session->set_userdata('no', $hp);
				$this->session->set_userdata('password', $pass);
				redirect('klien/home');
			}else{
				redirect('');
			}
	}

	public function get_status($no){
		$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		return $query -> result_array();
	}

	public function get_klien($no = FALSE){
		if ($no == FALSE) {
			$query = $this->db->query('select * from pencari_lowongan');
			return $query -> result_array();

		}else{
			$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
			return $query -> row_array();	
		}	
	}

	public function get_payment($no){
		$this->load->helper('url');
		$data = array(
			'status'=> 'pending',
			'bayar' => $this->input->post('bayar'),
		);
		$this->db->where('telp', $no);
		$this->db->update('pencari_lowongan', $data);
		$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		return $query -> result_array();
	}

	public function update_sms($sts, $sms){
		$data = array(
			'kuota_sms' => $sms,
		);
		$this->db->where('status',$sts);
		return $this->db->update('pencari_lowongan', $data);
	}

	public function update_sms_all(){
		$off	= $this->input->post('off');
		$pending	= $this->input->post('pending');
		$on	= $this->input->post('on');
		$data = array (
			'kuota_sms' => $off,
		);
		$this->db->where('status','off');
		$this->db->update('pencari_lowongan', $data);
			
			$data2 = array (
				'kuota_sms' => $pending,
			);
			$this->db->where('status','pending');
			$this->db->update('pencari_lowongan', $data2);

				$data3 = array (
					'kuota_sms' => $on,
				);
				$this->db->where('status','on');
				return $this->db->update('pencari_lowongan', $data3);
	}

	public function cek_payment($no){
		$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		return $query -> result_array();
	}

	public function get_id($no){
		$query = $this->db->get_where('pengalaman_kerja', array('id_pencari' => $no));
		return $query -> result_array();
	}

	public function get_new_klien(){
		$query = $this->db->query("select id, username,status_cari_kerja, date_format(tgl_gabung,'%d-%m-%Y') as gabung, email, telp, bayar, status from pencari_lowongan where status='pending' or status='off'");
		return $query -> result_array();
	}

	public function get_count(){
		$query = $this->db->query('select count(id) as count from pencari_lowongan');
		return $query -> result_array();
	}
	public function get_count_new(){
		$query = $this->db->query("select count(id) as count from pencari_lowongan where status='pending' or status='off'");
		return $query -> result_array();
	}

	public function get_update(){
		$query = $this->db->query("select * from pencari_lowongan where update_kuota > 0");
		return $query -> result_array();
	}

	public function get_session($tlp){
			$query = $this->db->get_where('pencari_lowongan', array('telp' => $tlp));
			if ($query->num_rows()>0) {
				$this->session->set_userdata('no', $tlp);
				redirect('klien/pembayaran/keanggotaan');
			}else{
				redirect('adminkl1011');
			}
	}

	public function detail($value='')
	{
		$this->db->where('id', $value); 
		$this->db->from('pencari_lowongan ');  
		$query = $this->db->get(); 
        if($query->num_rows() != 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
	}

	public function get_session_tandingan($tlp){
			$query = $this->db->get_where('pencari_lowongan', array('telp' => $tlp));
			if ($query->num_rows()>0) {
				$this->session->set_userdata('no', $tlp);
				return 1;
			}else{
				return 0;
			}
	}

	public function set_on($id, $sms){
		$data = array(
			'id'=> $id,
			'status' => 'on',
			'kuota_sms' => $sms,
		);
		$this->db->where('id', $id);
		return $this->db->update('pencari_lowongan', $data);
	}

	public function count_on(){
		$query = $this->db->query("select count(id) as count from pencari_lowongan where status='on'");
		return $query -> result_array();
	}
	public function count_pending(){
		$query = $this->db->query("select count(id) as count from pencari_lowongan where status='pending'");
		return $query -> result_array();
	}
	public function count_off(){
		$query = $this->db->query("select count(id) as count from pencari_lowongan where status='off'");
		return $query -> result_array();
	}

	public function delete_permintaan_update($id){
		$data = array(
			'update_kuota' => 0,
		);
		$this->db->where('id', $id);
		return $this->db->update('pencari_lowongan', $data);
	}

	public function proses_permintaan_update($id, $sekarang, $sms){
		$baru=$sekarang+$sms;
		$data = array(
			'kuota_sms' => $baru,
			'update_kuota' => 0,
		);
		$this->db->where('id', $id);
		return $this->db->update('pencari_lowongan', $data);
	}

	public function get_pendidikan($no){
		$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		return $query -> result_array();
	}
	public function delete($id){
		return $this->db->delete('pencari_lowongan', array('id' => $id));
	}

	public function show_klien($no){
		$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		return $query -> result_array();
		//$query = $this->db->query('select * from pencari_lowongan ');
	}
	public function show_pesan($no){
		$query = $this->db->query("Select message.id as idpesan,pencari_lowongan.id,perusahaan.id,pesan,message.tanggal,message.status,perusahaan.nama_per from message,pencari_lowongan,perusahaan where pencari_lowongan.id = message.id_pencari and perusahaan.id=message.id_perusahaan and pencari_lowongan.telp='$no'");
		return $query -> result_array();
		//$query = $this->db->query('select * from pencari_lowongan ');
	}
	public function show_pesan_baru($no){
		$query = $this->db->query("Select message.id as idpesan,pencari_lowongan.id,perusahaan.id,pesan,message.tanggal,message.status,perusahaan.nama_per from message,pencari_lowongan,perusahaan where pencari_lowongan.id = message.id_pencari and perusahaan.id=message.id_perusahaan and pencari_lowongan.telp='$no' and message.status='baru'");
		return $query -> result_array();
		//$query = $this->db->query('select * from pencari_lowongan ');
	}

	public function show_lowonganseven($no,$pendi){
		if ($pendi == "SD") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' and status_kuota='buka'and perusahaan.region= 'dalam' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "SMP") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP') and status_kuota='buka' and perusahaan.region= 'dalam' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}

		else if ($pendi == "SMA") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMA') and status_kuota='buka' and perusahaan.region= 'dalam' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "SMK") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK')  and status_kuota='buka' and perusahaan.region= 'dalam' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D1") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1') and status_kuota='buka' and perusahaan.region= 'dalam' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D2") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and status_kuota='buka' and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2') and perusahaan.region= 'dalam' and lowongan.status = 'shared'  group by lowongan.id_post order by tgl desc");
		}

		else if ($pendi == "D3") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3') and status_kuota='buka' and perusahaan.region= 'dalam' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D4") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'D4') and status_kuota='buka' and perusahaan.region= 'dalam' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc
				");
		}
		else if ($pendi == "S1") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'S1') and status_kuota='buka' and perusahaan.region= 'dalam' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "S2") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'S1' or lowongan.pendidikan = 'S2') and status_kuota='buka' and perusahaan.region= 'dalam' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		
		return $query -> result_array();
		//$query = $this->db->query('select * from pencari_lowongan ');
	} 
	public function show_lowongansevenluar($no,$pendi){
		if ($pendi == "SD") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' and status_kuota='buka' and perusahaan.region='luar' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "SMP") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP') and status_kuota='buka'and perusahaan.region='luar' and lowongan.status = 'shared'  group by lowongan.id_post order by tgl desc");
		}

		else if ($pendi == "SMA") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMA') and status_kuota='buka'and perusahaan.region='luar' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "SMK") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK')  and status_kuota='buka' and perusahaan.region='luar' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D1") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1') and status_kuota='buka' and perusahaan.region='luar' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D2") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and status_kuota='buka' and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2') and perusahaan.region='luar' and lowongan.status = 'shared'  group by lowongan.id_post order by tgl desc");
		}

		else if ($pendi == "D3") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3') and status_kuota='buka' and perusahaan.region='luar' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D4") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'D4') and status_kuota='buka' and perusahaan.region='luar' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc
				");
		}
		else if ($pendi == "S1") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'S1') and status_kuota='buka'and perusahaan.region='luar' and lowongan.status = 'shared'  group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "S2") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE tgl >= ( CURDATE() - INTERVAL 7 DAY ) and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan='SMA' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'S1' or lowongan.pendidikan = 'S2') and status_kuota='buka' and perusahaan.region='luar' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		return $query -> result_array();
		//$query = $this->db->query('select * from pencari_lowongan ');
	} 
	public function show_lowongan($no,$pendi){
		$judul	= $this->input->post('judul');
		if ($pendi == "SD") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD'  and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "SMP") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}

		else if ($pendi == "SMA") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMA' and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "SMK") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK'  and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D1") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1'  and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D2") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}

		else if ($pendi == "D3") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D4") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and  perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'D4' and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc
				");
		}
		else if ($pendi == "S1") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and  perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3' or lowongan.pendidikan = 'D4'  or lowongan.pendidikan = 'S1' and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "S2") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'D4' or lowongan.pendidikan = 'S1' or lowongan.pendidikan = 'S2' and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		return $query -> result_array();
		//$query = $this->db->query('select * from pencari_lowongan ');
	}



public function show_cari($no,$pendi){
	$this->load->helper('url');
	$judul	= $this->input->post('judul');
	$min	= $this->input->post('min');
	$max	= $this->input->post('max');
		if ($pendi == "SD") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and (perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD'  ) and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "SMP") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and (perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' and )status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}

		else if ($pendi == "SMA") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and (perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMA' ) and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "SMK") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and (perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' ) and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D1") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and (perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' )  and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D2") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and (perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' ) and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}

		else if ($pendi == "D3") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3' ) and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "D4") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'D4' ) and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc
				");
		}
		else if ($pendi == "S1") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and perusahaan.id=lowongan.id_perus and (lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3' or lowongan.pendidikan = 'D4' or lowongan.pendidikan = 'S1' ) and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		else if ($pendi == "S2") {
			$query = $this->db->query("select * from lowongan,perusahaan WHERE lowongan.judul like '%$judul%' and (perusahaan.id=lowongan.id_perus and lowongan.pendidikan = 'SD' or lowongan.pendidikan = 'SMP' or lowongan.pendidikan = 'SMK' or lowongan.pendidikan = 'D1' or lowongan.pendidikan = 'D2' or lowongan.pendidikan = 'D3'  or lowongan.pendidikan = 'S1' or lowongan.pendidikan = 'S2' ) and status_kuota='buka' and lowongan.status = 'shared' group by lowongan.id_post order by tgl desc");
		}
		return $query -> result_array();
		//$query = $this->db->query('select * from pencari_lowongan ');
	}

public function show_jobfair(){
	// $todayDate = date("Y-m-d");
 //   	$now = strtotime(date("Y-m-d"));
 //   	$date = date('Y-m-d', strtotime('-9 day', $now));
	// $query = $this->db->query("SELECT * FROM `jobfair` WHERE tanggal BETWEEN '$date' AND '$todayDate'");
	$query = $this->db->query("SELECT * FROM `jobfair`");
		return $query -> result_array();
		//$query = $this->db->query('select * from pencari_lowongan ');
	}




	public function get_loker_id ($id_post){
		$query = $this->db->query("select * from lowongan,perusahaan WHERE perusahaan.id=lowongan.id_perus and lowongan.id_post = $id_post ");
		return $query -> result_array();
	}
	public function get_jobfair_id ($id_post){
		$query = $this->db->query("select * from jobfair WHERE id = $id_post ");
		return $query -> result_array();
	}

	public function show_lamaran($no){
		$query = $this->db->query("select *,lamaran.status as statuslam from lamaran,lowongan,perusahaan,pencari_lowongan where lamaran.id_pencari=pencari_lowongan.id and perusahaan.id = lowongan.id_perus and lamaran.id_low=lowongan.id_post and pencari_lowongan.telp = '$no' order by lamaran.tgl desc");
		return $query -> result_array();
		//$query = $this->db->query('select * from pencari_lowongan ');
	} 
	public function show_pendidikan($no){
		$query = $this->db->get_where('pendidikan_informal', array('id_pencari' => $no));
		return $query -> result_array();
	}
	public function show_pengalaman($no){
		$query = $this->db->get_where('pengalaman_kerja', array('id_pencari' => $no));
		return $query -> result_array();
	}
	public function show_each_pengalaman($no,$id){
		$query = $this->db->get_where('pengalaman_kerja', array('id_pencari' => $no, 'id' => $id));
		return $query -> result_array();
	}

	public function edit_pengalaman($id){
		//$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		$this->load->helper('url');
		$this->load->library('form_validation');
		//$idpekerja		= $this->input->post('nama');
		
		$jabatan 	= $this->input->post('jabatan');
		$institusi =$this->input->post('institusi'); 
		$masuk =$this->input->post('masuk');
		$keluar =$this->input->post('keluar');
		

		$data = array(	'jabatan' => $jabatan,
						
						'institusi' => $institusi,
						'masuk' => $masuk,
						'keluar' => $keluar
		);


		$this->db->where('id', $id);
		$this->db->update('pengalaman_kerja', $data);


		    $config['upload_path']          = base_url().'/dist/berkas/';
                $config['upload_url']    		=	base_url()."dist/berkas/";
                $config['allowed_types']        = 'pdf';
                $config['max_size']             = 1000;
                
                //$config ['file_name']			= $no;

                $this->load->library('upload', $config);
   

 if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            echo "hahaha";

           // $this->load->view('upload_form', $error);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());

           
          } 


		
		redirect('klien/profile-edit-pk-sukses');
		
	}
	public function show_each_pendidikan($no,$id){
		$query = $this->db->get_where('pendidikan_informal', array('id_pencari' => $no, 'id' => $id));
		return $query -> result_array();
	}

	public function edit_pendidikan($id){
		//$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		$this->load->helper('url');
		$this->load->library('form_validation');
		//$idpekerja		= $this->input->post('nama');
		$pendidikan 	= $this->input->post('pendidikan');
		$institusi =$this->input->post('institusi'); 
		$masuk =$this->input->post('masuk');
		$keluar =$this->input->post('keluar');
		

		$data = array(	'pendidikan' => $pendidikan,
						
						'institusi' => $institusi,
						'masuk' => $masuk,
						'keluar' => $keluar
		);




		$this->db->where('id', $id);
		$this->db->update('pendidikan_informal', $data);
		redirect('klien/profile-edit-pi-sukses');
		
	}
	public function baca($id_pesan){
		//$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		//$this->load->helper('url');
		//$this->load->library('form_validation');
		//$idpekerja		= $this->input->post('nama');
		
		

		$data = array('status' => 'baca');




		$this->db->where('id', $id_pesan);
		$this->db->update('message', $data);
		redirect('klien/pesan');
		
	}
	public function edit_profile($no){
		//$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		$this->load->helper('url');
		$this->load->library('form_validation');
		//$idpekerja		= $this->input->post('nama');
		$username	= $this->input->post('username');
		//$email =$this->input->post('email'); 
		//$telp =$this->input->post('telp');
		$pendidikan =$this->input->post('pendidikan');
		

		$data = array(	'username' => $username,
						
						
						'pend_terakhir' => $pendidikan
		);
		$this->db->where('telp', $no);
		$this->db->update('pencari_lowongan', $data);
		redirect('klien/profile');
	}
	public function edit_akun($no){
		//$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		$this->load->helper('url');
		$this->load->library('form_validation');
		//$idpekerja		= $this->input->post('nama');
		//$username	= $this->input->post('username');
		$pass =$this->input->post('konfirmnewsandi'); 
		//$telp =$this->input->post('telp');
		//$pendidikan =$this->input->post('pendidikan');
		

		$data = array(	
						
						
						'password' => $pass
		);



		$this->db->where('telp', $no);
		$this->db->update('pencari_lowongan', $data);
		redirect('logout');
		
	}

	public function tambah_kuota($no){
		//$query = $this->db->get_where('pencari_lowongan', array('telp' => $no));
		$this->load->helper('url');
		$this->load->library('form_validation');
		//$idpekerja		= $this->input->post('nama');
		//$username	= $this->input->post('username');
		$bayar =$this->input->post('bayar'); 
		//$telp =$this->input->post('telp');
		//$pendidikan =$this->input->post('pendidikan');
		

		$data = array(	
						
						
						'update_kuota' => $bayar
		);



		$this->db->where('telp', $no);
		$this->db->update('pencari_lowongan', $data);
		redirect('klien/profile');
		
	}


	public function deletependidikan($id){
		return $this->db->delete('pendidikan_informal', array('id' => $id));
		
	}
	public function deletepengalaman($id){
		return $this->db->delete('pengalaman_kerja', array('id' => $id));
		
	}

	public function do_upload()
        {
        	
                $config['upload_path']          = base_url().'/dist/img/';
                $config['upload_url']    		=	base_url()."dist/img/";
                $config['allowed_types']        = 'jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
                //$config ['file_name']			= $no;

                $this->load->library('upload', $config);
   

 if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            echo "hahaha";

           // $this->load->view('upload_form', $error);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());

           
          }   
        }

 public function uploadcv()
        {
        	
                $config['upload_path']          = base_url().'/dist/berkas/';
                $config['upload_url']    		=	base_url()."dist/berkas/";
                $config['allowed_types']        = 'pdf';
                $config['max_size']             = 1000;
                
                //$config ['file_name']			= $no;

                $this->load->library('upload', $config);
   

 if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            echo "hahaha";

           // $this->load->view('upload_form', $error);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());

           
          }   
        }
        

        public function uploadktp()
        {
        	
                $config['upload_path']          = base_url().'/dist/berkas/';
                $config['upload_url']    		=	base_url()."dist/berkas/";
                $config['allowed_types']        = 'pdf';
                $config['max_size']             = 1000;
                
                //$config ['file_name']			= $no;

                $this->load->library('upload', $config);
   

 		if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            echo "hahaha";

           // $this->load->view('upload_form', $error);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());

           
          }   
        }

        public function uploadijazahdep()
        {
        	
                $config['upload_path']          = base_url().'/dist/berkas/';
                $config['upload_url']    		=	base_url()."dist/berkas/";
                $config['allowed_types']        = 'pdf';
                $config['max_size']             = 1000;
                
                //$config ['file_name']			= $no;

                $this->load->library('upload', $config);
   

	 		if ( ! $this->upload->do_upload())
	        {
	            $error = array('error' => $this->upload->display_errors());
	            echo "hahaha";

	           // $this->load->view('upload_form', $error);
	        }
	        else
	        {
	            $data = array('upload_data' => $this->upload->data());

	           
	          }   
        }

        public function uploadijazahbel()
        {
        	
            $config['upload_path']          = base_url().'/dist/berkas/';
            $config['upload_url']    		=	base_url()."dist/berkas/";
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 1000;
            
            //$config ['file_name']			= $no;

            $this->load->library('upload', $config);
   

	 		if ( ! $this->upload->do_upload())
	        {
	            $error = array('error' => $this->upload->display_errors());
	            echo "hahaha";

	           // $this->load->view('upload_form', $error);
	        }
	        else
	        {
	            $data = array('upload_data' => $this->upload->data());

	           
	          }   
        }


}
?>