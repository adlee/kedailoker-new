<?php
class perusahaan_model extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_location(){
		$query = $this->db->query('SELECT * FROM inf_lokasi where lokasi_kabupatenkota=0 and lokasi_kecamatan=0 and lokasi_kelurahan=0 order by lokasi_nama');
		return $query -> result_array();
	}

	public function get_session($tlp){
			$query = $this->db->get_where('perusahaan', array('no_telp' => $tlp));
			if ($query->num_rows()>0) {
				$this->session->set_userdata('nom', $tlp);
				redirect('perusahaan/persetujuan');
			}else{
				redirect('perusahaan');
			}
	}
	public function get_info($no){
		$query = $this->db->get_where('perusahaan', array('no_telp' => $no));
		return $query -> result_array();
	}

	public function get_sd($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='SD' group by pencari_lowongan.id");
		return $query -> result_array();
	}
	public function get_smp($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='SMP' group by pencari_lowongan.id");
		return $query -> result_array();
	}
	public function get_sma($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='SMA' group by pencari_lowongan.id");
		return $query -> result_array();
	}
	public function get_smk($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='SMK' group by pencari_lowongan.id");
		return $query -> result_array();
	}
	public function get_d1($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='D1' group by pencari_lowongan.id");
		return $query -> result_array();
	}
	public function get_d2($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='D2' group by pencari_lowongan.id");
		return $query -> result_array();
	}
	public function get_d3($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='D3' group by pencari_lowongan.id");
		return $query -> result_array();
	}
	public function get_d4($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='D4' group by pencari_lowongan.id");
		return $query -> result_array();
	}
	public function get_s1($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='S1' group by pencari_lowongan.id");
		return $query -> result_array();
	}
	public function get_s2($no){
		$query = $this->db->query("select lamaran.id_pencari from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no and pencari_lowongan.pend_terakhir='S2' group by pencari_lowongan.id");
		return $query -> result_array();
	}

	public function get_lowongan_idpost($id){
		$query = $this->db->query("select *, date_format(tgl,'%d-%m-%Y') as tgl from lowongan where id_post=$id");
		return $query -> result_array();
	}
	public function get_pelamar_idlow($id){
		$query = $this->db->query("select *, pencari_lowongan.id as idpel, date_format(tgl,'%d-%m-%Y') as tgl from pencari_lowongan,lamaran where pencari_lowongan.id=lamaran.id_pencari and lamaran.id_low=$id");
		return $query -> result_array();
	}
	public function get_pelamar_id($id){
		$query = $this->db->query("select * from pencari_lowongan where id=$id");
		return $query -> result_array();
	}
	public function get_loker($no){
		$query = $this->db->query("select *,lowongan.status as lstatus from lowongan,perusahaan where lowongan.id_perus=perusahaan.id and perusahaan.no_telp=$no");
		return $query -> result_array();
	}
	public function get_loker2($no){
		$query = $this->db->query("select *,lowongan.status as lstatus from lowongan,perusahaan where lowongan.id_perus=perusahaan.id and perusahaan.no_telp=$no order by lowongan.aktif asc");
		return $query -> result_array();
	}
	public function get_loker_id($id){
		$query = $this->db->query("select *, date_format(tgl,'%d-%m-%Y') as tgl from lowongan where id_perus=$id");
		return $query -> result_array();
	}
	public function get_loker_selected($id_post){
		$query = $this->db->query("select *, date_format(tgl,'%d-%m-%Y') as tgl from lowongan where id_post=$id_post");
		return $query -> result_array();
	}
	public function get_loker_pending(){
		$query = $this->db->query("select id_perus from lowongan where status='pending' ");
		return $query -> result_array();
	}
	public function get_pelamar(){
		$query = $this->db->query('select * from lamaran');
		return $query -> result_array();
	}
	public function get_pend_pelamar(){
		$query = $this->db->query('select pend_terakhir from pencari_lowongan');
		return $query -> result_array();
	}
	public function get_lamaran_ke($tlp_perusahaan, $id_pencari){
		$query = $this->db->query("select lamaran.id_pencari as idp, pencari_lowongan.username, lowongan.judul as judul, date_format(lamaran.tgl,'%d-%m-%Y') as tgl, lamaran.status as status, lowongan.id_post as id_post from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$tlp_perusahaan and pencari_lowongan.id=$id_pencari");
		return $query -> result_array();
	}
	public function set_on($no){
		$data = array(
			'status' => 'on',
		);
		$this->db->where('no_telp', $no);
		return $this->db->update('perusahaan', $data);
	}
	public function get_status($no){
		$query = $this->db->get_where('perusahaan', array('no_telp' => $no));
		return $query -> result_array();
	}
	public function update_profile($no){
		$this->load->helper('url');

		$nama	= $this->input->post('namaper');
		$tlp 	= $this->input->post('notelp');
		$region	= $this->input->post('region');
		$alm 	= $this->input->post('alamat');
		$hrd 	= $this->input->post('namahrd');
		$tlp2 	= $this->input->post('nohrd');
		$email 	= $this->input->post('email');
		$sandi 	= $this->input->post('password');

		$data = array(	'nama_per' => $nama,
						'region' => $region,
						'password' =>  $sandi,
						'no_telp' => $tlp,
						'alamat' => $alm,
						'nama_hrd' =>  $hrd,
						'no_hrd' => $tlp2,
						'email_hrd' => $email,
						'password' => $sandi
		);
		$this->db->where('no_telp',$no);
		return $this->db->update('perusahaan', $data);
	}
	public function add_perusahaan(){
		$this->load->helper('url');

		$nama	= $this->input->post('nama');
		$tlp 	= $this->input->post('telepon');
		$region	= $this->input->post('region');
		$alm 	= $this->input->post('alamat');
		$prop 	= $this->input->post('prop');
		$kota 	= $this->input->post('kota');
		$hrd 	= $this->input->post('nama_hrd');
		$tlp2 	= $this->input->post('teleponhrd');
		$email 	= $this->input->post('email');
		$sandi 	= $this->input->post('repeat');

		$data = array(	'nama_per' => $nama,
						'region' => $region,
						'password' =>  $sandi,
						'no_telp' => $tlp,
						'alamat' => $alm,
						'nama_hrd' =>  $hrd,
						'no_hrd' => $tlp2,
						'email_hrd' => $email,
						'password' => $sandi
		);
		$this->db->insert('perusahaan', $data);
		
		$data2 = array(	'email' => $email
		);
		return $this->db->insert('users_login', $data2);
	}

	public function get_login($mail, $pass){
			$query = $this->db->get_where('perusahaan', array('email_hrd' => $mail, 'password' => $pass));
			if ($query->num_rows()>0) {
				$this->session->set_userdata('mail', $mail);
				$this->session->set_userdata('password', $pass);
				redirect('perusahaan/home');
			}else{
				redirect('perusahaan');
			}
	}
	public function get_no_telepon($mail){
		$query = $this->db->get_where('perusahaan', array('email_hrd' => $mail));
		return $query -> result_array();
	}

	public function get_jobfair_perusahaan($id_jobfair) {
		$this->db->where('id_jobfair', $id_jobfair);
		$res = $this->db->get('jobfair_perusahaan');
		return $res->result();
	}

	public function get_count(){
		$query = $this->db->query('select count(id) as count from perusahaan');
		return $query ->result_array();
	}

	public function get_perusahaan($id = FALSE){
		if ($id == FALSE) {
			$query = $this->db->query('select * from perusahaan order by id');
			return $query -> result_array();

		}else{
			$query = $this->db->get_where('perusahaan', array('id' => $id));
			return $query -> result_array();
		}	
	}
	public function get_perusahaan_n_loker(){
		$query = $this->db->query("select *,perusahaan.id as id_perus from perusahaan,lowongan where perusahaan.id=lowongan.id_perus");
		return $query -> result_array();
	}
	public function delete_pelamar($id_lok, $id_pel){
		return $this->db->query("delete from lamaran where id_pencari=$id_pel and id_low=$id_lok ");
	}
	public function konfirm_pelamar($id_lok, $id_pel){
		return $this->db->query("update lamaran set status='diterima' where id_pencari=$id_pel and id_low=$id_lok");
	}
	public function set_off_perusahaan($id){
		$data = array(
			'status' => 'off',
		);
		$this->db->where('id', $id);
		return $this->db->update('perusahaan', $data);
	}
	public function set_on_perusahaan($id){
		$data = array(
			'status' => 'on',
		);
		$this->db->where('id', $id);
		return $this->db->update('perusahaan', $data);
	}
	public function delete_perusahaan($id){
		return $this->db->query("delete from perusahaan where id=$id ");
	}
	public function delete_posting($id){
		return $this->db->query("delete from lowongan where id_post=$id ");
	}
	public function share_posting($id_post, $bayar){
		if ($bayar>150000) {
			$aktif=90;
		}else if ($bayar>100000) {
			$aktif=60;
		}else if ($bayar>50000) {
			$aktif=30;
		}
		$data = array(
			'status' => 'shared',
			'tgl_aktif' => date('Y-m-d'),
			'aktif' => $aktif,
		);
		$this->db->where('id_post', $id_post);
		return $this->db->update('lowongan', $data);
	}
	public function decrease_sms($minpend){
		if ($minpend=="SD") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' or pend_terakhir='S1' or pend_terakhir='D4' or pend_terakhir='D3' or pend_terakhir='D2' or pend_terakhir='D1' or pend_terakhir='SMK' or pend_terakhir='SMA' or pend_terakhir='SMP' or pend_terakhir='SD' ");
		}else if ($minpend=="SMP") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' or pend_terakhir='S1' or pend_terakhir='D4' or pend_terakhir='D3' or pend_terakhir='D2' or pend_terakhir='D1' or pend_terakhir='SMK' or pend_terakhir='SMA' or pend_terakhir='SMP' ");
		}else if ($minpend=="SMA") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' or pend_terakhir='S1' or pend_terakhir='D4' or pend_terakhir='D3' or pend_terakhir='D2' or pend_terakhir='D1' or pend_terakhir='SMA' ");
		}else if ($minpend=="SMK") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' or pend_terakhir='S1' or pend_terakhir='D4' or pend_terakhir='D3' or pend_terakhir='D2' or pend_terakhir='D1' or pend_terakhir='SMK' ");
		}else if ($minpend=="D1") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' or pend_terakhir='S1' or pend_terakhir='D4' or pend_terakhir='D3' or pend_terakhir='D2' or pend_terakhir='D1' ");
		}else if ($minpend=="D2") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' or pend_terakhir='S1' or pend_terakhir='D4' or pend_terakhir='D3' or pend_terakhir='D2' ");
		}else if ($minpend=="D3") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' or pend_terakhir='S1' or pend_terakhir='D4' or pend_terakhir='D3' ");
		}else if ($minpend=="D4") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' or pend_terakhir='S1' or pend_terakhir='D4' ");
		}else if ($minpend=="S1") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' or pend_terakhir='S1' ");
		}else if ($minpend=="S2") {
			return $this->db->query("update pencari_lowongan set kuota_sms=kuota_sms-1 where kuota_sms>0 and pend_terakhir='S2' ");
		}
	}
	public function get_namapelamar($no){
		$query = $this->db->query("select pencari_lowongan.username as nama, pencari_lowongan.id as id, pencari_lowongan.pend_terakhir as pend, pencari_lowongan.telp as telp from lamaran,perusahaan,lowongan,pencari_lowongan where perusahaan.id=lowongan.id_perus and lowongan.id_post=lamaran.id_low and lamaran.id_pencari=pencari_lowongan.id and perusahaan.no_telp=$no group by id ");
		return $query -> result_array();
	}
	public function get_pelamar_informal($no){
		$query = $this->db->query("select * from pendidikan_informal where id_pencari='$no' group by id");
		return $query -> result_array();
	}
	public function get_pelamar_pengalaman($no){
		$query = $this->db->query("select * from pengalaman_kerja where id_pencari='$no' group by id");
		return $query -> result_array();
	}
	public function upload_loker($id){
		$this->load->helper('url');

		$gaji1=$this->input->post('gaji1');
		$gaji2=$this->input->post('gaji2');
		$all="";
		$cek=0; 
	    for ($i=10; $i>=1 ; $i--) { 
	      if (isset($_POST['cek'.$i])) {
	          $data   = $_POST['cek'.$i] ;
	          $all = $data." ".$all;
	          $cek++;
	      }
	    }
		
		$data = array(	'id_perus' => $id,
						'judul' => $this->input->post('judul'),
						'isi' =>  $this->input->post('isi'),
						'pendidikan' => $all,
						'jml_pend' => $cek,
						'gaji_min' => $gaji1,
						'gaji_max' => $gaji2,
						'kuota' =>	$this->input->post('kuota'),
						'masa_aktif' => $this->input->post('masa_aktif')
		);
		return $this->db->insert('lowongan', $data);
	}
	public function update_loker($id_post){
		$this->load->helper('url');

		$gaji1=$this->input->post('gaji1');
		$gaji2=$this->input->post('gaji2');
		
		$data = array(	'judul' => $this->input->post('judul'),
						'isi' =>  $this->input->post('isi'),
						'pendidikan' => $this->input->post('pendidikan'),
						'gaji_min' => $gaji1,
						'gaji_max' => $gaji2,
						'kuota' =>	$this->input->post('kuota')
		);
		$this->db->where('id_post', $id_post);
		return $this->db->update('lowongan', $data);
	}
	
	
	public function set_message($id_perusahaan, $id_lowongan, $id_pencari){
		$this->load->helper('url');
		$data = array(	'id_perusahaan' => $id_perusahaan,
						'id_lowongan' => $id_lowongan,
						'id_pencari' =>  $id_pencari,
						'pesan' => $this->input->post('pesan')
		);
		return $this->db->insert('message', $data);		
	}
	public function set_message_ph($id_perusahaan){
		$this->load->helper('url');
		$data = array(	'id_perusahaan' => $id_perusahaan,
						'id_lowongan' => $this->input->post('lowongan'),
						'id_pencari' =>  $this->input->post('pencari'),
						'pesan' => $this->input->post('pesan')
		);
		return $this->db->insert('message', $data);		
	}
	public function get_pesan($id = false){
		if ($id == FALSE) {
			$query = $this->db->query("select *, date_format(tanggal,'%d-%m-%Y') as tanggal from message");
		}else{
			$query = $this->db->query("select *, date_format(tanggal,'%d-%m-%Y') as tanggal from message where id=$id ");
		}
		return $query -> result_array();
	}
	public function delete_message($id_pesan){
		return $this->db->query("delete from message where id=$id_pesan");
	}
	public function edit_pesan($id_pesan){
		$this->load->helper('url');
		$tgl=date('Y-m-d');
		$data = array(	'tanggal' =>  $tgl,
						'pesan' => $this->input->post('pesan'),
						'status' => 'baru'
		);
		$this->db->where('id', $id_pesan);
		return $this->db->update('message', $data);		
	}
	public function tutup_lowongan($id_low){
		$this->load->helper('url');
		$data = array(	'status_kuota' =>  "tutup",
		);
		$this->db->where('id_post', $id_low);
		return $this->db->update('lowongan', $data);		
	}
	public function buka_lowongan($id_low){
		$this->load->helper('url');
		$data = array(	'status_kuota' =>  "buka",
		);
		$this->db->where('id_post', $id_low);
		return $this->db->update('lowongan', $data);		
	}
	public function get_message_jf($no){
		$query = $this->db->get_where('message_jobfair', array('id_perusahaan' => $no));
		return $query -> result_array();
	}
	public function get_newmessage_jf($no){
		$query = $this->db->get_where('message_jobfair', array('id_perusahaan' => $no, 'status' => "baru"));
		return $query -> result_array();
	}
	public function get_jf_id($idmjf){
		$query = $this->db->get_where('message_jobfair', array('id' => $idmjf));
		return $query -> result_array();
	}
	public function buka_pesan($idmjf){
		$this->load->helper('url');
		$data = array(	'status' => 'terbaca' );
		$this->db->where('id', $idmjf);
		return $this->db->update('message_jobfair', $data);		
	}
	public function del_pesan_jf($id){
		return $this->db->query("delete from message_jobfair where id=$id");
	}

	// baru========================================================================================================
	public function get_count_new(){
		$query = $this->db->query("select count(id) as count from perusahaan where cek='unchecked'");
		return $query -> result_array();
	}
	public function get_new_perusahaan(){
		$query = $this->db->query("select * from perusahaan where cek='unchecked'");
		return $query -> result_array();
	}
	public function get_jobfair(){
		$query = $this->db->query("select count(id) as count from jobfair");
		return $query -> result_array();
	}
	public function update_masa_aktif(){
		$id_post = $this->input->post('id_lowongan');
		$aktif = $this->input->post('perpanjangan');

		$this->load->helper('url');
		$data = array(	'update_masa_aktif' => $aktif );
		$this->db->where('id_post', $id_post);

		return $this->db->update('lowongan', $data);
	}
	public function get_permintaan_new(){
		$query = $this->db->query("select count(id_post) as count from lowongan where update_masa_aktif > 0");
		return $query -> result_array();
	}
}
?>