<?php
class admin_model extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_admin($user, $pass){
			$query = $this->db->get_where('admin', array('username' => $user, 'password' => $pass));
			if ($query->num_rows()>0) {
				$this->session->set_userdata('username', $user);
				$this->session->set_userdata('password', $pass);
				redirect('adminkl1012/home');
			}else{
				redirect('adminkl1011');
			}
	}

	public function get_bank(){
		$query = $this->db->query('select * from bank');
		return $query -> result_array();
	}

	public function get_tlp(){
		$query = $this->db->query('select telepon from admin');
		return $query -> result_array();
	}
	public function set_jobfair($jml){
		$this->load->helper('url');
		$nama	= $this->input->post('nama');
		$lokasi	= $this->input->post('lokasi');
		$tanggal= $this->input->post('tanggal');
		$jamm	= $this->input->post('jam_m');
    	$menitm	= $this->input->post('menit_m');
    	$jams	= $this->input->post('jam_s');
    	$menits	= $this->input->post('menit_s');
    	$jam = "$jamm:$menitm - $jams:$menits";
		$id_per="";
	    $cek=0;
	    $id_company = array();
        for ($i=1; $i<=$jml ; $i++) { 
          if (isset($_POST['cek'.$i])) {
              $data   = $_POST['cek'.$i] ;
              $id_per = $data." ".$id_per;
              $cek++;
              $id_company[] = $id_per;
          }
        }
        // insert to jobfair table
		$datajobfair = array(	
			'nama' 		=> $nama,
			'lokasi' 	=> $lokasi,
			'tanggal' 	=> $tanggal,
			'jam' 		=> $jam,
			'id_perusahaan' => $id_per,
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s')
		);
		$this->db->insert('jobfair', $datajobfair);

		// insert company on jobfair
		$last_id = $this->db->insert_id();
		for($i=0;$i<count($id_company);$i++) {
			$data_ins = array(
					'id_perusahaan'	=> $id_company[$i],
					'id_jobfair'	=> $last_id,
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			$this->db->insert('jobfair_perusahaan', $data_ins);
		}
		

		$pesan="Anda diundang untuk menghadiri JOBFAIR<br/>$nama akan diselenggarakan di $lokasi pada tanggal $tanggal, jam $jam.";
		$all=explode(" ", $id_per);
    	for($i=1; $i<=$cek ; $i++) { 
    		$x=$i-1;
          	$pr[$i]=$all[$x];
          	$data_pesan = array('id_perusahaan' => $pr[$i],
          					'pesan' => $pesan,
          	);
			$this->db->insert('message_jobfair', $data_pesan);
    	}

    	return 1;
	}

	public function set_loker(){
		$this->load->helper('url');

		$lowongan	= $this->input->post('lowongan');
		$kuota		= $this->input->post('kuota');
		$syarat		= $this->input->post('syarat');
		$gaji_min	= $this->input->post('gaji1');
    	$gaji_max	= $this->input->post('gaji2');
    	$perusahaan	= $this->input->post('perusahaan');
    	$cekarray	= $this->input->post('pend[]'); 
    	$aktif		= $this->input->post('masa_aktif');
    	$jobfair		= $this->input->post('jobfair');
    	$jenisloker		= $this->input->post('jenisloker');
    	$pendidikan = implode(" ", $cekarray);

		$datajobfair = array(	'judul' => $lowongan,
						'kuota' => $kuota,
						'isi' => $syarat,
						'gaji_min' => $gaji_min,
						'gaji_max' => $gaji_max,
						'id_perus' => $perusahaan,
						'pendidikan' => $pendidikan,
						'masa_aktif' => $aktif,
						'jenis_loker' => $jenisloker,
						'id_jobfair' => $jobfair
		);

		$this->db->insert('lowongan', $datajobfair);		

	}

	public function get_jf($id = FALSE){
		if ($id == FALSE) {
			$query = $this->db->query("select *, date_format(tanggal,'%d-%m-%Y') as tgl from jobfair");
		}else{
			$query = $this->db->query("select *, date_format(tanggal,'%d-%m-%Y') as tgl from jobfair where id=$id ");
		}
		return $query -> result_array();
	}
	public function delete_jobfair($id){
		return $this->db->query("delete from jobfair where id=$id");
	}
	public function update_jobfair($id, $jml){
		$this->load->helper('url');

		$nama	= $this->input->post('nama');
		$lokasi	= $this->input->post('lokasi');
		$tanggal= $this->input->post('tanggal');
		$jamm	= $this->input->post('jam_m');
    	$menitm	= $this->input->post('menit_m');
    	$jams	= $this->input->post('jam_s');
    	$menits	= $this->input->post('menit_s');
    	$jam = "$jamm:$menitm - $jams:$menits";
		$id_per="";
	    $cek=0; 
	    $id_company = array();
	    $qry = $this->db->query('select max(id) as maxid from perusahaan');
	    $maxid = $qry->result();
	    $maxid = $maxid[0]->maxid;
        for ($i=1; $i<=$maxid ; $i++) { 
          if (isset($_POST['cek'.$i])) {
              $data   = $_POST['cek'.$i];
              $id_perusahaan = $id_per;
              $id_per = $data." ".$id_per;
              $cek++;
              $id_company[] = $data;
          }
        }

		$datajobfair = array(	
			'nama' => $nama,
			'lokasi' => $lokasi,
			'tanggal' => $tanggal,
			'jam' => $jam,
			'id_perusahaan' => $id_per,
			'perusahaan' => $cek
		);
		$this->db->where('id', $id);
		$this->db->update('jobfair', $datajobfair);

		// delete all data jobfair perusahaan
		$this->db->where('id_jobfair', $id);
		$this->db->delete('jobfair_perusahaan');

		// insert data to jobfair_perusahaan
		for($i=0;$i<count($id_company);$i++) {
			$data_ins = array(
					'id_perusahaan'	=> $id_company[$i],
					'id_jobfair'	=> $id,
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s')
				);
			$this->db->insert('jobfair_perusahaan', $data_ins);
		}
	}

	public function update_data_perusahaan() {
		
		$telp_p     = trim($this->input->post('telp_p'));
		$hrd_p      = trim($this->input->post('hrd_p'));
		$email_p    = trim($this->input->post('email_p'));
		$alamat_p   = trim($this->input->post('alamat_p'));
		$telp_hrd_p = trim($this->input->post('telp_hrd_p'));
		$image      = $this->input->post('image');
		$id         = $this->input->post('id_p');

		$page = $this->find('perusahaan', $id);

		if($_FILES['image']['name'] != ""){

			unlink("dist/img/perusahaan/$page->foto");
			
			$config['upload_path']   = 'dist/img/perusahaan';
			$config['allowed_types'] = 'jpg|png|jpeg|gif';
			$config['max_size']      = '2000';
			$config['remove_spaces'] = true;
			$config['overwrite']     = false;
			$config['encrypt_name']  = true;
			$config['max_width']     = '';
			$config['max_height']    = '';
		    $this->load->library('upload', $config);
		    $this->upload->initialize($config);            
		    if (!$this->upload->do_upload('image'))
		    {
		        $this->session->set_flashdata('item','
					<div class="alert alert-success alert-dismissible" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
	                        '.$this->upload->display_errors().' !!
	                </div>'
	            );
				
				redirect('adminkl1011/perusahaan');
		    }
		    else
		    {
	        	$image = $this->upload->data();
	        	$source = "./dist/img/perusahaan/".$image['file_name'];

				// Permission Configuration
				chmod($source, 0777);
		        if ($image['file_name'])
		        {
		            $data['file1'] = $image['file_name'];
		        }        
		        $img 	= $data['file1'];
		    }

		    $data = array(
				'no_telp'   => $telp_p,
				'no_hrd'    => $telp_hrd_p,
				'nama_hrd'  => $hrd_p, 
				'email_hrd' => $email_p,
				'alamat'    => $alamat_p, 
				'foto'      => $img, 
			);
		} else {
			
			$data = array(
				'no_telp'   => $telp_p,
				'no_hrd'    => $telp_hrd_p,
				'nama_hrd'  => $hrd_p, 
				'email_hrd' => $email_p,
				'alamat'    => $alamat_p, 
			);
		}

		$this->db->where('id', $this->input->post('id_p'));
		$this->db->update('perusahaan', $data);
		return 1;
	}

	public function count_off(){
		$query = $this->db->query("select * from pencari_lowongan where kuota_sms=0 order by pend_terakhir");
		return $query -> result_array();
	}
	public function count_warning(){
		$query = $this->db->query("select * from pencari_lowongan where kuota_sms < 6 and kuota_sms > 0 order by pend_terakhir");
		return $query -> result_array();
	}
	public function count_on(){
		$query = $this->db->query("select * from pencari_lowongan where kuota_sms > 5 order by pend_terakhir");
		return $query -> result_array();
	}
	public function perusahaan_on($jml, $perusahaan, $hrd, $teleponn, $lowongan){
		$all="";
		$cek=0; 
        for ($i=1; $i<=$jml ; $i++) { 
          if (isset($_POST['cek'.$i])) {
            if (isset($_POST['cek'.$i])) {
              $data   = $_POST['cek'.$i] ;
              $all = $data." ".$all;
              $cek++;
            }
          }
        }
        $id_all= explode(" ", $all);
    	$data_perusahaan = $this->db->where('lowongan.id_perus', $id_all[0] );
    	$idx=$id_all[0];

    	$id_all= explode(" ", $all);
    	$start=1;
    	while ($start < $cek) {
    		$data_perusahaan = $this->db->or_where('lowongan.id_perus', $id_all[$start] );
    		$idx = $id_all[$start];
    	$start++;
    	}
    	$data_perusahaan = $this->db->join('perusahaan', 'perusahaan.id = lowongan.id_perus', 'left')
    							 ->order_by("lowongan.id_perus", "asc")
    						     ->get('lowongan')->result();
    	?>
    	<section class="content-header" style="width:100%">
		    <h2>Perusahaan</h2>
		    <h4>Status ON</h4>
		</section>  
		<table  class="table table-bordered table-striped" border="1" style="font-size:100%">
		    <tr>
		      <th width="50px">No</th>
		      <?php if ($perusahaan!='0'): ?>
		      <th width="200px">Perusahaan </th>
		      <?php endif ?>
		      <?php if ($hrd!='0'): ?>
		      <th width="100px">HRD</th>	
		      <?php endif ?>
		      <?php if ($teleponn!='0'): ?>
		      <th width="100px">Telepon</th>	
		      <?php endif ?>
		      <?php if ($lowongan!='0'): ?>
		      <th width="100px">Lowongan</th>	
		      <?php endif ?>
		    </tr>
		    <?php 
		  	  $no=1;
              $telepon="";
              foreach ($data_perusahaan as $perusahaan_on) { 
                if ($perusahaan_on->aktif > 3) {
                  if ($telepon==$perusahaan_on->no_telp) {
                  }else{
                    $telepon=$perusahaan_on->no_telp ?>
                    <tr style="text-align:left">
                      <td valign="top" ><?php echo $no ?></td>
                      <?php if ($perusahaan!='0'): ?>
				      	<td valign="top"><?php echo $perusahaan_on->nama_per ?></td>  
				      <?php endif ?>
				      <?php if ($hrd!='0'): ?>
				     	 <td valign="top"><?php echo $perusahaan_on->nama_hrd ?></td>  
				      <?php endif ?>
				      <?php if ($teleponn!='0'): ?>
				     	 <td valign="top"><?php echo $perusahaan_on->no_telp ?></td>  
				      <?php endif ?>
				      <?php if ($lowongan!='0'): ?>
					      <td valign="top">
	                        <?php foreach ($data_perusahaan as $perusahaan_on_all) {
	                          if ($perusahaan_on_all->aktif > 3) {
	                            if ($perusahaan_on->id_perus == $perusahaan_on_all->id_perus) {
	                              echo $perusahaan_on_all->judul;
	                              echo "<br/>";
	                            }
	                          }
	                            
	                        } ?>
	                      </td>  
				      <?php endif ?>
                    </tr><?php 
                  $no++;
                  }
                }
              }?>
    	</table> <?php 
	}
	public function perusahaan_warning($jml, $perusahaan, $hrd, $teleponn, $lowongan){
		$all="";
		$cek=0; 
        for ($i=1; $i<=$jml ; $i++) { 
          if (isset($_POST['cek'.$i])) {
            if (isset($_POST['cek'.$i])) {
              $data   = $_POST['cek'.$i] ;
              $all = $data." ".$all;
              $cek++;
            }
          }
        }
        $id_all= explode(" ", $all);
    	$data_perusahaan = $this->db->where('lowongan.id_perus', $id_all[0] );
    	$idx=$id_all[0];

    	$id_all= explode(" ", $all);
    	$start=1;
    	while ($start < $cek) {
    		$data_perusahaan = $this->db->or_where('lowongan.id_perus', $id_all[$start] );
    		$idx = $id_all[$start];
    	$start++;
    	}
    	$data_perusahaan = $this->db->join('perusahaan', 'perusahaan.id = lowongan.id_perus', 'left')
    							 ->order_by("lowongan.id_perus", "asc")
    						     ->get('lowongan')->result();
    	?>
    	<section class="content-header" style="width:100%">
		    <h2>Perusahaan</h2>
		    <h4>Status WARNING</h4>
		</section>  
		<table  class="table table-bordered table-striped" border="1" style="font-size:100%">
		    <tr>
		      <th width="50px">No</th>
		      <?php if ($perusahaan!='0'): ?>
		      <th width="200px">Perusahaan </th>
		      <?php endif ?>
		      <?php if ($hrd!='0'): ?>
		      <th width="100px">HRD</th>	
		      <?php endif ?>
		      <?php if ($teleponn!='0'): ?>
		      <th width="100px">Telepon</th>	
		      <?php endif ?>
		      <?php if ($lowongan!='0'): ?>
		      <th width="100px">Lowongan</th>	
		      <?php endif ?>
		    </tr>
		    <?php 
		  	  $no=1;
              $telepon="";
              foreach ($data_perusahaan as $perusahaan_warning) { 
                if ($perusahaan_warning->aktif > 3) {
                  if ($telepon==$perusahaan_warning->no_telp) {
                  }else{
                    $telepon=$perusahaan_warning->no_telp ?>
                    <tr style="text-align:left">
                      <td valign="top" ><?php echo $no ?></td>
                      <?php if ($perusahaan!='0'): ?>
				      <td valign="top"><?php echo $perusahaan_warning->nama_per ?></td>  
				      <?php endif ?>
				      <?php if ($hrd!='0'): ?>
				      <td valign="top"><?php echo $perusahaan_warning->nama_hrd ?></td>  
				      <?php endif ?>
				      <?php if ($teleponn!='0'): ?>
				      <td valign="top"><?php echo $perusahaan_warning->no_telp ?></td>  
				      <?php endif ?>
				      <?php if ($lowongan!='0'): ?>
				      <td valign="top">
                        <?php foreach ($data_perusahaan as $perusahaan_warning_all) {
                          if ($perusahaan_warning_all->aktif > 3) {
                            if ($perusahaan_warning->id_perus == $perusahaan_warning_all->id_perus) {
                              echo $perusahaan_warning_all->judul;
                              echo "<br/>";
                            }
                          }
                            
                        } ?>
                      </td>  
				      <?php endif ?>
                    </tr><?php 
                  $no++;
                  }
                }
              }?>
    	</table> <?php 
	}
	public function perusahaan_off($jml, $perusahaan, $hrd, $teleponn, $lowongan){
		$all="";
		$cek=0; 
        for ($i=1; $i<=$jml ; $i++) { 
          if (isset($_POST['cek'.$i])) {
            if (isset($_POST['cek'.$i])) {
              $data   = $_POST['cek'.$i] ;
              $all = $data." ".$all;
              $cek++;
            }
          }
        }
        $id_all= explode(" ", $all);
    	$data_perusahaan = $this->db->where('lowongan.id_perus', $id_all[0] );
    	$idx=$id_all[0];

    	$id_all= explode(" ", $all);
    	$start=1;
    	while ($start < $cek) {
    		$data_perusahaan = $this->db->or_where('lowongan.id_perus', $id_all[$start] );
    		$idx = $id_all[$start];
    	$start++;
    	}
    	$data_perusahaan = $this->db->join('perusahaan', 'perusahaan.id = lowongan.id_perus', 'left')
    						     ->order_by("lowongan.id_perus", "asc")
    						     ->get('lowongan')->result();
    	?>
    	<section class="content-header" style="width:100%">
		    <h2>Perusahaan</h2>
		    <h4>Status OFF</h4>
		</section>  
		<table  class="table table-bordered table-striped" border="1" style="font-size:100%">
		    <tr>
		      <th width="50px">No</th>
		      <?php if ($perusahaan!='0'): ?>
		      <th width="200px">Perusahaan </th>
		      <?php endif ?>
		      <?php if ($hrd!='0'): ?>
		      <th width="100px">HRD</th>	
		      <?php endif ?>
		      <?php if ($teleponn!='0'): ?>
		      <th width="100px">Telepon</th>	
		      <?php endif ?>
		      <?php if ($lowongan!='0'): ?>
		      <th width="100px">Lowongan</th>	
		      <?php endif ?>
		    </tr>
		    <?php 
		  	  $no=1;
              $telepon="";
              foreach ($data_perusahaan as $perusahaan_off) { 
                if ($perusahaan_off->aktif > 3) {
                  if ($telepon==$perusahaan_off->no_telp) {
                  }else{
                    $telepon=$perusahaan_off->no_telp ?>
                    <tr style="text-align:left">
                      <td valign="top" ><?php echo $no ?></td>
                      <?php if ($perusahaan!='0'): ?>
				      <td valign="top"><?php echo $perusahaan_off->nama_per ?></td>  
				      <?php endif ?>
				      <?php if ($hrd!='0'): ?>
				      <td valign="top"><?php echo $perusahaan_off->nama_hrd ?></td>  
				      <?php endif ?>
				      <?php if ($teleponn!='0'): ?>
				      <td valign="top"><?php echo $perusahaan_off->no_telp ?></td>  
				      <?php endif ?>
				      <?php if ($lowongan!='0'): ?>
				      <td valign="top">
                        <?php foreach ($data_perusahaan as $perusahaan_off_all) {
                          if ($perusahaan_off_all->aktif > 3) {
                            if ($perusahaan_off->id_perus == $perusahaan_off_all->id_perus) {
                              echo $perusahaan_off_all->judul;
                              echo "<br/>";
                            }
                          }
                            
                        } ?>
                      </td>  
				      <?php endif ?>
                    </tr><?php 
                  $no++;
                  }
                }
              }?>
    	</table> <?php 
	}
	public function pencari_kuota_on($jml, $nama, $telepon, $pendidikan, $kuota){
		$all="";
		$cek=0; 
        for ($i=1; $i<=$jml ; $i++) { 
          if (isset($_POST['cek'.$i])) {
            if (isset($_POST['cek'.$i])) {
              $data   = $_POST['cek'.$i] ;
              $all = $data." ".$all;
              $cek++;
            }
          }
        }
        $id_all= explode(" ", $all);
    	$data_pencari = $this->db->where('id', $id_all[0] );
    	$idx=$id_all[0];
    	$this->db->query("update pencari_lowongan set kuota_sms = kuota_sms-1 where id = $idx ");

    	$id_all= explode(" ", $all);
    	$start=1;
    	while ($start < $cek) {
    		$data_pencari = $this->db->or_where('id', $id_all[$start] );
    		$idx = $id_all[$start];
    		$this->db->query("update pencari_lowongan set kuota_sms = kuota_sms-1 where id = $idx ");
    	$start++;
    	}
    	$data_pencari = $this->db->get('pencari_lowongan')->result();
    	?>
    	<section class="content-header" style="width:100%">
		    <h2>Pencari Lowongan</h2>
		    <h4>Status ON</h4>
		</section>  
		<table  class="table table-bordered table-striped" border="1" style="font-size:100%">
		    <tr>
		      <th width="50px">No</th>
		      <?php if ($nama!='0'): ?>
		      <th width="200px">Nama </th>		
		      <?php endif ?>
		      <?php if ($telepon!='0'): ?>
		      <th width="100px">Telepon</th>	
		      <?php endif ?>
		      <?php if ($pendidikan!='0'): ?>
		      <th width="100px">Pendidikan</th>
		      <?php endif ?>
		      <?php if ($kuota!='0'): ?>
		      <th width="100px">Kuota</th>	
		      <?php endif ?>
		    </tr>
		  <?php 
		  	$no=1;
		  	foreach ($data_pencari as $pencari) { ?>
		  	<tr>
		  	  <td> &nbsp <?php echo $no; ?> &nbsp</td>
		  	  <?php if ($nama!='0'): ?>
		      <td> &nbsp <?php echo $pencari->username ?> &nbsp</td>
		      <?php endif ?>
		      <?php if ($telepon!='0'): ?>
		      <td> &nbsp <?php echo $pencari->telp ?> &nbsp</td>
		      <?php endif ?>
		      <?php if ($pendidikan!='0'): ?>
		      <td> &nbsp <?php echo $pencari->pend_terakhir ?> &nbsp</td>
		      <?php endif ?>
		      <?php if ($kuota!='0'): ?>
		      <td> &nbsp <?php echo $pencari->kuota_sms ?> &nbsp</td>
		      <?php endif ?>
		  	</tr><?php
		  	$no++;
    	   } ?>
		</table><?php
	}

	public function pencari_kuota_warning($jml, $nama, $telepon, $pendidikan, $kuota){
		$all="";
		$cek=0; 
        for ($i=1; $i<=$jml ; $i++) { 
          if (isset($_POST['cek'.$i])) {
            if (isset($_POST['cek'.$i])) {
              $data   = $_POST['cek'.$i] ;
              $all = $data." ".$all;
              $cek++;
            }
          }
        }
        $id_all= explode(" ", $all);
    	$data_pencari = $this->db->where('id', $id_all[0] );
    	$idx=$id_all[0];
    	$this->db->query("update pencari_lowongan set kuota_sms = kuota_sms-1 where id = $idx ");

    	$id_all= explode(" ", $all);
    	$start=1;
    	while ($start < $cek) {
    		$data_pencari = $this->db->or_where('id', $id_all[$start] );
    		$idx = $id_all[$start];
    		$this->db->query("update pencari_lowongan set kuota_sms = kuota_sms-1 where id = $idx ");
    	$start++;
    	}
    	$data_pencari = $this->db->get('pencari_lowongan')->result();
    	?>
    	<section class="content-header" style="width:100%">
		    <h2>Pencari Lowongan</h2>
		    <h4>Status WARNING</h4>
		</section>  
		<table  class="table table-bordered table-striped" border="1" style="font-size:100%">
		    <tr>
		      <th width="50px">No</th>
		      <?php if ($nama!='0'): ?>
		      <th width="200px">Nama </th>		
		      <?php endif ?>
		      <?php if ($telepon!='0'): ?>
		      <th width="100px">Telepon</th>	
		      <?php endif ?>
		      <?php if ($pendidikan!='0'): ?>
		      <th width="100px">Pendidikan</th>
		      <?php endif ?>
		      <?php if ($kuota!='0'): ?>
		      <th width="100px">Kuota</th>	
		      <?php endif ?>
		    </tr>
		  <?php 
		  	$no=1;
		  	foreach ($data_pencari as $pencari) { ?>
		  	<tr>
		  	  <td> &nbsp <?php echo $no; ?> &nbsp</td>
		  	  <?php if ($nama!='0'): ?>
		      <td> &nbsp <?php echo $pencari->username ?> &nbsp</td>
		      <?php endif ?>
		      <?php if ($telepon!='0'): ?>
		      <td> &nbsp <?php echo $pencari->telp ?> &nbsp</td>
		      <?php endif ?>
		      <?php if ($pendidikan!='0'): ?>
		      <td> &nbsp <?php echo $pencari->pend_terakhir ?> &nbsp</td>
		      <?php endif ?>
		      <?php if ($kuota!='0'): ?>
		      <td> &nbsp <?php echo $pencari->kuota_sms ?> &nbsp</td>
		      <?php endif ?>
		  	</tr><?php
		  	$no++;
    	   } ?>
		</table><?php
	}

	public function pencari_kuota_off($jml, $nama, $telepon, $pendidikan, $kuota){
		$all="";
		$cek=0; 
        for ($i=1; $i<=$jml ; $i++) { 
          if (isset($_POST['cek'.$i])) {
            if (isset($_POST['cek'.$i])) {
              $data   = $_POST['cek'.$i] ;
              $all = $data." ".$all;
              $cek++;
            }
          }
        }
        $id_all= explode(" ", $all);
    	$data_pencari = $this->db->where('id', $id_all[0] );
    	$idx=$id_all[0];

    	$id_all= explode(" ", $all);
    	$start=1;
    	while ($start < $cek) {
    		$data_pencari = $this->db->or_where('id', $id_all[$start] );
    		$idx = $id_all[$start];
    	$start++;
    	}
    	$data_pencari = $this->db->get('pencari_lowongan')->result();
    	?>
    	<section class="content-header" style="width:100%">
		    <h2>Pencari Lowongan</h2>
		    <h4>Status OFF</h4>
		</section>  
		<table  class="table table-bordered table-striped" border="1" style="font-size:100%">
		    <tr>
		      <th width="50px">No</th>
		      <?php if ($nama!='0'): ?>
		      <th width="200px">Nama </th>		
		      <?php endif ?>
		      <?php if ($telepon!='0'): ?>
		      <th width="100px">Telepon</th>	
		      <?php endif ?>
		      <?php if ($pendidikan!='0'): ?>
		      <th width="100px">Pendidikan</th>
		      <?php endif ?>
		      <?php if ($kuota!='0'): ?>
		      <th width="100px">Kuota</th>	
		      <?php endif ?>
		    </tr>
		  <?php 
		  	$no=1;
		  	foreach ($data_pencari as $pencari) { ?>
		  	<tr>
		  	  <td> &nbsp <?php echo $no; ?> &nbsp</td>
		  	  <?php if ($nama!='0'): ?>
		      <td> &nbsp <?php echo $pencari->username ?> &nbsp</td>
		      <?php endif ?>
		      <?php if ($telepon!='0'): ?>
		      <td> &nbsp <?php echo $pencari->telp ?> &nbsp</td>
		      <?php endif ?>
		      <?php if ($pendidikan!='0'): ?>
		      <td> &nbsp <?php echo $pencari->pend_terakhir ?> &nbsp</td>
		      <?php endif ?>
		      <?php if ($kuota!='0'): ?>
		      <td> &nbsp <?php echo $pencari->kuota_sms ?> &nbsp</td>
		      <?php endif ?>
		  	</tr><?php
		  	$no++;
    	   } ?>
		</table><?php
	}
	public function update_sms($sts, $sms){
		$data = array(
			'kuota_sms' => $sms,
		);
		if ($sts=="on") {
			return $this->db->query("update pencari_lowongan set kuota_sms = $sms where kuota_sms > 5");
		}else if ($sts=="warning") {
			return $this->db->query("update pencari_lowongan set kuota_sms = $sms where kuota_sms > 0 and kuota_sms < 6");
		}else if ($sts=="off") {
			return $this->db->query("update pencari_lowongan set kuota_sms = $sms where kuota_sms = 0");
		}
	}
	public function update_sms_all(){
		$off	= $this->input->post('off');
		$warning	= $this->input->post('warning');
		$on	= $this->input->post('on');
		//updata on
		$this->db->query("update pencari_lowongan set kuota_sms = $on where kuota_sms > 5");
		//update warning
		$this->db->query("update pencari_lowongan set kuota_sms = $warning where kuota_sms > 0 and kuota_sms < 6");
		//update off
		return $this->db->query("update pencari_lowongan set kuota_sms = $off where kuota_sms = 0");
	}
	public function pencari_csv(){
		$data_pencari = $this->db->query("select * from pencari_lowongan order by kuota_sms asc")->result();
    	?>
    	<section class="content-header" style="width:100%">
		    <h2>Pencari Lowongan</h2>
		</section>  
		<table  class="table table-bordered table-striped" border="1" style="font-size:100%">
		    <tr>
		      <th align="center"  width="50px">No</th>
		      <th style="padding-right:5px;padding-left:5px"  width="200px">Nama / Username</th>
		      <th style="padding-right:5px;padding-left:5px"  width="120px">Pendidikan</th>
		      <th style="padding-right:5px;padding-left:5px"  width="100px">Password</th>
		      <th style="padding-right:5px;padding-left:5px"  width="100px">Telepon</th>
		      <th style="padding-right:5px;padding-left:5px"  width="100px">Kuota</th>
		      <th style="padding-right:5px;padding-left:5px"  width="100px">Status</th>
		    </tr>
		  <?php 
		  	$no=1;
		  	foreach ($data_pencari as $pencari) { ?>
		  	<tr style="text-align:left">
		  		<td align="center" ><?php echo $no ?></td>
		  		<td style="padding-right:5px;padding-left:5px" ><?php echo $pencari->username ?></td>
		  		<td style="padding-right:5px;padding-left:5px" ><?php echo $pencari->pend_terakhir ?></td>
		  		<td style="padding-right:5px;padding-left:5px" ><?php echo $pencari->password ?></td>
		  		<td style="padding-right:5px;padding-left:5px" > &nbsp <?php echo $pencari->telp ?> </td>
		  		<td align="center" ><?php echo $pencari->kuota_sms ?></td>
		  		<td style="padding-right:5px;padding-left:5px" ><?php 
		  			if ($pencari->kuota_sms > 5) {
                      echo "ON";
                    }else if ($pencari->kuota_sms > 0) {
                      echo "WARNING";
                    }else {echo "OFF";} ?>
                </td>
		  	</tr><?php
		  	$no++;
    	   } ?>
		</table><?php
	}
	public function get_klien(){
		$query = $this->db->query("select * from pencari_lowongan order by kuota_sms asc");
		return $query -> result_array();
	}
	public function get_permintaan_perusahaan(){
		$query = $this->db->query('select * from lowongan where update_masa_aktif > 0');
		return $query -> result_array();
	}
	public function perpanjang_lowongan($id, $sekarang, $tambah){
		$baru=$sekarang+$tambah;
		$data = array(
			'aktif' => $baru,
			'update_masa_aktif' => 0,
		);
		$this->db->where('id_post', $id);
		return $this->db->update('lowongan', $data);
	}
	public function delete_permintaan_perpanjangan($id){
		$data = array(
			'update_masa_aktif' => 0,
		);
		$this->db->where('id_post', $id);
		return $this->db->update('lowongan', $data);
	}

	public function cek_masa_aktif_lowongan(){
		$query = $this->db->query("SELECT id_post, aktif, tgl_aktif, CURRENT_DATE( ) AS tgl_sekarang, DATEDIFF( CURRENT_DATE( ) , tgl_aktif ) AS selisih FROM lowongan where aktif > 0");
		return $query -> result_array();	
	}

	public function find($table,$id)
	{	
		$this->db->from($table);
		return $this->where('id', $id)->row();
	}

	public function where($key, $value = null)
	{
		$this->db->where($key, $value);
		return $this;
	}

	public function row()
	{
		return $this->limit(1)->get()->row();
	}

	public function limit($limit, $offset = null)
	{
		$this->db->limit($limit, $offset);
		return $this;
	}

	public function get()
	{
		$result = $this->db->get();
		return $result;
	}
}
